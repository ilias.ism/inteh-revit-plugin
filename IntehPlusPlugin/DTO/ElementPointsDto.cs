﻿using Autodesk.Revit.DB;
using IntehPlus.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace IntehPlus.Dto
{
    public class ElementPointsDto
    {
        public ElementId ElementId { get; set; }
        public ElementId LevelId { get; set; }
        public List<XYZ> PointList { get; set; }
    }
}