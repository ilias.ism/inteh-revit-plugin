﻿using IntehPlus.Enums;
using Newtonsoft.Json;
using System;

namespace IntehPlus.Dto
{
    public class ProjectDataDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("createDateTime")]
        public DateTime CreateDateTime { get; set; }

        [JsonProperty("editDateTime")]
        public DateTime? EditDateTime { get; set; }

        [JsonProperty("isDeleted")]
        public bool IsDeleted { get; set; }
    }
}