﻿namespace IntehPlus.Dto
{
    public class InspectorInsertDto
    {
        public string Category{ get; set; }
        public string Condition { get; set; }
        public string Value { get; set; }
        public string Level { get; set; }
    }

    public class ComboboxMatchesDto
    {
        public string Level { get; set; }
        public string RevitLevel { get; set; }
    }
}