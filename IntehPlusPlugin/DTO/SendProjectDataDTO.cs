﻿using IntehPlus.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace IntehPlus.DTO
{
    public class SendProjectDataDTO
    {
        [JsonProperty("files")]
        public ICollection<byte[]> Files { get; set; }
        public Guid ProjectId { get; set; }
        public string Name { get; set; }
        public ConstructionTypeEnum ConstructionType { get; set; }
        public string Steps { get; set; }
        public int StageCount { get; set; }
        public string MainFileSetId { get; set; }
    }
    
}
