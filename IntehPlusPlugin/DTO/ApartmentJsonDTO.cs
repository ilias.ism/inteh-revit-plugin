﻿using IntehPlus.Enums;
using Newtonsoft.Json;

namespace IntehPlus.Dto
{
    public class ApartmentJsonDTO
    {
        public string ApartmentNumber { get; set; }

        public string RoomsCount { get; set; }

        public double ApartmentArea { get; set; }

        public double ApartmentLivingArea { get; set; }

        public double ApartmentTotalArea { get; set; }

        public ApartmentTypeEnum ApartmentType { get; set; }

        public double RoomAreaWithCoef { get; set; }

        [JsonIgnore]
        public string ApartmentTypeAsString { get; set; }
    }
}