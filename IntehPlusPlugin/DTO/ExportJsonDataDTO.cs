﻿using IntehPlus.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace IntehPlus.Dto
{
    public class ExportJsonDataDTO
    {
        public string Name { get; set; }
        public Guid CooperationId { get; set; }
        public ModelTypeEnum ModelType { get; set; }
        public HeightMappingDTO HeightMapping { get; set; }
        public ProjectPointsDTO ConstructionContour { get; set; }
        public List<ApartmentJsonDTO> ConstructionMetaData { get; set; }
        public bool? IsFacade { get; set; }
    }
}
