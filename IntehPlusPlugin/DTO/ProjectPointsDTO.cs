﻿namespace IntehPlus.Dto
{
    public class ProjectPointsDTO
    {
        public double MaxX { get; set; }
        public double MaxY { get; set; }
        public double MinX { get; set; }
        public double MinY { get; set; }
    }
}
