﻿namespace IntehPlus.Dto
{
    public class HeightMappingDTO
    {
        public double MinPoint { get; set; }
        public double MaxPoint { get; set; }
    }
}
