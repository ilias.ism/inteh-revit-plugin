﻿using Newtonsoft.Json;

namespace IntehPlus.DTO
{
    public class ProjectSetDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
