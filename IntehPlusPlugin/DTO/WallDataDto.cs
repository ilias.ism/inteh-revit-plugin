﻿using IntehPlus.Enums;
using Newtonsoft.Json;

namespace IntehPlus.Dto
{
    public class WallDataDto
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public string HostElemId { get; set; }
    }
}