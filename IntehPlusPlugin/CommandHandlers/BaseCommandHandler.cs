﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace IntehPlus.CommandHandlers
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class BaseCommandHandler : IExternalCommand
    {
        protected Document doc;
        protected UIDocument uiDoc;
        protected UIApplication uiApp;
        public virtual Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            doc = commandData.Application.ActiveUIDocument.Document;
            uiDoc = commandData.Application.ActiveUIDocument;
            uiApp = commandData.Application;
            return Result.Succeeded;
        }
    }
}
