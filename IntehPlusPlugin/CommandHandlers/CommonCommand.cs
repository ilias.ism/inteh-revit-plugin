﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using IntehPlus.CommandHandlers;
using IntehPlus.Views;

namespace IntehPlus.Commands
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class CommonCommand : BaseCommandHandler
    {
        public override Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            base.Execute(commandData, ref message, elements);
            var view = new CommonMainView(doc, commandData);
            view.ShowDialog();
            return Result.Succeeded;
        }
    }
}
