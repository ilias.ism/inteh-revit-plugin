﻿using System;
using System.Collections.Generic;
using System.Text;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace IntehPlus.Interfaces
{
    public interface IBaseLogic
    {
        Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements);
    }
}
