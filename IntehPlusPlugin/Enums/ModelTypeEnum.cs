﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IntehPlus.Enums
{
    public enum ModelTypeEnum
    {
        FirstStage = 1,
        SecondStage = 2,
        TypoStage = 3,
        TechStage = 4,
        Roof = 5,
        Cottage = 6
    }
}
