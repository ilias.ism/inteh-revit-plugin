﻿using System.ComponentModel.DataAnnotations;

namespace IntehPlus.Enums
{
    public enum ConstructionTypeEnum
    {
        [Display(Name = "Нет типа")]
        NoType = 0,
        [Display(Name = "Меридианные")]
        Meridian = 1,
        [Display(Name = "Широтные")]
        Latitudinal = 2,
        [Display(Name = "Угловые")]
        Corner = 3,
        [Display(Name = "Башни")]
        Tower = 10
    }
}
