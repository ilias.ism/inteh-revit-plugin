﻿namespace IntehPlus.Enums
{
    public enum ApartmentTypeEnum
    {
        TechApp = -1,
        MOP = 0,
        App = 1,
        Pent = 2,
        Office = 3,
        Parking = 4,
        Pantry = 5,
        Garage = 6,
        GroundFloor = 7,
        Cottage = 8,
        Townhouse = 9,
        CottageDeluxe = 10,
        CottageComfort = 11,
        Duplex = 12,
        QuadroHouse = 13,
        SplitHouse = 14,
        Plot = 15,
        TechStage = 16,
        Motoparking = 17
    }
}
