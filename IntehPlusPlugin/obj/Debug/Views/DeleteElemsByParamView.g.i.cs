﻿#pragma checksum "..\..\..\Views\DeleteElemsByParamView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "3FD21713887B4FC020492F3A1CD1A9CFADA7F7530FBDD73C220D88913047050D"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace IntehPlus.Views {
    
    
    /// <summary>
    /// DeleteElemsByParamView
    /// </summary>
    public partial class DeleteElemsByParamView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\Views\DeleteElemsByParamView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ParamName;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\Views\DeleteElemsByParamView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ElemsCount;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\Views\DeleteElemsByParamView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Calc_Button;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\Views\DeleteElemsByParamView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ParamsText;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\Views\DeleteElemsByParamView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ParamValues;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\Views\DeleteElemsByParamView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SelectedValues;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\Views\DeleteElemsByParamView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Start_Button;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\Views\DeleteElemsByParamView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Close_Button;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/IntehPlusPlugin;component/views/deleteelemsbyparamview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\DeleteElemsByParamView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ParamName = ((System.Windows.Controls.ComboBox)(target));
            
            #line 10 "..\..\..\Views\DeleteElemsByParamView.xaml"
            this.ParamName.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ValidationTextBox);
            
            #line default
            #line hidden
            return;
            case 2:
            this.ElemsCount = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.Calc_Button = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\..\Views\DeleteElemsByParamView.xaml"
            this.Calc_Button.Click += new System.Windows.RoutedEventHandler(this.Calc_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.ParamsText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.ParamValues = ((System.Windows.Controls.ComboBox)(target));
            
            #line 14 "..\..\..\Views\DeleteElemsByParamView.xaml"
            this.ParamValues.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ParamValuesChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.SelectedValues = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.Start_Button = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\..\Views\DeleteElemsByParamView.xaml"
            this.Start_Button.Click += new System.Windows.RoutedEventHandler(this.Start_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.Close_Button = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\..\Views\DeleteElemsByParamView.xaml"
            this.Close_Button.Click += new System.Windows.RoutedEventHandler(this.Close_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

