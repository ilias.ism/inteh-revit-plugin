﻿#pragma checksum "..\..\..\Views\OVVKGroupModelView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "427F2EE4FFEB97BAA45ACC11896B7BEDE17F5FC79766C75058BABF696054069D"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace IntehPlus.Views {
    
    
    /// <summary>
    /// OVVKGroupModelView
    /// </summary>
    public partial class OVVKGroupModelView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 30 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Трубы;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Гибкие_трубы;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Изоляция_труб;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Арматура_труб;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Соединительные_детали_труб;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Оборудование;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Воздуховоды;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Гибкие_воздуховоды;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Изоляция_воздуховодов;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Арматура_воздуховодов;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Соединительные_детали_воздуховодов;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Воздухораспеделители;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Радиаторы;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Views\OVVKGroupModelView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Start_Button;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/IntehPlusPlugin;component/views/ovvkgroupmodelview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\OVVKGroupModelView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Трубы = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.Гибкие_трубы = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.Изоляция_труб = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.Арматура_труб = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.Соединительные_детали_труб = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.Оборудование = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.Воздуховоды = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.Гибкие_воздуховоды = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.Изоляция_воздуховодов = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.Арматура_воздуховодов = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.Соединительные_детали_воздуховодов = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.Воздухораспеделители = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.Радиаторы = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.Start_Button = ((System.Windows.Controls.Button)(target));
            
            #line 69 "..\..\..\Views\OVVKGroupModelView.xaml"
            this.Start_Button.Click += new System.Windows.RoutedEventHandler(this.Start_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

