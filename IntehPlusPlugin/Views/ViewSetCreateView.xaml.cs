﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class ViewSetCreateView : Window
    {
        Document _doc;
        ExternalCommandData _commandData;
        public ViewSetCreateView(Document doc, ExternalCommandData commandData)
        {
            _doc = doc;
            _commandData = commandData;
            InitializeComponent();
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            var viewSets = new FilteredElementCollector(_doc).OfClass(typeof(ViewSheetSet)).ToList();
            var name = NewViewSetName.Text;
            if (viewSets.Any(x => x.Name == name))
            {
                MessageBox.Show("Набор с таким именем уже существует");
                return;
            }

            var uiDoc = _commandData.Application.ActiveUIDocument;
            var selectedIds = uiDoc.Selection.GetElementIds().ToList();
            var selectedElems = new List<ViewSheet>();
            foreach (var id in selectedIds)
            {
                var elem = _doc.GetElement(id);
                if (elem.GetType().Name == "ViewSheet")
                    selectedElems.Add(elem as ViewSheet);
            }

            var printManager = _doc.PrintManager;
            if (printManager.PrintRange != PrintRange.Select)
                printManager.PrintRange = PrintRange.Select;

            var viewSheetSet = printManager.ViewSheetSetting;
            var viewSet = new ViewSet();
            foreach (var elem in selectedElems)
            {
                if (elem.CanBePrinted)
                    viewSet.Insert(elem);
            }

            viewSheetSet.CurrentViewSheetSet.Views = viewSet;
            CheckSetName(viewSheetSet);
            MessageBox.Show("Набор создан");
            Close();
        }

        private void CheckSetName(ViewSheetSetting viewSheetSetting)
        {
            var name = NewViewSetName.Text;
            Transaction transaction = new Transaction(_doc, "Удаление элементов");
            transaction.Start();
            viewSheetSetting.SaveAs(name);
            transaction.Commit();
        }

        private void ValidationTextBox(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(NewViewSetName.Text))
                Create_Button.IsEnabled = false;
            else
                Create_Button.IsEnabled = true;
        }
    }
}