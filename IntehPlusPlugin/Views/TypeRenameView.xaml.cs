﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Grid = System.Windows.Controls.Grid;

namespace IntehPlus.Views
{
    public partial class TypeRenameView : Window
    {
        private List<string> allCategories;
        private List<Element> allTypes;
        private string categoryFilter;
        private string typeFilter;
        private Document _doc;

        public TypeRenameView(Document doc)
        {
            InitializeComponent();
            _doc = doc;
            allCategories = GetAllCategories(doc);
            allTypes = GetAllTypes(doc, allCategories);
            SetViewElements();
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void SetViewElements()
        {
            var counter = 0;
            var grid = MainContentGrid;
            bool enableCatFilter = categoryFilter != "" && categoryFilter != null;
            bool enableTypeFilter = typeFilter != "" && typeFilter != null;
            List<Element> filterResultFirst = allTypes.Where(x => enableCatFilter ? x.Category.Name.ToLower().StartsWith(categoryFilter) : true).ToList();
            List<Element> filterResultSecond = filterResultFirst.Where(x => enableTypeFilter ? x.Name.ToLower().StartsWith(typeFilter) : true).ToList();

            foreach (var type in filterResultSecond)
            {
                RowDefinition newRow = new RowDefinition();
                grid.RowDefinitions.Add(newRow);

                TextBlock textBlock = CreateTypeTextBlock(type, "textBlock_" + counter.ToString());
                textBlock.ToolTip = type.Category.Name;
                System.Windows.Controls.TextBox textBox = CreateEmptyNewTypeTextBox("textBox_" + counter.ToString());

                grid.Children.Add(textBlock);
                grid.Children.Add(textBox);

                Grid.SetRow(textBlock, counter);
                Grid.SetColumn(textBlock, 0);
                Grid.SetRow(textBox, counter);
                Grid.SetColumn(textBox, 1);

                counter += 1;
            }
        }

        private TextBlock CreateTypeTextBlock(Element type, string name)
        {
            return new TextBlock
            {
                Name = name,
                Text = type.Name,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(10, 3, 0, 0),
            };
        }

        private System.Windows.Controls.TextBox CreateEmptyNewTypeTextBox(string name)
        {
            return new System.Windows.Controls.TextBox
            {
                Name = name,
                Margin = new Thickness(0, 3, 10, 0),
            };
        }

        private List<string> GetAllCategories(Document doc)
        {
            List<string> selectedCategories = new List<string>();
            Categories categories = doc.Settings.Categories;
            foreach (Category categorie in categories)
            {
                if (categorie.AllowsBoundParameters == true && categorie.CategoryType == CategoryType.Model)
                    selectedCategories.Add(categorie.Name);
            }
            return selectedCategories;
        }

        private List<Element> GetAllTypes(Document doc, List<string> categoryNames)
        {
            var allTypes = new FilteredElementCollector(doc).WhereElementIsElementType().ToList();
            var selectedTypes = allTypes.Where(x => x.Category != null && categoryNames.Contains(x.Category.Name)).ToList();
            return selectedTypes;
        }

        private void CategoryFilter(object sender, System.Windows.Input.KeyEventArgs e)
        {
            categoryFilter = CategoryFilterBox.Text.ToLower();
            var grid = MainContentGrid;
            grid.Children.Clear();
            SetViewElements();
        }

        private void TypeFilter(object sender, System.Windows.Input.KeyEventArgs e)
        {
            typeFilter = TypeFilterBox.Text.ToLower();
            var grid = MainContentGrid;
            grid.Children.Clear();
            SetViewElements();
        }

        private void Rename_Click(object sender, RoutedEventArgs e)
        {
            var grid = MainContentGrid.Children;
            var elements = grid.Cast<FrameworkElement>().ToList();
            var textBlocksUi = elements.Where(x => x.Name.StartsWith("textBlock"));
            var textBoxesUi = elements.Where(x => x.Name.StartsWith("textBox"));
            var textBlocks = textBlocksUi.Cast<TextBlock>().ToList();
            var textBoxes = textBoxesUi.Cast<System.Windows.Controls.TextBox>().ToList();

            var filledBoxes = textBoxes.Where(x => x.Text != null && x.Text != "").ToList();

            foreach (var filledBox in filledBoxes)
            {
                var nameAsNumber = filledBox.Name.Split('_')[1];
                var nameToRename = filledBox.Text;
                var correspondingTextBlockName = textBlocks[Convert.ToInt32(nameAsNumber)].Text.ToLower();
                var correspondingType = allTypes.Where(x => x.Name.ToLower() == correspondingTextBlockName).FirstOrDefault();
                RenameType(correspondingType, nameToRename);
            }
            MessageBox.Show("Выполнено");
            Close();
        }

        private void RenameType(Element type, string newName)
        {
            using (Transaction tx = new Transaction(_doc))
            {
                tx.Start("Transaction Name");
                ElementType elemess = type as ElementType;
                elemess.Name = newName;
                tx.Commit();
            }
        }
    }
}