﻿using Autodesk.Revit.DB;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class ClearWorkingSetView : Window
    {
        Document _doc;
        List<int> selectedWorksets = new List<int>();
        StackPanel stackPanel = null;
        List<Element> elemsList;
        public ClearWorkingSetView(Document doc)
        {
            _doc = doc;

            var list = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                            .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                            .Where(c => c.IsValidObject);
            elemsList = list.Where(c => !string.IsNullOrEmpty(c.Name)
                                && c.DemolishedPhaseId.IntegerValue == -1
                                && !string.IsNullOrEmpty(c.Category?.Name)).ToList();
            InitializeComponent();
            InitStackPanel();
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void InitStackPanel()
        {
            stackPanel = stackPanelMain;
            var workSets = new FilteredWorksetCollector(_doc).Where(x => x.Kind.ToString() == "UserWorkset").ToList();
            if (!workSets.Any())
            {
                MessageBox.Show("Рабочие наборы не найдены");
                return;
            }

            foreach (var workset in workSets)
            {
                var checkbox = new CheckBox
                {
                    Tag = workset.Id,
                    Content = workset.Name,
                    Margin = new Thickness(15, 2, 15, 0),
                    IsThreeState = false,
                };
                checkbox.Click += CheckBox_Click;
                stackPanel.Children.Add(checkbox);
            }
            Height += workSets.Count * 15;
            AddButtons(stackPanel);
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            if (checkBox.IsChecked == true)
            {
                selectedWorksets.Add(Int32.Parse(checkBox.Tag.ToString()));
            }
            else
            {
                selectedWorksets.Remove(Int32.Parse(checkBox.Tag.ToString()));
            }

            var buttons = stackPanel.Children.OfType<Button>().ToList();
            var startButton = buttons.FirstOrDefault(x => x.Name == "Start_Button");
            if (selectedWorksets.Any())
                startButton.IsEnabled = true;
            else
                startButton.IsEnabled = false;
        }

        private void AddButtons(StackPanel stackPanel)
        {
            var buttonStart = new Button
            {
                Margin = new Thickness(40, 10, 40, 0),
                Content = "Запуск",
                Height = 20,
                Name = "Start_Button",
                IsEnabled = false
            };
            buttonStart.Click += Start_Click;

            var buttonClose = new Button
            {
                Margin = new Thickness(40, 10, 40, 0),
                Content = "Закрыть",
                Height = 20,
                Name = "Close_Button",
                IsEnabled = true
            };
            buttonClose.Click += Close_Click;

            stackPanel.Children.Add(buttonStart);
            stackPanel.Children.Add(buttonClose);
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            var success = 0;
            var fail = 0;
            Transaction transaction = new Transaction(_doc, "Удаление элементов");
            transaction.Start();
            foreach (var elem in elemsList)
            {
                try
                {
                    var worksetId = elem.WorksetId;
                    if (selectedWorksets.Contains(Int32.Parse(worksetId.ToString())))
                    {
                        try
                        {
                            _doc.Delete(elem.Id);
                            success += 1;
                        }
                        catch (Exception ex)
                        {
                            fail += 1;
                        }
                    }
                }
                catch
                {

                }
            }
            transaction.Commit();
            MessageBox.Show("Успешно удалено: " + success.ToString() + " не получилось удалить: " + fail.ToString());
            Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}