﻿using Autodesk.Revit.DB;
using ClosedXML.Excel;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class ElectricalMainView : Window
    {
        readonly Document _doc;

        public ElectricalMainView(Document doc)
        {
            InitializeComponent();
            _doc = doc;
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SetChain_Click(object sender, RoutedEventArgs e)
        {
            var elemsList = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                            .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                            .Where(c => c.IsValidObject)
                            .Where(c => !string.IsNullOrEmpty(c.Name)
                                && c.DemolishedPhaseId.IntegerValue == -1
                                && !string.IsNullOrEmpty(c.Category?.Name)).ToList();

            Transaction transaction = new Transaction(_doc, "Назначение цепей элементов");
            transaction.Start();

            var totalSet = 0;
            foreach (var elem in elemsList)
            {
                if (elem.Name.Contains("видеокамера"))
                {
                    var chainParams = elem.GetParameters("Номер цепи");
                    var rbzChainParams = elem.GetParameters("RBZ_Наименование линии");
                    if (chainParams.Any() && rbzChainParams.Any())
                    {
                        var chainValue = chainParams.First().AsString();
                        if (!string.IsNullOrEmpty(chainValue))
                        {
                            var rbzChainParam = rbzChainParams.First();
                            rbzChainParam.Set(chainValue);
                            totalSet += 1;
                        }
                    }
                }
            }

            transaction.Commit();
            MessageBox.Show("Всего проверено элементов: " + elemsList.Count.ToString() + " назначено значений RBZ наим. линии для элементов с видеокамера в имеин типа " + totalSet.ToString());
            Close();
        }

        private void AGSK_Node_Excel_Click(object sender, RoutedEventArgs e)
        {
            var allElems = new FilteredElementCollector(_doc).OfCategory(BuiltInCategory.OST_DetailComponents)
                .WhereElementIsNotElementType().ToElements().Where(x => x.Category.Name == "Элементы узлов").ToList();
            var result = new List<ElectroAGSKDto>();
            foreach (var elem in allElems)
            {
                var comment = elem.GetParameters("Комментарии").First().AsString();
                var amperageParam = elem.GetParameters("Номинальный ток расцепителя");
                var amperage = "";
                if (amperageParam.Any())
                    amperage = amperageParam.First().AsValueString();
                var category = elem.Category.Name;
                var type = _doc.GetElement(elem.GetTypeId());
                var typeName = type.Name;
                var family = type as ElementType;
                var dto = new ElectroAGSKDto()
                {
                    CategoryName = category,
                    FamilyName = family.FamilyName,
                    TypeName = typeName,
                    Amperage = amperage,
                    Comment = comment,
                };
                result.Add(dto);
            }

            IXLWorkbook workbook = new XLWorkbook();
            IXLWorksheet worksheet = workbook.Worksheets.Add("Лист 1");
            worksheet.Cell(1, 1).Value = "Категория";
            worksheet.Cell(1, 2).Value = "Семейство";
            worksheet.Cell(1, 3).Value = "Имя типа";
            worksheet.Cell(1, 4).Value = "Ток";
            worksheet.Cell(1, 5).Value = "Комментарий";

            var counter = 2;
            var groupedByCategory = result.GroupBy(x => x.CategoryName).ToList();
            foreach (var groupByCat in groupedByCategory)
            {
                var groupedByFamily = groupByCat.GroupBy(x => x.FamilyName).ToList();
                foreach (var groupByFamily in groupedByFamily)
                {
                    var groupedByType = groupByFamily.GroupBy(x => x.TypeName).ToList();
                    foreach (var groupByType in groupedByType)
                    {
                        var groupedByAmperage = groupByType.GroupBy(x => x.Amperage).ToList();
                        foreach (var groupByAmperage in groupedByAmperage)
                        {
                            worksheet.Cell(counter, 1).Value = groupByCat.Key;
                            worksheet.Cell(counter, 2).Value = groupByFamily.Key;
                            worksheet.Cell(counter, 3).Value = groupByType.Key;
                            worksheet.Cell(counter, 4).Value = groupByAmperage.Key;
                            var value = groupByAmperage.FirstOrDefault(x => !string.IsNullOrEmpty(x.Comment));
                            worksheet.Cell(counter, 5).Value = value == null ? "" : value.Comment;
                            counter += 1;
                        }
                    }
                }
            }

            var saveFileDialog = new System.Windows.Forms.SaveFileDialog
            {
                Filter = "Excel Files(.xlsx)| *.xlsx",
                FileName = _doc.Title.Replace(".rvt", "")
            };
            if (saveFileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            workbook.SaveAs(saveFileDialog.FileName);
            Close();
        }

        private void AGSK_Node_Set_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog
            {
                Filter = "Excel Files(.xlsx)| *.xlsx"
            };
            var result = dialog.ShowDialog();
            if (result != true)
                Close();

            var dtoList = new List<ElectroAGSKDto>();
            try
            {
                using (var workbook = new XLWorkbook(dialog.FileName))
                {
                    IXLWorksheet worksheet = workbook.Worksheets.First();
                    var firstCell = worksheet.FirstCellUsed();
                    var lastCell = worksheet.LastCellUsed();

                    int startRow = firstCell.Address.RowNumber;
                    int endRow = lastCell.Address.RowNumber;
                    int startColumn = firstCell.Address.ColumnNumber;
                    int endColumn = lastCell.Address.ColumnNumber;

                    for (int row = startRow + 1; row <= endRow; row++)
                    {
                        var category = worksheet.Cell(row, 1).Value.ToString();
                        var family = worksheet.Cell(row, 2).Value.ToString();
                        var type = worksheet.Cell(row, 3).Value.ToString();
                        var amperage = worksheet.Cell(row, 4).Value.ToString();
                        var value = worksheet.Cell(row, 5).Value.ToString();
                        var dto = new ElectroAGSKDto()
                        {
                            CategoryName = category,
                            FamilyName = family,
                            TypeName = type,
                            Amperage = amperage,
                            Comment = value,
                        };
                        dtoList.Add(dto);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Проверьте закрыт ли файл и правильный ли файл используется");
                return;
            }

            var allElems = new FilteredElementCollector(_doc).OfCategory(BuiltInCategory.OST_DetailComponents)
                .WhereElementIsNotElementType().ToElements().Where(x => x.Category.Name == "Элементы узлов").ToList();
            var errorList = new List<int>();
            using (Transaction tra = new Transaction(_doc))
            {
                tra.Start("Назначение кодов");
                foreach (var elem in allElems)
                {
                    var comment = elem.GetParameters("Комментарии");
                    var amperageParam = elem.GetParameters("Номинальный ток расцепителя");
                    var amperage = "";
                    if (amperageParam.Any())
                        amperage = amperageParam.First().AsValueString();
                    var category = elem.Category.Name;
                    var type = _doc.GetElement(elem.GetTypeId());
                    var typeName = type.Name;
                    var family = type as ElementType;

                    var currentDto = dtoList.Where(x => x.CategoryName == category &&
                        x.TypeName == typeName && x.FamilyName == family.FamilyName
                        && x.Amperage == amperage).FirstOrDefault();
                    if (currentDto != null && comment.Any())
                        try
                        {
                            comment.First().Set(currentDto.Comment);
                        }
                        catch
                        {
                            errorList.Add(elem.Id.IntegerValue);
                        }
                }
                tra.Commit();
            }
            var error = "";
            if (errorList.Any())
                error = "Ошибка заполнения в следующих элементах: " + string.Join(", ", errorList.ToArray());;
            MessageBox.Show("Добавлено." + error);
            Close();
        }
    }

    public class ElectroAGSKDto
    {
        public string CategoryName { get; set; }
        public string FamilyName { get; set; }
        public string TypeName { get; set; }
        public string Amperage { get; set; }
        public string Comment { get; set; }
    }
}
