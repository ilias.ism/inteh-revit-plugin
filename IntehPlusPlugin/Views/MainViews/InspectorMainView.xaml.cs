﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ClosedXML.Excel;
using IntehPlus.Dto;
using IntehPlus.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace IntehPlus.Views
{
    public partial class InspectorMainView : Window
    {
        readonly Document _doc;
        ExternalCommandData _commandData;
        Microsoft.Win32.OpenFileDialog _dialog;

        public InspectorMainView(Document doc, ExternalCommandData commandData)
        {
            _dialog = new Microsoft.Win32.OpenFileDialog
            {
                Filter = "Excel Files(.xlsx)| *.xlsx"
            };
            var result = _dialog.ShowDialog();
            if (result == true)
            {
                InitializeComponent();
                _doc = doc;
                _commandData = commandData;
                KeyUp += MainWindow_KeyUp;
                EnableLevels();
            }
            else
                Close();
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void EnableLevels()
        {
            var dtoList = new List<InspectorInsertDto>();
            try
            {
                using (var workbook = new XLWorkbook(_dialog.FileName))
                {
                    IXLWorksheet worksheet = workbook.Worksheets.First();
                    var firstCell = worksheet.FirstCellUsed();
                    var lastCell = worksheet.LastCellUsed();

                    int startRow = firstCell.Address.RowNumber;
                    int endRow = lastCell.Address.RowNumber;
                    int startColumn = firstCell.Address.ColumnNumber;
                    int endColumn = lastCell.Address.ColumnNumber;

                    for (int row = startRow + 1; row <= endRow; row++)
                    {
                        var category = worksheet.Cell(row, 1).Value.ToString();
                        var condition = worksheet.Cell(row, 2).Value.ToString();
                        var value = worksheet.Cell(row, 3).Value.ToString();
                        var level = worksheet.Cell(row, 4).Value.ToString();
                        var dto = new InspectorInsertDto()
                        {
                            Category = category,
                            Condition = condition,
                            Value = value,
                            Level = level,
                        };
                        dtoList.Add(dto);
                    }
                }
            }
            catch
            {
                MessageBox.Show("в процессе чтения excel произошла ошибка");
                throw new Exception();
            }

            var levelsList = dtoList.Select(x => x.Level).ToList();
            var convertedLevels = ConvertLevels(levelsList);
            AddLevels(convertedLevels);
            AddButtons(StackPanelMain);
        }

        private void AddLevels(List<int> convertedLevels)
        {
            var revitLevels = RoomUtils.GetAllLevels(_doc).Select(x => x.Name).ToList();
            var ordered = convertedLevels.Distinct().OrderBy(x => x).ToList();
            var mainGrid = GridMain;
            var rowCounter = 0;
            foreach (var convertedLevel in ordered)
            {
                mainGrid.RowDefinitions.Add(new RowDefinition());
                var textBlock = new TextBlock();
                textBlock.Text = convertedLevel.ToString();
                textBlock.Margin = new Thickness(15, 0, 15, 0);
                System.Windows.Controls.Grid.SetRow(textBlock, rowCounter);
                System.Windows.Controls.Grid.SetColumn(textBlock, 0);
                mainGrid.Children.Add(textBlock);

                var comboBox = new System.Windows.Controls.ComboBox();
                comboBox.ItemsSource = revitLevels;
                comboBox.Margin = new Thickness(15, 0, 15, 0);
                comboBox.ToolTip = convertedLevel;
                System.Windows.Controls.Grid.SetRow(comboBox, rowCounter);
                System.Windows.Controls.Grid.SetColumn(comboBox, 1);
                mainGrid.Children.Add(comboBox);
                rowCounter += 1;
            }
        }

        private void AddButtons(StackPanel stackPanel)
        {
            var buttonStart = new Button
            {
                Margin = new Thickness(40, 10, 40, 0),
                Content = "Запуск",
                Height = 20,
                Name = "Start_Button",
            };
            buttonStart.Click += Start_Click;
            stackPanel.Children.Add(buttonStart);
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            var comboboxes = GetAllComboBoxes(this);
            var levelMatches = new List<ComboboxMatchesDto>();
            foreach (var elem in comboboxes)
            {
                var selectedItem = elem.SelectedValue?.ToString();
                var tooltip = elem.ToolTip?.ToString();
                if (string.IsNullOrEmpty(selectedItem) || string.IsNullOrEmpty(tooltip))
                {
                    MessageBox.Show("Не все поля заполнены");
                    return;
                }
                levelMatches.Add(new ComboboxMatchesDto()
                {
                    Level = selectedItem,
                    RevitLevel = tooltip,
                });
            }

            var list = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                .Where(c => c.IsValidObject);
            var elemsList = list.Where(c => !string.IsNullOrEmpty(c.Name)
                                && c.DemolishedPhaseId.IntegerValue == -1
                                && !string.IsNullOrEmpty(c.Category?.Name)).ToList();
        }

        public static List<System.Windows.Controls.ComboBox> GetAllComboBoxes(DependencyObject parent)
        {
            var comboBoxes = new List<System.Windows.Controls.ComboBox>();
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                if (child is System.Windows.Controls.ComboBox comboBox)
                {
                    comboBoxes.Add(comboBox);
                }
                comboBoxes.AddRange(GetAllComboBoxes(child));
            }
            return comboBoxes;
        }

        private List<int> ConvertLevels(List<string> levels)
        {
            List<int> result = new List<int>();
            foreach (string level in levels)
            {
                if (level.StartsWith("-"))
                {
                    if (int.TryParse(level, out int singleNumber))
                        result.Add(singleNumber);
                    else
                    {
                        MessageBox.Show("Не получилось конвертировать этажи в числа из excel");
                        throw new Exception();
                    }
                }
                else
                {
                    var levelTrimed = level.Trim().Replace(" ", "");
                    string[] parts = levelTrimed.Split('-');
                    if (parts.Length == 1)
                    {
                        if (int.TryParse(parts[0], out int singleNumber))
                            result.Add(singleNumber);
                        else
                        {
                            MessageBox.Show("Не получилось конвертировать этажи в числа из excel");
                            throw new Exception();
                        }
                    }
                    else if (parts.Length == 2)
                    {
                        if (int.TryParse(parts[0], out int start) && int.TryParse(parts[1], out int end))
                        {
                            for (int i = start; i <= end; i++)
                            {
                                result.Add(i);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Не получилось конвертировать этажи в числа из excel");
                            throw new Exception();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не получилось конвертировать этажи в числа из excel");
                        throw new Exception();
                    }
                }
            }

            return result;

        }
    }
}
