﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class SuperFilterMainView : Window
    {
        Document _doc;
        ExternalCommandData _commandData;
        List<Element> _allSelectedElems = new List<Element>();
        List<CatTypes> _tree = new List<CatTypes>();
        public ObservableCollection<TreeNode> TreeNodes { get; } = new ObservableCollection<TreeNode>();
        public SuperFilterMainView(Document doc, ExternalCommandData commandData)
        {
            InitializeComponent();
            _doc = doc;
            _commandData = commandData;
            DataContext = this;
            InitElems();
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void InitElems()
        {
            _tree.Clear();
            var uiDoc = _commandData.Application.ActiveUIDocument;
            var selectedIds = uiDoc.Selection.GetElementIds().ToList();
            foreach (var id in selectedIds)
            {
                var elem = _doc.GetElement(id);
                _allSelectedElems.Add(elem);
                var category = elem.Category.Name;
                var type = _doc.GetElement(elem.GetTypeId())?.Name;
                //var qwe = elem.
                if (!_tree.Any(x => x.Category == category))
                {
                    var listTypes = new List<Types>
                    {
                        new Types()
                        {
                            Type = type,
                            TypeInstancesIds = new List<ElementId>() { id },
                            Count = 1,
                        }
                    };
                    _tree.Add(new CatTypes()
                    {
                        Category = category,
                        Types = listTypes,
                        Count = 1,
                    });

                }
                else
                {
                    var currentCat = _tree.First(x => x.Category == category);
                    if (!currentCat.Types.Any(x => x.Type == type))
                    {
                        currentCat.Count += 1;
                        currentCat.Types.Add(new Types()
                        {
                            Type = type,
                            Count = 1,
                            TypeInstancesIds = new List<ElementId>() { id },
                        });
                    }
                    else
                    {
                        var currentType = currentCat.Types.First(x => x.Type == type);
                        currentType.TypeInstancesIds.Add(id);
                        currentCat.Count += 1;
                        currentType.Count += 1;
                    }
                }
            }

            var rootNode = new TreeNode { Name = "Выбрать все (" + _allSelectedElems.Count.ToString() + ")", Level = 1 };
            foreach (var category in _tree.OrderBy(x => x.Category))
            {
                var subNode = new TreeNode { Name = category.Category + " (" + category.Count.ToString() + ")", Level = 2 };
                subNode.SetParent(rootNode);
                foreach (var type in category.Types)
                {
                    var children = new TreeNode
                    {
                        Name = type.Type + " (" + type.Count.ToString() + ")",
                        Level = 3,
                        TypeInstancesIds = type.TypeInstancesIds,
                    };
                    children.SetParent(subNode);
                    subNode.Children.Add(children);
                }
                rootNode.Children.Add(subNode);
            }

            TreeNodes.Add(rootNode);
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            List<TreeNode> allNodes = GetAllNodes(TreeNodes);
            var selectedTypes = allNodes.Where(x => x.Level == 3 && x.IsChecked).ToList();
            var result = new List<ElementId>();
            foreach (var type in selectedTypes)
            {
                result.AddRange(type.TypeInstancesIds);
            }
            _commandData.Application.ActiveUIDocument.Selection.SetElementIds(result);
            Close();
        }

        private List<TreeNode> GetAllNodes(IEnumerable<TreeNode> nodes)
        {
            List<TreeNode> allNodes = new List<TreeNode>();
            foreach (var node in nodes)
            {
                allNodes.Add(node);
                if (node.Children.Count > 0)
                    allNodes.AddRange(GetAllNodes(node.Children));
            }
            return allNodes;
        }
    }

    public class TreeNode : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private bool Up = false;
        private bool isChecked;
        public int Level { get; set; }
        public List<ElementId> TypeInstancesIds { get; set; }
        public string Name { get; set; }
        public bool IsChecked
        {
            get { return isChecked; }
            set
            {
                if (isChecked != value)
                {
                    int fromLevel = 0;
                    if (Level == 3)
                        fromLevel = 3;
                    isChecked = value;
                    OnPropertyChanged(nameof(IsChecked));
                    SetParentsChecked(value, fromLevel);
                    if (!Up)
                    SetChildrenChecked(value, fromLevel);
                }
            }
        }

        public ObservableCollection<TreeNode> Children { get; } = new ObservableCollection<TreeNode>();

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetChildrenChecked(bool value, int fromLevel = 0)
        {
            foreach (var child in Children)
            {
                child.IsChecked = value;
                // Рекурсивно вызываем SetChildrenChecked для всех дочерних элементов
                child.SetChildrenChecked(value, fromLevel);
            }
        }

        private void SetParentsChecked(bool value, int fromLevel = 0)
        {
            if (Parent != null && !value)
            {
                Parent.Up = true;
                Parent.IsChecked = value;
                Parent.Up = false;
                Parent.SetParentsChecked(value, fromLevel);
            }
        }

        public TreeNode Parent { get; private set; }
        public void SetParent(TreeNode parent)
        {
            Parent = parent;
        }
    }


    public class CatTypes
    {
        public string Category { get; set; }
        public List<Types> Types { get; set; }
        public int Count { get; set; }
    }

    public class Types
    {
        public string Type { get; set; }
        public List<ElementId> TypeInstancesIds { get; set; }
        public int Count { get; set; }
    }
}
