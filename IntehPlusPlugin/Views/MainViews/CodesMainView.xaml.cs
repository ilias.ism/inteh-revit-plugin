﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ClosedXML.Excel;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using static IntehPlus.Views.CodesExcelModelView;

namespace IntehPlus.Views
{
    public partial class CodesMainView : Window
    {
        readonly Document _doc;
        readonly ExternalCommandData _commandData;

        public CodesMainView(Document doc, ExternalCommandData commandData)
        {
            InitializeComponent();
            _doc = doc;
            _commandData = commandData;
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Excel_Click(object sender, RoutedEventArgs e)
        {
            var view = new CodesExcelModelView(_doc);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void Insert_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog
            {
                Filter = "Excel Files(.xlsx)| *.xlsx"
            };
            var result = dialog.ShowDialog();
            if (result != true)
                Close();

            var dtoList = new List<ElemsDto>();
            try
            {
                using (var workbook = new XLWorkbook(dialog.FileName))
                {
                    IXLWorksheet worksheet = workbook.Worksheets.First();
                    var firstCell = worksheet.FirstCellUsed();
                    var lastCell = worksheet.LastCellUsed();

                    int startRow = firstCell.Address.RowNumber;
                    int endRow = lastCell.Address.RowNumber;
                    int startColumn = firstCell.Address.ColumnNumber;
                    int endColumn = lastCell.Address.ColumnNumber;

                    for (int row = startRow + 1; row <= endRow; row++)
                    {
                        var category = worksheet.Cell(row, 1).Value.ToString();
                        var family = worksheet.Cell(row, 2).Value.ToString();
                        var type = worksheet.Cell(row, 3).Value.ToString();
                        var value = worksheet.Cell(row, 4).Value.ToString();
                        var dto = new ElemsDto()
                        {
                            CategoryName = category,
                            FamilyName = family,
                            TypeName = type,
                            CodeValue = value,
                        };
                        dtoList.Add(dto);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Проверьте закрыт ли файл и правильный ли файл используется");
                return;
            }

            var list = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                            .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                            .Where(c => c.IsValidObject);
            List<Element> elemsList = list.Where(c => !string.IsNullOrEmpty(c.Name)
                                && c.DemolishedPhaseId.IntegerValue == -1
                                && !string.IsNullOrEmpty(c.Category?.Name)).ToList();

            var elemTypesIds = elemsList.GroupBy(x => x.GetTypeId());
            using (Transaction tra = new Transaction(_doc))
            {
                tra.Start("Назначение кодов");
                foreach (var elemTypeId in elemTypesIds)
                {
                    var type = _doc.GetElement(elemTypeId.Key);
                    if (type != null)
                    {
                        var parameter = type.GetParameters("BI_код_оборудования");
                        if (parameter.Any())
                        {
                            var family = type as ElementType;
                            var currentDto = dtoList.Where(x => x.CategoryName == type.Category.Name &&
                                x.TypeName == type.Name &&
                                x.FamilyName == family.FamilyName).FirstOrDefault();
                            if (currentDto != null)
                                try
                                {
                                    parameter.First().Set(currentDto.CodeValue);
                                }
                                catch
                                {
                                    var q = 1;
                                }
                        }
                    }
                }

                foreach (var elem in elemsList)
                {
                    var parameter = elem.GetParameters("BI_код_оборудования");
                    var typeId = elem.GetTypeId();
                    if (typeId != null && parameter.Any())
                    {
                        var type = _doc.GetElement(typeId);
                        if (type != null)
                        {
                            var family = type as ElementType;
                            var currentDto = dtoList.Where(x => x.CategoryName == type.Category.Name &&
                                x.TypeName == type.Name &&
                                x.FamilyName == family.FamilyName).FirstOrDefault();
                            if (currentDto != null)
                                try
                                {
                                    parameter.First().Set(currentDto.CodeValue);
                                }
                                catch
                                {
                                    var q = 1;
                                }
                        }
                    }
                }
                tra.Commit();
            }
            MessageBox.Show("Добавлено");
            Close();
        }

        private void SetBiCode_Click(object sender, RoutedEventArgs e)
        {
            var list = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                            .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                            .Where(c => c.IsValidObject);
            List<Element> elemsList = list.Where(c => !string.IsNullOrEmpty(c.Name)
                                && c.DemolishedPhaseId.IntegerValue == -1
                                && !string.IsNullOrEmpty(c.Category?.Name)).ToList();

            var grouped = elemsList.GroupBy(x => x.GetTypeId()).ToList();
            DefinitionFile defFile = _commandData.Application.Application.OpenSharedParameterFile();
            DefinitionGroups myGroups = defFile.Groups;
            DefinitionGroup myGroup = myGroups.get_Item("04_Инженерные системы");
            Definitions myDefinitions = myGroup.Definitions;
            ExternalDefinition eDef = myDefinitions.get_Item("BI_код_оборудования") as ExternalDefinition;
            var count = 0;
            foreach (var group in grouped)
            {
                var elemOfGroup = group.First();
                var type = _doc.GetElement(elemOfGroup.GetTypeId());
                if (type != null && !type.GetParameters("BI_код_оборудования").Any())
                {
                    var familyInstance = elemOfGroup as FamilyInstance;
                    if (familyInstance != null)
                    {
                        Family family = familyInstance.Symbol.Family;
                        var familyDoc = _doc.EditFamily(family);
                        try
                        {
                            using (Transaction transaction = new Transaction(familyDoc, "BI_код_оборудования добавление"))
                            {
                                transaction.Start();
                                if (familyInstance.GetParameters("BI_код_оборудования").Any())
                                {
                                    var param = familyDoc.FamilyManager.get_Parameter("BI_код_оборудования");
                                    if (param != null && param.IsInstance)
                                    {
                                        if (elemOfGroup.Category.Name == "Соединительные детали трубопроводов" ||
                                            elemOfGroup.Category.Name == "Соединительные детали воздуховодов")
                                        {
                                            familyDoc.FamilyManager.RemoveParameter(param);
                                            familyDoc.FamilyManager.AddParameter(eDef, BuiltInParameterGroup.PG_TEXT, true);
                                        }
                                        else
                                        {
                                            familyDoc.FamilyManager.RemoveParameter(param);
                                            familyDoc.FamilyManager.AddParameter(eDef, BuiltInParameterGroup.PG_TEXT, false);
                                        }
                                    }
                                    else
                                    {
                                        if (elemOfGroup.Category.Name == "Соединительные детали трубопроводов" ||
                                            elemOfGroup.Category.Name == "Соединительные детали воздуховодов")
                                        {
                                            familyDoc.FamilyManager.AddParameter(eDef, BuiltInParameterGroup.PG_TEXT, true);
                                        }
                                        else
                                        {
                                            familyDoc.FamilyManager.AddParameter(eDef, BuiltInParameterGroup.PG_TEXT, false);
                                        }
                                    }
                                }
                                else
                                {
                                    if (elemOfGroup.Category.Name == "Соединительные детали трубопроводов" ||
                                        elemOfGroup.Category.Name == "Соединительные детали воздуховодов")
                                    {
                                        familyDoc.FamilyManager.AddParameter(eDef, BuiltInParameterGroup.PG_TEXT, true);
                                    }
                                    else
                                    {
                                        familyDoc.FamilyManager.AddParameter(eDef, BuiltInParameterGroup.PG_TEXT, false);
                                    }
                                }
                                transaction.Commit();
                                familyDoc.LoadFamily(_doc, new FamilyLoadOptions());
                                familyDoc.Close(false);
                                count += 1;
                            }
                        }
                        catch
                        {
                            familyDoc.Close(false);
                        }
                    }
                    else
                    {
                        if (!type.GetParameters("BI_код_оборудования").Any())
                        {
                            //using (Transaction transaction = new Transaction(_doc, "BI_код_оборудования добавление"))
                            //{
                            //    transaction.Start();

                            //    var bindingMap = _doc.ParameterBindings;
                            //    var binding = _doc.ParameterBindings.get_Item(eDef);
                            //    if (binding == null)
                            //    {
                            //        var allCats = _doc.Settings.Categories;
                            //        var categorySet = _commandData.Application.Application.Create.NewCategorySet();
                            //        foreach (var cat in allCats)
                            //        {
                            //            if ((cat as Category).CategoryType == CategoryType.Model &&
                            //                (cat as Category).AllowsBoundParameters)
                            //                categorySet.Insert(cat as Category);
                            //        }

                            //        Binding newBinding = _commandData.Application.Application.Create.NewTypeBinding(categorySet);
                            //        bindingMap.Insert(eDef, newBinding, BuiltInParameterGroup.PG_TEXT);
                            //}
                            //        else if (binding.GetType() == typeof(InstanceBinding))
                            //        {
                            //            bindingMap.Remove(eDef);
                            //            var allCats = _doc.Settings.Categories;
                            //            var categorySet = _commandData.Application.Application.Create.NewCategorySet();
                            //            foreach (var cat in allCats)
                            //            {
                            //                if ((cat as Category).CategoryType == CategoryType.Model &&
                            //                    (cat as Category).AllowsBoundParameters)
                            //                    categorySet.Insert(cat as Category);
                            //            }

                            //            Binding newBinding = _commandData.Application.Application.Create.NewTypeBinding(categorySet);
                            //            bindingMap.Insert(eDef, newBinding, BuiltInParameterGroup.PG_TEXT);
                            //        }
                            //        else
                            //        {
                            //            var catSet = (binding as TypeBinding).Categories;
                            //            catSet.Insert(type.Category);
                            //            Binding newBinding = _commandData.Application.Application.Create.NewTypeBinding(catSet);
                            //            bindingMap.Insert(eDef, newBinding, BuiltInParameterGroup.PG_TEXT);
                            //        }
                            //    transaction.Commit();
                            //}
                        }
                    }
                }
            }

            MessageBox.Show("Добавлено в " + count.ToString() + " семейств");
            Close();
        }

        class FamilyLoadOptions : IFamilyLoadOptions
        {
            public bool OnFamilyFound(bool familyInUse, out bool overwriteParameterValues)
            {
                overwriteParameterValues = true;
                return true;
            }

            public bool OnSharedFamilyFound(Family sharedFamily, bool familyInUse, out FamilySource source, out bool overwriteParameterValues)
            {
                source = FamilySource.Family;
                overwriteParameterValues = true;
                return true;
            }
        }
    }
}
