﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using IntehPlusPlugin.DTO;
using IntehPlusPlugin.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class ArchitectureMainView : Window
    {
        readonly Document _doc;
        ExternalCommandData _commandData;
        List<string> whiteList = new List<string>() { "бетон", "кирпич", "газоблок", "утеплит" };

        public ArchitectureMainView(Document doc, ExternalCommandData commandData)
        {
            InitializeComponent();
            _doc = doc;
            _commandData = commandData;
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Room_Finishing_Click(object sender, RoutedEventArgs e)
        {
            var view = new RoomFinishingView(_doc);
            view.ShowDialog();
            Close();
        }

        private void Wall_Compound_Click(object sender, RoutedEventArgs e)
        {
            TryToJoinWalls();
            Close();
        }

        private void TryToJoinWalls()
        {
            var allWalls = new FilteredElementCollector(_doc)
                        .OfCategory(BuiltInCategory.OST_Walls).WhereElementIsNotElementType().ToElements();
            var finishing = allWalls.Where(x => x.Name.ToLower().Contains("отделк")).GroupBy(x => x.LevelId).ToList();
            var otherWallsFromWhiteList = allWalls.Where(x => !x.Name.ToLower().Contains("отделк") && (x.Name.ToLower().Contains("внутренн") || x.Name.ToLower().Contains("наружн") || x.Name.ToLower().Contains("утепл"))).GroupBy(x => x.LevelId).ToList();
            using (Transaction transaction = new Transaction(_doc))
            {
                var failureHandlingOptions = transaction.GetFailureHandlingOptions();

                failureHandlingOptions.SetFailuresPreprocessor(new JoinsFailurePreprocessor());

                transaction.SetFailureHandlingOptions(failureHandlingOptions);
                transaction.Start("change join");
                foreach (var finishGroup in finishing)
                {
                    foreach (var otherWallsGroup in otherWallsFromWhiteList)
                    {
                        if (finishGroup.FirstOrDefault().LevelId == otherWallsGroup.FirstOrDefault().LevelId)
                        {
                            foreach (var finishWall in finishGroup)
                            {
                                foreach (var otherWall in otherWallsGroup)
                                {
                                    try
                                    {
                                        if (whiteList.Any(x => finishWall.Name.ToLower().Contains(x) && otherWall.Name.ToLower().Contains(x) && !JoinGeometryUtils.AreElementsJoined(_doc, finishWall, otherWall)))
                                        {
                                            JoinGeometryUtils.JoinGeometry(_doc, finishWall, otherWall);
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
                transaction.Commit();
            }

            MessageBox.Show("Выполнено");
            Close();
        }

        private void Wall_Clip_Click(object sender, RoutedEventArgs e)
        {
            var allWalls = new FilteredElementCollector(_doc)
                        .OfCategory(BuiltInCategory.OST_Walls).WhereElementIsNotElementType().ToElements()
                        .Where(x => !x.Name.ToLower().Contains("витраж"));

            var allColumns = new FilteredElementCollector(_doc)
                        .OfCategory(BuiltInCategory.OST_StructuralColumns).WhereElementIsNotElementType().ToElements()
                        .Where(x => !x.Name.ToLower().Contains("железобетон")); ;

            if (CheckParameterExists(allWalls.FirstOrDefault()))
                return;

            using (Transaction tra = new Transaction(_doc))
            {
                tra.Start("Расчет соединений стен");

                //длина / 1,5
                var op1 = allWalls.Where(x => x.Name.ToLower().Contains("газоблок")
                                        && (_doc.GetElement(x.GetTypeId()) as ElementType)
                                        .LookupParameter("Ширина").AsDouble() * 304.8 > 99).ToList();
                foreach (var wall in op1)
                {
                    var length = wall.LookupParameter("Длина").AsDouble() * 304.8 / 1000;
                    var op1Param = wall.LookupParameter("Оп-1").Set(length / 1.5);
                }

                //примыкает к несущей колонне с именем типа железобетон
                //неприсоед высота / 3
                var op2 = allWalls.Where(x => x.Name.ToLower().Contains("газоблок")
                            && (_doc.GetElement(x.GetTypeId()) as ElementType)
                            .LookupParameter("Ширина").AsDouble() * 304.8 > 99
                            && allColumns.Any(y => CheckTouchingColumn(x, y))).ToList();
                foreach (var wall in op2)
                {
                    var height = wall.LookupParameter("Неприсоединенная высота").AsDouble() * 304.8 / 1000;
                    var op1Param = wall.LookupParameter("Оп-2").Set(height / 3);
                }

                // длина / 1,5
                var op3 = allWalls.Where(x => x.Name.ToLower().Contains("кирпич")
                                        && (_doc.GetElement(x.GetTypeId()) as ElementType)
                                        .LookupParameter("Ширина").AsDouble() * 304.8 > 119).ToList();
                foreach (var wall in op3)
                {
                    var length = wall.LookupParameter("Длина").AsDouble() * 304.8 / 1000;
                    var op1Param = wall.LookupParameter("Оп-3").Set(length / 1.5);
                }

                //примыкает к несущей колонне с именем типа железобетон
                // неприсоед выс / 3
                var op4 = allWalls.Where(x => x.Name.ToLower().Contains("кирпич")
                                        && (_doc.GetElement(x.GetTypeId()) as ElementType)
                                        .LookupParameter("Ширина").AsDouble() * 304.8 > 119
                                        && allColumns.Any(y => CheckTouchingColumn(x, y))).ToList();
                foreach (var wall in op4)
                {
                    var height = wall.LookupParameter("Неприсоединенная высота").AsDouble() * 304.8 / 1000;
                    var op1Param = wall.LookupParameter("Оп-4").Set(height / 3);
                }

                var op4_2 = allWalls.Where(x => x.Name.ToLower().Contains("кирпич")
                                        && (_doc.GetElement(x.GetTypeId()) as ElementType)
                                        .LookupParameter("Ширина").AsDouble() * 304.8 > 119
                                        && allWalls.Any(y => CheckTouchingWalls(x, y))).ToList();
                foreach (var wall in op4_2)
                {
                    var height = wall.LookupParameter("Неприсоединенная высота").AsDouble() * 304.8 / 1000;
                    var op1Param = wall.LookupParameter("Оп-4").Set(height / 3);
                }

                //параметр «Неприсоединенная высота» разделить на 0,45 и умножить на параметр «Длина»
                var s2 = allWalls.Where(x => x.Name.ToLower().Contains("кирпич")
                                        && (_doc.GetElement(x.GetTypeId()) as ElementType)
                                        .LookupParameter("Ширина").AsDouble() * 304.8 == 120).ToList();
                foreach (var wall in s2)
                {
                    var height = wall.LookupParameter("Неприсоединенная высота").AsDouble() * 304.8 / 1000;
                    var length = wall.LookupParameter("Длина").AsDouble() * 304.8 / 1000;
                    var op1Param = wall.LookupParameter("С-1").Set(height / 0.45 * length);
                }

                //параметр «Неприсоединенная высота» разделить на 0,45 и умножить на параметр «Длина».
                var s3 = allWalls.Where(x => x.Name.ToLower().Contains("кирпич")
                                        && (_doc.GetElement(x.GetTypeId()) as ElementType)
                                        .LookupParameter("Ширина").AsDouble() * 304.8 == 250).ToList();
                foreach (var wall in s3)
                {
                    var height = wall.LookupParameter("Неприсоединенная высота").AsDouble() * 304.8 / 1000;
                    var length = wall.LookupParameter("Длина").AsDouble() * 304.8 / 1000;
                    var op1Param = wall.LookupParameter("С-1").Set(height / 0.45 * length);
                }

                //параметр «Неприсоединенная высота» разделить на 1,2 и умножить на параметр «Длина».
                var a1 = allWalls.Where(x => x.Name.ToLower().Contains("наружн") && x.Name.ToLower().Contains("газоблок")).ToList();
                foreach (var wall in a1)
                {
                    var height = wall.LookupParameter("Неприсоединенная высота").AsDouble() * 304.8 / 1000;
                    var length = wall.LookupParameter("Длина").AsDouble() * 304.8 / 1000;
                    var op1Param = wall.LookupParameter("А-1").Set(height / 1.2 * length);
                }

                tra.Commit();
            }

            MessageBox.Show("Выполнено");
            Close();
        }

        private bool CheckParameterExists(Element wall)
        {
            List<string> checkList = new List<string>() { "Оп-1", "Оп-2", "Оп-3", "Оп-4", "С-1", "А-1" };
            try
            {
                foreach (var param in checkList)
                {
                    var temp = wall.LookupParameter(param).AsValueString();
                }
            }
            catch
            {
                MessageBox.Show("Не хватает одного из следующих параметров: Оп-1, Оп-2, Оп-3, Оп-4, С-1, А-1. \n Параметры при создании должны иметь \n тип данных: Число \n русским алфавитом \n по экземпляру");
                return true;
            }

            return false;
        }

        private bool CheckTouchingColumn(Element wall, Element column)
        {
            if (wall.LevelId != column.LevelId) return false;

            List<XYZ> allWallPointsList = new List<XYZ>();
            List<double> allXPointsList = new List<double>();
            List<double> allYPointsList = new List<double>();
            List<double> allZPointsList = new List<double>();
            List<string> xList = new List<string>();
            List<string> yList = new List<string>();
            List<string> zList = new List<string>();

            GeometryElement wallElemGeom = wall.get_Geometry(new Options());
            foreach (var solid in wallElemGeom)
            {
                Solid solidAsSolid = solid as Solid;
                if (solidAsSolid != null && solidAsSolid.Edges != null)
                    foreach (Edge edge in solidAsSolid.Edges)
                    {
                        var startPoint = edge.AsCurve().GetEndPoint(0);
                        var endPoint = edge.AsCurve().GetEndPoint(1);

                        if (!(xList.Contains(startPoint.X.ToString()) && yList.Contains(startPoint.Y.ToString()) && zList.Contains(startPoint.Z.ToString())))
                        {
                            xList.Add(startPoint.X.ToString());
                            xList.Add(endPoint.X.ToString());
                            yList.Add(startPoint.Y.ToString());
                            yList.Add(endPoint.Y.ToString());
                            zList.Add(startPoint.Z.ToString());
                            xList.Add(endPoint.Z.ToString());
                            allWallPointsList.Add(startPoint);
                            allWallPointsList.Add(endPoint);
                        }
                    }
            }

            List<GeometryInstance> tempList = column.get_Geometry(new Options()).Where(o => o is GeometryInstance).Cast<GeometryInstance>().ToList();
            foreach (GeometryInstance temp in tempList)
            {
                if (temp.GetInstanceGeometry().Count() > 0)
                {
                    foreach (GeometryObject geometryObject in temp.GetInstanceGeometry())
                    {
                        Solid solidAsSolid = geometryObject as Solid;
                        if (solidAsSolid != null && solidAsSolid.Edges != null)
                            foreach (Edge edge in solidAsSolid.Edges)
                            {
                                var startPoint = edge.AsCurve().GetEndPoint(0);
                                var endPoint = edge.AsCurve().GetEndPoint(1);

                                allXPointsList.Add(startPoint.X);
                                allXPointsList.Add(endPoint.X);

                                allYPointsList.Add(startPoint.Y);
                                allYPointsList.Add(endPoint.Y);

                                allZPointsList.Add(startPoint.Z);
                                allZPointsList.Add(endPoint.Z);
                            }
                    }
                }
                else
                {
                    var secondLogicForColumn = wall.get_Geometry(new Options());
                    foreach (var solid in secondLogicForColumn)
                    {
                        Solid solidAsSolid = solid as Solid;
                        if (solidAsSolid != null && solidAsSolid.Edges != null)
                            foreach (Edge edge in solidAsSolid.Edges)
                            {
                                var startPoint = edge.AsCurve().GetEndPoint(0);
                                var endPoint = edge.AsCurve().GetEndPoint(1);

                                allXPointsList.Add(startPoint.X);
                                allXPointsList.Add(endPoint.X);

                                allYPointsList.Add(startPoint.Y);
                                allYPointsList.Add(endPoint.Y);

                                allZPointsList.Add(startPoint.Z);
                                allZPointsList.Add(endPoint.Z);
                            }
                    }
                }
            }

            MaxMinPointsDto points = new MaxMinPointsDto();
            try
            {
                points = new MaxMinPointsDto
                {
                    MaxX = Math.Round(allXPointsList.Max() * 304.8),
                    MaxY = Math.Round(allYPointsList.Max() * 304.8),
                    MaxZ = Math.Round(allZPointsList.Max() * 304.8),
                    MinX = Math.Round(allXPointsList.Min() * 304.8),
                    MinY = Math.Round(allYPointsList.Min() * 304.8),
                    MinZ = Math.Round(allZPointsList.Min() * 304.8),
                };
            }
            catch (Exception e)
            {
                var q = 1;
            }


            foreach (var wallPoint in allWallPointsList)
            {
                if ((points.MinX <= Math.Round(wallPoint.X * 304.8) && Math.Round(wallPoint.X * 304.8) <= points.MaxX) &&
                    (points.MinY <= Math.Round(wallPoint.Y * 304.8) && Math.Round(wallPoint.Y * 304.8) <= points.MaxY) &&
                    (points.MinZ <= Math.Round(wallPoint.Z * 304.8) && Math.Round(wallPoint.Z * 304.8) <= points.MaxZ))
                {
                    return true;
                }
            }

            return false;
        }

        private bool CheckTouchingWalls(Element wall1, Element wall2)
        {
            if (wall1.LevelId != wall2.LevelId) return false;

            List<XYZ> allWallPointsList = new List<XYZ>();
            List<double> allXPointsList = new List<double>();
            List<double> allYPointsList = new List<double>();
            List<double> allZPointsList = new List<double>();
            List<string> xList = new List<string>();
            List<string> yList = new List<string>();
            List<string> zList = new List<string>();

            GeometryElement wallElemGeom1 = wall1.get_Geometry(new Options());
            foreach (var solid in wallElemGeom1)
            {
                Solid solidAsSolid = solid as Solid;
                if (solidAsSolid != null && solidAsSolid.Edges != null)
                    foreach (Edge edge in solidAsSolid.Edges)
                    {
                        var startPoint = edge.AsCurve().GetEndPoint(0);
                        var endPoint = edge.AsCurve().GetEndPoint(1);

                        if (!(xList.Contains(startPoint.X.ToString()) && yList.Contains(startPoint.Y.ToString()) && zList.Contains(startPoint.Z.ToString())))
                        {
                            xList.Add(startPoint.X.ToString());
                            xList.Add(endPoint.X.ToString());
                            yList.Add(startPoint.Y.ToString());
                            yList.Add(endPoint.Y.ToString());
                            zList.Add(startPoint.Z.ToString());
                            xList.Add(endPoint.Z.ToString());
                            allWallPointsList.Add(startPoint);
                            allWallPointsList.Add(endPoint);
                        }
                    }
            }

            GeometryElement wallElemGeom2 = wall2.get_Geometry(new Options());
            foreach (var solid in wallElemGeom2)
            {
                Solid solidAsSolid = solid as Solid;
                if (solidAsSolid != null && solidAsSolid.Edges != null)
                    foreach (Edge edge in solidAsSolid.Edges)
                    {
                        var startPoint = edge.AsCurve().GetEndPoint(0);
                        var endPoint = edge.AsCurve().GetEndPoint(1);

                        allXPointsList.Add(startPoint.X);
                        allXPointsList.Add(endPoint.X);

                        allYPointsList.Add(startPoint.Y);
                        allYPointsList.Add(endPoint.Y);

                        allZPointsList.Add(startPoint.Z);
                        allZPointsList.Add(endPoint.Z);
                    }
            }

            MaxMinPointsDto points = new MaxMinPointsDto();
            try
            {
                points = new MaxMinPointsDto
                {
                    MaxX = Math.Round(allXPointsList.Max() * 304.8),
                    MaxY = Math.Round(allYPointsList.Max() * 304.8),
                    MaxZ = Math.Round(allZPointsList.Max() * 304.8),
                    MinX = Math.Round(allXPointsList.Min() * 304.8),
                    MinY = Math.Round(allYPointsList.Min() * 304.8),
                    MinZ = Math.Round(allZPointsList.Min() * 304.8),
                };
            }
            catch (Exception e)
            {
                var q = 1;
            }


            foreach (var wallPoint in allWallPointsList)
            {
                if ((points.MinX <= Math.Round(wallPoint.X * 304.8) && Math.Round(wallPoint.X * 304.8) <= points.MaxX) &&
                    (points.MinY <= Math.Round(wallPoint.Y * 304.8) && Math.Round(wallPoint.Y * 304.8) <= points.MaxY) &&
                    (points.MinZ <= Math.Round(wallPoint.Z * 304.8) && Math.Round(wallPoint.Z * 304.8) <= points.MaxZ))
                {
                    return true;
                }
            }

            return false;
        }


        public class JoinsFailurePreprocessor : IFailuresPreprocessor
        {
            public FailureProcessingResult PreprocessFailures(FailuresAccessor failuresAccessor)
            {
                var joinWarningMessages = failuresAccessor
                        .GetFailureMessages(FailureSeverity.Warning)
                        .Where(x => x.GetFailureDefinitionId() == BuiltInFailures.JoinElementsFailures.JoiningDisjointWarn)
                        .ToList();

                foreach (var failureMessageAccessor in joinWarningMessages)
                {
                    failureMessageAccessor.SetCurrentResolutionType(FailureResolutionType.DetachElements);
                    failuresAccessor.ResolveFailure(failureMessageAccessor);
                }

                return FailureProcessingResult.ProceedWithCommit;
            }
        }

        private void Stained_Glass_Window_Click(object sender, RoutedEventArgs e)
        {
            var mullions = new FilteredElementCollector(_doc)
                        .OfCategory(BuiltInCategory.OST_CurtainWallMullions)
                        .WhereElementIsNotElementType();

            var panels = new FilteredElementCollector(_doc)
                        .OfCategory(BuiltInCategory.OST_CurtainWallPanels)
                        .WhereElementIsNotElementType();

            var allDoors = new FilteredElementCollector(_doc)
                        .OfCategory(BuiltInCategory.OST_Doors)
                        .WhereElementIsNotElementType();

            var allWindows = new FilteredElementCollector(_doc)
                        .OfCategory(BuiltInCategory.OST_Windows)
                        .WhereElementIsNotElementType();

            using (Transaction tx = new Transaction(_doc))
            {
                tx.Start("Запись марок витражей");

                foreach (var mullion in mullions)
                {
                    var _ = mullion as Mullion;
                    var hostMark = _.Host.LookupParameter("Марка").AsString();
                    if (hostMark != null && hostMark != "")
                    {
                        mullion.LookupParameter("Марка").Set(hostMark);
                    }
                }

                foreach (var panel in panels)
                {
                    var _ = panel as Panel;
                    var hostMark = _.Host.LookupParameter("Марка").AsString();
                    if (hostMark != null && hostMark != "")
                    {
                        panel.LookupParameter("Марка").Set(hostMark);
                    }
                }

                tx.Commit();
            }
            Close();
        }

        private void Align_Click(object sender, RoutedEventArgs e)
        {
            var view = new FinishingAlignView(_doc);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void CalculateParams_Click(object sender, RoutedEventArgs e)
        {
            var uiDoc = _commandData.Application.ActiveUIDocument;
            var selectedIds = uiDoc.Selection.GetElementIds().ToList();
            var totalLength = 0;
            var totalWidth = 0;
            var totalArea = 0;
            var totalVolume = 0;
            foreach (var id in selectedIds)
            {
                var elem = _doc.GetElement(id);
                var length = elem.GetParameters("Длина");
                var width = elem.GetParameters("Ширина");
                var area = elem.GetParameters("Площадь");
                var volume = elem.GetParameters("Объем");
            }
        }
    }
}
