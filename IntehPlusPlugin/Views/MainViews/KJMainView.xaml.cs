﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class KJMainView : Window
    {
        readonly Document _doc;
        ExternalCommandData _commandData;

        public KJMainView(Document doc, ExternalCommandData commandData)
        {
            InitializeComponent();
            _doc = doc;
            _commandData = commandData;
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void PileMarks_Click(object sender, RoutedEventArgs e)
        {
            var uiDoc = _commandData.Application.ActiveUIDocument;
            var selectedIds = uiDoc.Selection.GetElementIds().ToList();
            var selectedMarks = new List<int>();
            var elems = new List<Element>();
            foreach (var elemId in selectedIds)
            {
                var elem = _doc.GetElement(elemId);
                var type = _doc.GetElement(elem.GetTypeId());
                if (elem.Category.Name == "Фундамент несущей конструкции" && type.Name.ToLower().Contains("сва"))
                {
                    elems.Add(elem);
                    try
                    {
                        var mark = elem.LookupParameter("Марка").AsString();
                        int intValue = int.Parse(mark);
                        selectedMarks.Add(intValue);
                    }
                    catch
                    {
                        MessageBox.Show("Имеются элементы с неправильной маркировкой, нельзя конвертировать в число");
                        Close();
                        return;
                    }
                }
            }

            var result = FormatNumberList(selectedMarks);
            using (Transaction tra = new Transaction(_doc))
            {
                tra.Start("Добавление марок свай");
                foreach (var elem in elems)
                {
                    elem.LookupParameter("Комментарии").Set(result);
                }
                tra.Commit();
            }

            MessageBox.Show("Добавлено");
            Close();
        }

        private string FormatNumberList(List<int> numbers)
        {
            if (numbers == null || numbers.Count == 0)
            {
                return "";
            }
            var ordered = numbers.OrderBy(n => n).ToList();

            string result = "";
            int startRange = ordered[0];
            int endRange = ordered[0];

            for (int i = 1; i < ordered.Count; i++)
            {
                if (ordered[i] == endRange + 1)
                {
                    endRange = ordered[i];
                }
                else
                {
                    result += FormatRange(startRange, endRange) + ",";
                    startRange = endRange = ordered[i];
                }
            }

            result += FormatRange(startRange, endRange);

            return result;
        }

        static string FormatRange(int start, int end)
        {
            return start == end ? start.ToString() : (start + 1 == end ? $"{start},{end}" : $"{start}...{end}");
        }

        private void Dimension_Click(object sender, RoutedEventArgs e)
        {
            var uiDoc = _commandData.Application.ActiveUIDocument;
            var selectedIds = uiDoc.Selection.GetElementIds().ToList();
            bool error = false;
            Transaction transaction = new Transaction(_doc, "Назначение размеров");
            transaction.Start();
            foreach (var id in selectedIds)
            {
                try
                {
                    var elem = _doc.GetElement(id);
                    if (elem.Category?.Name == "Размеры")
                    {
                        var asDimension = elem as Dimension;
                        var references = asDimension.References;
                        Element rebarElem = null;
                        foreach (var reference in references)
                        {
                            var referenceSelected = reference as Reference;
                            rebarElem = _doc.GetElement(referenceSelected.ElementId);
                            var checkParam = rebarElem.GetParameters("Интервал");
                            if (checkParam.Any())
                                break;
                        }

                        var parameterInterval = rebarElem.GetParameters("Интервал");
                        if (parameterInterval.Any())
                        {
                            var parameterValueString = parameterInterval.First().AsValueString();
                            var parameterValue = int.Parse(parameterValueString.Split(' ')[0]);
                            var segments = asDimension.Segments;
                            if (segments.Size == 0)
                            {
                                var value = (double)Math.Round((double)(asDimension.Value * 304.8), 0);
                                if (value % parameterValue == 0)
                                {
                                    if (value <= parameterValue)
                                        continue;
                                    if (value > 2000)
                                        asDimension.Prefix = parameterValue.ToString() + "x" + (value / parameterValue).ToString() + "=";
                                    else
                                        asDimension.ValueOverride = parameterValue.ToString() + "x" + (value / parameterValue).ToString();
                                }
                            }
                            else
                            {
                                foreach (var segment in segments)
                                {
                                    var asSegment = segment as DimensionSegment;
                                    var value = (double)Math.Round((double)(asSegment.Value * 304.8), 0);
                                    if (value % parameterValue == 0)
                                    {
                                        if (value <= parameterValue)
                                            continue;
                                        if (value > 2000)
                                            asSegment.Prefix = parameterValue.ToString() + "x" + (value / parameterValue).ToString() + "=";
                                        else
                                            asSegment.ValueOverride = parameterValue.ToString() + "x" + (value / parameterValue).ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                catch
                {
                    error = true;
                }
            }

            transaction.Commit();
            if (error)
            {
                MessageBox.Show("Были ошибки, проверьте результат");
            }
            Close();
        }

        private void Wholes_Click(object sender, RoutedEventArgs e)
        {
            var view = new KJWholesView(_doc, _commandData);
            view.ShowDialog();
            view.Close();
        }
    }
}
