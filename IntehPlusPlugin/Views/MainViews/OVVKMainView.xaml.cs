﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class OVVKMainView : Window
    {
        readonly Document _doc;
        readonly ExternalCommandData _commandData;

        public OVVKMainView(Document doc, ExternalCommandData commandData)
        {
            InitializeComponent();
            _doc = doc;
            _commandData = commandData;
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void ElevationSpot_Click(object sender, RoutedEventArgs e)
        {
            var view = new ElevationSpotSelection(_doc, _commandData, this);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void GroupModelsSet_Click(object sender, RoutedEventArgs e)
        {
            var view = new OVVKGroupModelView(_doc);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void AGSKCodesExcel_Click(object sender, RoutedEventArgs e)
        {
            var list = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                            .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                            .Where(c => c.IsValidObject);
            List<Element> elemsList = list.Where(c => !string.IsNullOrEmpty(c.Name)
                                && c.DemolishedPhaseId.IntegerValue == -1
                                && !string.IsNullOrEmpty(c.Category?.Name)).ToList();

            IXLWorkbook workbook = new XLWorkbook();
            IXLWorksheet worksheetPipes = workbook.Worksheets.Add("Трубы");
            IXLWorksheet worksheetPipeInsulation = workbook.Worksheets.Add("Изоляция трубопроводов");
            IXLWorksheet worksheetDuct = workbook.Worksheets.Add("Воздуховоды");
            IXLWorksheet worksheetDuctInsulation = workbook.Worksheets.Add("Изоляция воздуховодов");
            IXLWorksheet worksheetPipeConnects = workbook.Worksheets.Add("Соединители трубопроводов");
            FillExcelSheets(worksheetPipes, worksheetPipeInsulation, worksheetDuct, worksheetDuctInsulation, worksheetPipeConnects);

            var pipes = new List<ExcelData>();
            var pipeInsulation = new List<ExcelData>();
            var ducts = new List<ExcelData>();
            var ductInsulation = new List<ExcelData>();
            var pipeConnects = new List<ExcelData>();
            foreach (var elem in elemsList)
            {
                var biCode = elem.GetParameters("BI_код_оборудования");
                if (elem.Category.Name == "Трубы")
                {
                    var type = _doc.GetElement(elem.GetTypeId());
                    var diameter = elem.GetParameters("Диаметр");
                    if (type != null && diameter.Any())
                    {
                        var dto = new ExcelData()
                        {
                            Type = type.Name,
                            Diameter = diameter.FirstOrDefault().AsValueString(),
                            BiCode = biCode.Any() ? biCode.First().AsString() : "",
                        };
                        if (!pipes.Any(x => x.Type == dto.Type && x.Diameter == dto.Diameter))
                            pipes.Add(dto);
                    }
                }
                if (elem.Category.Name == "Материалы изоляции труб")
                {
                    var type = _doc.GetElement(elem.GetTypeId());
                    var thickness = elem.GetParameters("Толщина изоляции");
                    var host = (elem as InsulationLiningBase).HostElementId;
                    var pipe = _doc.GetElement(host);
                    var diameter = pipe.GetParameters("Диаметр");
                    if (type != null && diameter.Any() && thickness.Any())
                    {
                        var dto = new ExcelData()
                        {
                            Type = type.Name,
                            Diameter = diameter.FirstOrDefault().AsValueString(),
                            Thickness = thickness.FirstOrDefault().AsValueString(),
                            BiCode = biCode.Any() ? biCode.First().AsString() : "",
                        };
                        if (!pipeInsulation.Any(x => x.Type == dto.Type && x.Diameter == dto.Diameter && x.Thickness == dto.Thickness))
                            pipeInsulation.Add(dto);
                    }
                }
                if (elem.Category.Name == "Воздуховоды")
                {
                    var type = _doc.GetElement(elem.GetTypeId());
                    var size = elem.GetParameters("Размер");
                    if (type != null && size.Any())
                    {
                        var dto = new ExcelData()
                        {
                            Type = type.Name,
                            Size = size.FirstOrDefault().AsString(),
                            BiCode = biCode.Any() ? biCode.First().AsString() : "",
                        };
                        if (!ducts.Any(x => x.Type == dto.Type && x.Size == dto.Size))
                            ducts.Add(dto);
                    }
                }
                if (elem.Category.Name == "Материалы изоляции воздуховодов")
                {
                    var type = _doc.GetElement(elem.GetTypeId());
                    var thickness = elem.GetParameters("Толщина изоляции");
                    if (type != null && thickness.Any())
                    {
                        var dto = new ExcelData()
                        {
                            Type = type.Name,
                            Thickness = thickness.FirstOrDefault().AsValueString(),
                            BiCode = biCode.Any() ? biCode.First().AsString() : "",
                        };
                        if (!ductInsulation.Any(x => x.Type == dto.Type && x.Thickness == dto.Thickness))
                            ductInsulation.Add(dto);
                    }
                }
                if (elem.Category.Name == "Соединительные детали трубопроводов")
                {
                    var type = _doc.GetElement(elem.GetTypeId());
                    var size = elem.GetParameters("Размер");
                    if (type != null && size.Any())
                    {
                        var dto = new ExcelData()
                        {
                            Type = type.Name,
                            Size = size.FirstOrDefault().AsString(),
                            BiCode = biCode.Any() ? biCode.First().AsString() : "",
                        };
                        if (!pipeConnects.Any(x => x.Type == dto.Type && x.Size == dto.Size))
                            pipeConnects.Add(dto);
                    }
                }
            }

            var counter1 = 2;
            foreach (var dto in pipes.OrderBy(x => x.Type))
            {
                worksheetPipes.Cell(counter1, 1).Value = dto.Type;
                worksheetPipes.Cell(counter1, 2).Value = dto.Diameter;
                worksheetPipes.Cell(counter1, 3).Value = dto.BiCode;
                counter1 += 1;
            }

            var counter2 = 2;
            foreach (var dto in pipeInsulation.OrderBy(x => x.Type))
            {
                worksheetPipeInsulation.Cell(counter2, 1).Value = dto.Type;
                worksheetPipeInsulation.Cell(counter2, 2).Value = dto.Diameter;
                worksheetPipeInsulation.Cell(counter2, 3).Value = dto.Thickness;
                worksheetPipeInsulation.Cell(counter2, 4).Value = dto.BiCode;
                counter2 += 1;
            }

            var counter3 = 2;
            foreach (var dto in ducts.OrderBy(x => x.Type))
            {
                worksheetDuct.Cell(counter3, 1).Value = dto.Type;
                worksheetDuct.Cell(counter3, 2).Value = dto.Size;
                worksheetDuct.Cell(counter3, 3).Value = dto.BiCode;
                counter3 += 1;
            }

            var counter4 = 2;
            foreach (var dto in ductInsulation.OrderBy(x => x.Type))
            {
                worksheetDuctInsulation.Cell(counter4, 1).Value = dto.Type;
                worksheetDuctInsulation.Cell(counter4, 2).Value = dto.Thickness;
                worksheetDuctInsulation.Cell(counter4, 3).Value = dto.BiCode;
                counter4 += 1;
            }

            var counter5 = 2;
            foreach (var dto in pipeConnects.OrderBy(x => x.Type))
            {
                worksheetPipeConnects.Cell(counter5, 1).Value = dto.Type;
                worksheetPipeConnects.Cell(counter5, 2).Value = dto.Size;
                worksheetPipeConnects.Cell(counter5, 3).Value = dto.BiCode;
                counter5 += 1;
            }

            var saveFileDialog = new System.Windows.Forms.SaveFileDialog
            {
                Filter = "Excel Files(.xlsx)| *.xlsx",
                FileName = _doc.Title.Replace(".rvt", "")
            };
            if (saveFileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            workbook.SaveAs(saveFileDialog.FileName);
            Close();
        }

        private void FillExcelSheets(IXLWorksheet worksheetPipes, IXLWorksheet worksheetPipeInsulation,
            IXLWorksheet worksheetDuct, IXLWorksheet worksheetDuctInsulation, IXLWorksheet worksheetPipeConnects)
        {
            worksheetPipes.Cell(1, 1).Value = "Тип";
            worksheetPipes.Cell(1, 2).Value = "Диаметр";
            worksheetPipes.Cell(1, 3).Value = "BI код оборудования";

            worksheetPipeInsulation.Cell(1, 1).Value = "Тип";
            worksheetPipeInsulation.Cell(1, 2).Value = "Диаметр";
            worksheetPipeInsulation.Cell(1, 3).Value = "Толщина изоляции";
            worksheetPipeInsulation.Cell(1, 4).Value = "BI код оборудования";

            worksheetDuct.Cell(1, 1).Value = "Тип";
            worksheetDuct.Cell(1, 2).Value = "Размер";
            worksheetDuct.Cell(1, 3).Value = "BI код оборудования";

            worksheetDuctInsulation.Cell(1, 1).Value = "Тип";
            worksheetDuctInsulation.Cell(1, 2).Value = "Толщина изоляции";
            worksheetDuctInsulation.Cell(1, 3).Value = "BI код оборудования";

            worksheetPipeConnects.Cell(1, 1).Value = "Тип";
            worksheetPipeConnects.Cell(1, 2).Value = "Размер";
            worksheetPipeConnects.Cell(1, 3).Value = "BI код оборудования";
        }

        private void AGSKInsert_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog
            {
                Filter = "Excel Files(.xlsx)| *.xlsx"
            };
            var result = dialog.ShowDialog();
            if (result != true)
                Close();

            var pipesList = new List<ExcelData>();
            var pipeInsulationList = new List<ExcelData>();
            var ductList = new List<ExcelData>();
            var ductInsulationList = new List<ExcelData>();
            var pipeConnectsList = new List<ExcelData>();
            List<string> errors = new List<string>();
            try
            {
                using (var workbook = new XLWorkbook(dialog.FileName))
                {
                    IXLWorksheet worksheetPipes = workbook.Worksheets.First(x => x.Name == "Трубы");
                    IXLWorksheet worksheetPipeInsulation = workbook.Worksheets.First(x => x.Name == "Изоляция трубопроводов");
                    IXLWorksheet worksheetDuct = workbook.Worksheets.First(x => x.Name == "Воздуховоды");
                    IXLWorksheet worksheetDuctInsulation = workbook.Worksheets.First(x => x.Name == "Изоляция воздуховодов");
                    IXLWorksheet worksheetPipeConnects = workbook.Worksheets.First(x => x.Name == "Соединители трубопроводов");

                    //pipes
                    var firstCellPipes = worksheetPipes.FirstCellUsed();
                    var lastCellPipes = worksheetPipes.LastCellUsed();
                    int startRowPipes = firstCellPipes.Address.RowNumber;
                    int endRowPipes = lastCellPipes.Address.RowNumber;
                    for (int row = startRowPipes + 1; row <= endRowPipes; row++)
                    {
                        var type = worksheetPipes.Cell(row, 1).Value.ToString();
                        var diameter = worksheetPipes.Cell(row, 2).Value.ToString();
                        var biCode = worksheetPipes.Cell(row, 3).Value.ToString();
                        var dto = new ExcelData()
                        {
                            Type = type,
                            Diameter = diameter,
                            BiCode = biCode,
                        };
                        pipesList.Add(dto);
                    }

                    //pipeInsulation
                    var firstCellPipeInsulation = worksheetPipeInsulation.FirstCellUsed();
                    var lastCellPipeInsulation = worksheetPipeInsulation.LastCellUsed();
                    int startRowPipeInsulation = firstCellPipeInsulation.Address.RowNumber;
                    int endRowPipeInsulation = lastCellPipeInsulation.Address.RowNumber;
                    for (int row = startRowPipeInsulation + 1; row <= endRowPipeInsulation; row++)
                    {
                        var type = worksheetPipeInsulation.Cell(row, 1).Value.ToString();
                        var diameter = worksheetPipeInsulation.Cell(row, 2).Value.ToString();
                        var thickness = worksheetPipeInsulation.Cell(row, 3).Value.ToString();
                        var biCode = worksheetPipeInsulation.Cell(row, 4).Value.ToString();
                        var dto = new ExcelData()
                        {
                            Type = type,
                            Diameter = diameter,
                            Thickness = thickness,
                            BiCode = biCode,
                        };
                        pipeInsulationList.Add(dto);
                    }

                    //ducts
                    var firstCellDucts = worksheetDuct.FirstCellUsed();
                    var lastCellDucts = worksheetDuct.LastCellUsed();
                    int startRowDucts = firstCellDucts.Address.RowNumber;
                    int endRowDucts = lastCellDucts.Address.RowNumber;
                    for (int row = startRowDucts + 1; row <= endRowDucts; row++)
                    {
                        var type = worksheetDuct.Cell(row, 1).Value.ToString();
                        var size = worksheetDuct.Cell(row, 2).Value.ToString();
                        var biCode = worksheetDuct.Cell(row, 3).Value.ToString();
                        var dto = new ExcelData()
                        {
                            Type = type,
                            Size = size,
                            BiCode = biCode,
                        };
                        ductList.Add(dto);
                    }

                    //ductInsulation
                    var firstCellDuctInsulation = worksheetDuctInsulation.FirstCellUsed();
                    var lastCellDuctInsulation = worksheetDuctInsulation.LastCellUsed();
                    int startRowDuctInsulation = firstCellDuctInsulation.Address.RowNumber;
                    int endRowDuctInsulation = lastCellDuctInsulation.Address.RowNumber;
                    for (int row = startRowDuctInsulation + 1; row <= endRowDuctInsulation; row++)
                    {
                        var type = worksheetDuctInsulation.Cell(row, 1).Value.ToString();
                        var thickness = worksheetDuctInsulation.Cell(row, 2).Value.ToString();
                        var biCode = worksheetDuctInsulation.Cell(row, 3).Value.ToString();
                        var dto = new ExcelData()
                        {
                            Type = type,
                            Thickness = thickness,
                            BiCode = biCode,
                        };
                        ductInsulationList.Add(dto);
                    }

                    //ductInsulation
                    var firstCellPipeConnects = worksheetPipeConnects.FirstCellUsed();
                    var lastCellPipeConnects = worksheetPipeConnects.LastCellUsed();
                    int startRowPipeConnects = firstCellPipeConnects.Address.RowNumber;
                    int endRowPipeConnects = lastCellPipeConnects.Address.RowNumber;
                    for (int row = startRowPipeConnects + 1; row <= endRowPipeConnects; row++)
                    {
                        var type = worksheetPipeConnects.Cell(row, 1).Value.ToString();
                        var size = worksheetPipeConnects.Cell(row, 2).Value.ToString();
                        var biCode = worksheetPipeConnects.Cell(row, 3).Value.ToString();
                        var dto = new ExcelData()
                        {
                            Type = type,
                            Size = size,
                            BiCode = biCode,
                        };
                        pipeConnectsList.Add(dto);
                    }
                }

                using (Transaction tra = new Transaction(_doc))
                {
                    tra.Start("Запись кодов АГСК");
                    var list = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                                    .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                                    .Where(c => c.IsValidObject);
                    List<Element> elemsList = list.Where(c => !string.IsNullOrEmpty(c.Name)
                                        && c.DemolishedPhaseId.IntegerValue == -1
                                        && !string.IsNullOrEmpty(c.Category?.Name)).ToList();

                    foreach (var elem in elemsList)
                    {
                        try
                        {
                            var biCode = elem.GetParameters("BI_код_оборудования");
                            if (elem.Category.Name == "Трубы")
                            {
                                var type = _doc.GetElement(elem.GetTypeId());
                                var diameter = elem.GetParameters("Диаметр");
                                if (type != null && diameter.Any())
                                {
                                    var excelValue = pipesList.FirstOrDefault(x => x.Type == type.Name && x.Diameter == diameter.FirstOrDefault().AsValueString());
                                    if (excelValue != null && biCode.Any() && !biCode.FirstOrDefault().IsReadOnly)
                                        biCode.FirstOrDefault().Set(excelValue.BiCode);
                                }
                            }
                            if (elem.Category.Name == "Материалы изоляции труб")
                            {
                                var type = _doc.GetElement(elem.GetTypeId());
                                var thickness = elem.GetParameters("Толщина изоляции");
                                var host = (elem as InsulationLiningBase).HostElementId;
                                var pipe = _doc.GetElement(host);
                                var diameter = pipe.GetParameters("Диаметр");
                                if (type != null && diameter.Any() && thickness.Any())
                                {
                                    var excelValue = pipeInsulationList.FirstOrDefault(x => x.Type == type.Name && x.Diameter == diameter.FirstOrDefault().AsValueString() &&
                                        x.Thickness == thickness.FirstOrDefault().AsValueString());
                                    if (excelValue != null && biCode.Any() && !biCode.FirstOrDefault().IsReadOnly)
                                        biCode.FirstOrDefault().Set(excelValue.BiCode);
                                }
                            }
                            if (elem.Category.Name == "Воздуховоды")
                            {
                                var type = _doc.GetElement(elem.GetTypeId());
                                var size = elem.GetParameters("Размер");
                                if (type != null && size.Any())
                                {
                                    var excelValue = ductList.FirstOrDefault(x => x.Type == type.Name && x.Size == size.FirstOrDefault().AsString());
                                    if (excelValue != null && biCode.Any() && !biCode.FirstOrDefault().IsReadOnly)
                                        biCode.FirstOrDefault().Set(excelValue.BiCode);
                                }
                            }
                            if (elem.Category.Name == "Материалы изоляции воздуховодов")
                            {
                                var type = _doc.GetElement(elem.GetTypeId());
                                var thickness = elem.GetParameters("Толщина изоляции");
                                if (type != null && thickness.Any())
                                {
                                    var excelValue = ductInsulationList.FirstOrDefault(x => x.Type == type.Name && x.Thickness == thickness.FirstOrDefault().AsValueString());
                                    if (excelValue != null && biCode.Any() && !biCode.FirstOrDefault().IsReadOnly)
                                        biCode.FirstOrDefault().Set(excelValue.BiCode);
                                }
                            }
                            if (elem.Category.Name == "Соединительные детали трубопроводов")
                            {
                                var type = _doc.GetElement(elem.GetTypeId());
                                var size = elem.GetParameters("Размер");
                                if (type != null && size.Any())
                                {
                                    var excelValue = pipeConnectsList.FirstOrDefault(x => x.Type == type.Name && x.Size == size.FirstOrDefault().AsString());
                                    if (excelValue != null && biCode.Any() && !biCode.FirstOrDefault().IsReadOnly)
                                        biCode.FirstOrDefault().Set(excelValue.BiCode);
                                }
                            }
                        }
                        catch
                        {
                            errors.Add(elem.Id.ToString());
                        }
                    }

                    tra.Commit();
                }
            }
            catch
            {
                MessageBox.Show("Проверьте закрыт ли файл и правильный ли файл используется");
                return;
            }
            var errorMessage = "";
            if (errors.Any())
                errorMessage = ". Ошибки в элементах: " + string.Join(", ", errors.ToArray());

            MessageBox.Show("Готово" + errorMessage);
            Close();
        }

        private List<Element> GetSelectedElements()
        {
            List<Element> selectedElems = new List<Element>();
            var uiDoc = _commandData.Application.ActiveUIDocument;
            var selectedIds = uiDoc.Selection.GetElementIds().ToList();
            foreach (var id in selectedIds)
            {
                var elem = _doc.GetElement(id);
                if (elem.GetType().Name == "Pipe")
                    selectedElems.Add(elem);
                else if (elem.GetType().Name == "Duct")
                    selectedElems.Add(elem);
            }
            return selectedElems;
        }

        private List<DocumentDtos> GetLinkedFiles()
        {
            List<DocumentDtos> resultDocs = new List<DocumentDtos>();
            var links = new FilteredElementCollector(_doc).OfCategory(BuiltInCategory.OST_RvtLinks).WhereElementIsNotElementType().ToElements();
            foreach (RevitLinkInstance link in links)
            {
                Document linkDoc = link.GetLinkDocument();
                if (linkDoc != null)
                    resultDocs.Add(new DocumentDtos()
                    {
                        Document = linkDoc,
                        RevitLinkInstance = link,
                        Transform = link.GetTotalTransform().Inverse
                    });
            }
            return resultDocs;
        }

        private List<LinkElemsWithTransform> GetLinkedFilesElems(List<DocumentDtos> resultDocs)
        {
            List<LinkElemsWithTransform> _linkSelectedElems = new List<LinkElemsWithTransform>();
            foreach (var link in resultDocs)
            {
                var dto = new LinkElemsWithTransform()
                {
                    Document = link.Document,
                    Transform = link.Transform.Inverse,
                    RevitLinkInstance = link.RevitLinkInstance,
                    Elements = new List<Element>(),
                };

                FilteredElementCollector wallCollector = new FilteredElementCollector(link.Document).OfClass(typeof(Wall));
                ElementCategoryFilter filter = new ElementCategoryFilter(BuiltInCategory.OST_Walls);
                var walls = wallCollector.WherePasses(filter).Cast<Wall>().Where(w => w.WallType.Kind == WallKind.Basic).ToList();
                dto.Elements.AddRange(walls.ToList());
                FilteredElementCollector floorCollector = new FilteredElementCollector(link.Document).OfClass(typeof(Floor));
                dto.Elements.AddRange(floorCollector.ToList());
                _linkSelectedElems.Add(dto);
            }
            return _linkSelectedElems;
        }

        private List<IntersectDto> GetAllIntersects(List<Element> selectedElems, List<LinkElemsWithTransform> linkedDocsWithElems)
        {
            var intersects = new List<IntersectDto>();
            foreach (var pipeOrDuct in selectedElems)
            {
                foreach (var linkedDoc in linkedDocsWithElems)
                {
                    foreach (var wallOrFloor in linkedDoc.Elements)
                    {
                        var dto = new IntersectDto()
                        {
                            PipeOrDuct = pipeOrDuct,
                            WallOrFloor = wallOrFloor,
                            IntersectingSolids = new List<Solid>(),
                        };
                        GeometryElement pipeOrDuctGeometry = pipeOrDuct.get_Geometry(new Options());
                        GeometryElement wallOrFloorGeometry = wallOrFloor.get_Geometry(new Options());
                        foreach (GeometryObject pipeOrDuctSolid in pipeOrDuctGeometry)
                        {
                            if (pipeOrDuctSolid is Solid)
                            {
                                foreach (GeometryObject wallOrFloorSolid in wallOrFloorGeometry)
                                {
                                    if (wallOrFloorSolid is Solid)
                                    {
                                        var newSolid = SolidUtils.CreateTransformed(wallOrFloorSolid as Solid, linkedDoc.Transform);
                                        Solid intersectSolid = BooleanOperationsUtils.ExecuteBooleanOperation(pipeOrDuctSolid as Solid, newSolid, BooleanOperationsType.Intersect);
                                        if (Math.Abs(intersectSolid.Volume) > 0.000001)
                                        {
                                            dto.Transform = linkedDoc.Transform;
                                            dto.IntersectingSolids.Add(intersectSolid);
                                            intersects.Add(dto);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return intersects;
        }

        private void Whole_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Element> selectedElems = GetSelectedElements();
                List<DocumentDtos> resultDocs = GetLinkedFiles();
                List<LinkElemsWithTransform> linkedDocsWithElems = GetLinkedFilesElems(resultDocs);
                var intersects = GetAllIntersects(selectedElems, linkedDocsWithElems);

                var allLinkElemsGrouping = intersects.Select(x => x.WallOrFloor).ToList();
                var touchingGroupes = new List<LinkGroupesDto>();
                foreach (var linkElem1 in allLinkElemsGrouping)
                {
                    if (!touchingGroupes.Any(x => x.TouchingElems.Any(y => y.Id.IntegerValue == linkElem1.Id.IntegerValue)))
                        touchingGroupes.Add(new LinkGroupesDto()
                        {
                            TouchingElems = new List<Element>() { linkElem1 }
                        });
                    foreach (var linkElem2 in allLinkElemsGrouping)
                    {
                        if (linkElem1.Id.IntegerValue != linkElem2.Id.IntegerValue)
                        {
                            GeometryElement geom1 = linkElem1.get_Geometry(new Options());
                            GeometryElement geom2 = linkElem2.get_Geometry(new Options());
                            foreach (GeometryObject solid1 in geom1)
                            {
                                if (solid1 is Solid)
                                {
                                    foreach (GeometryObject solid2 in geom2)
                                    {
                                        if (solid2 is Solid)
                                        {
                                            var solid1AsSolid = solid1 as Solid;
                                            var solid2AsSolid = solid2 as Solid;
                                            var faces1 = new List<Face>();
                                            var faces2 = new List<Face>();
                                            FacesFromSolids(solid1AsSolid, faces1);
                                            FacesFromSolids(solid2AsSolid, faces2);
                                            foreach (var face1 in faces1)
                                            {
                                                foreach (var face2 in faces2)
                                                {
                                                    var result = DoFacesIntersect(face1, face2);
                                                    if (result)
                                                    {
                                                        if (!touchingGroupes.Any())
                                                            touchingGroupes.Add(new LinkGroupesDto()
                                                            {
                                                                TouchingElems = new List<Element>() { linkElem1, linkElem2 }
                                                            });
                                                        else
                                                        {
                                                            var currentGr = touchingGroupes.Where(x => x.TouchingElems.Any(y =>
                                                                y.Id.IntegerValue == linkElem1.Id.IntegerValue)).FirstOrDefault();
                                                            if (!currentGr.TouchingElems.Any(x => x.Id.IntegerValue == linkElem2.Id.IntegerValue))
                                                                currentGr.TouchingElems.Add(linkElem2);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                var allGroupesForWholes = new List<GroupedIntersectDto>();
                foreach (var touchingGroup in touchingGroupes)
                {
                    var dto = new GroupedIntersectDto()
                    {
                        WallsOrFloors = touchingGroup.TouchingElems,
                        PipesOrDucts = new List<Element>(),
                        IntersectingSolids = new List<Solid>(),
                    };
                    foreach (var elem in touchingGroup.TouchingElems)
                    {
                        var currentWallIntersects = intersects.Where(x => x.WallOrFloor.Id.IntegerValue == elem.Id.IntegerValue).ToList();
                        foreach (var intersect in currentWallIntersects)
                        {
                            if (!dto.PipesOrDucts.Any(x => x.Id.IntegerValue != intersect.PipeOrDuct.Id.IntegerValue))
                                dto.PipesOrDucts.Add(intersect.PipeOrDuct);
                            dto.IntersectingSolids.AddRange(intersect.IntersectingSolids);
                        }
                    }
                    allGroupesForWholes.Add(dto);
                }

                Transaction transaction = new Transaction(_doc, "Размещение отверстий");
                transaction.Start();
                var createdFamilies = new List<ElementId>();
                foreach (var groupForWhole in allGroupesForWholes)
                {
                    var edges = new List<Edge>();
                    var points = new List<XYZ>();
                    foreach (var solid in groupForWhole.IntersectingSolids)
                    {
                        foreach (var edge in solid.Edges)
                        {
                            edges.Add(edge as Edge);
                            Curve curve = (edge as Edge).AsCurve();
                            XYZ startPoint = curve.GetEndPoint(0);
                            XYZ endPoint = curve.GetEndPoint(1);
                            points.Add(startPoint);
                            points.Add(endPoint);
                        }
                    }

                    var maxX = points.Max(x => x.X);
                    var maxY = points.Max(x => x.Y);
                    var maxZ = points.Max(x => x.Z);

                    var minX = points.Min(x => x.X);
                    var minY = points.Min(x => x.Y);
                    var minZ = points.Min(x => x.Z);

                    var midX = (maxX + minX) / 2;
                    var midY = (maxY + minY) / 2;
                    var midZ = (maxZ + minZ) / 2;

                    FilteredElementCollector collector = new FilteredElementCollector(_doc);
                    ICollection<Element> families = collector.OfClass(typeof(FamilySymbol)).ToElements();
                    var familySymbol = families.Where(x => x.Name == "(отверстие)ОВиВК_интех").FirstOrDefault();
                    if (familySymbol == null)
                    {
                        MessageBox.Show("(отверстие)ОВиВК_интех не найдено в списке семейств проект");
                        Close();
                        return;
                    }
                    var level = GetLevelForPoint(midZ);
                    var view = _doc.ActiveView;
                    var pointMiddle = new XYZ(midX, midY, midZ - level.Elevation);
                    var familyInstance = _doc.Create.NewFamilyInstance(pointMiddle, familySymbol as FamilySymbol, level, Autodesk.Revit.DB.Structure.StructuralType.NonStructural);
                    createdFamilies.Add(familyInstance.Id);
                    if (groupForWhole.WallsOrFloors.Any(x => x.Category.Name == "Стены"))
                    {
                        var pipe = groupForWhole.PipesOrDucts.First();
                        Line line = (pipe.Location as LocationCurve).Curve as Line;
                        XYZ pipeStartPoint = line.GetEndPoint(0);
                        XYZ pipeEndPoint = line.GetEndPoint(1);
                        XYZ pipeDirection = (pipeEndPoint - pipeStartPoint).Normalize();
                        XYZ familyDirection = new XYZ(0, -1, 0);
                        double rotationAngle = pipeDirection.AngleTo(familyDirection);

                        var pointMiddle1 = new XYZ(midX, midY, midZ + 10);
                        var pointMiddle2 = new XYZ(midX, midY, midZ - 10);
                        Line axis = Line.CreateBound(pointMiddle1, pointMiddle2);

                        var height = (maxZ - minZ) * 304.8 + 100;
                        var depth = (maxY - minY) * 304.8;
                        var width = (maxX - minX) * 304.8 + 100;
                        var _ = UnitUtils.Convert(rotationAngle, UnitTypeId.Radians, UnitTypeId.Degrees);
                        if (rotationAngle != 0)
                        {
                            ElementTransformUtils.RotateElement(_doc, familyInstance.Id, axis, rotationAngle);
                            if (_ > 85 && _ < 95)
                            {
                                var temp = width;
                                width = depth + 100;
                                depth = temp - 100;
                            }
                        }
                        var element = _doc.GetElement(familyInstance.Id);
                        var param1 = element.LookupParameter("Высота");
                        var param2 = element.LookupParameter("Глубина");
                        var param3 = element.LookupParameter("Ширина");
                        var param4 = element.LookupParameter("Высота от 0 проекта");
                        param1.Set(RoundUpToMultipleOf50(height) / 304.8);
                        param2.Set(RoundUpToMultipleOf50(depth) / 304.8);
                        param3.Set(RoundUpToMultipleOf50(width) / 304.8);
                        param4.Set(level.Elevation + pointMiddle.Z - (RoundUpToMultipleOf50(height) / 304.8 / 2));
                    }
                    else
                    {
                        var height = (maxZ - minZ) * 304.8;
                        var depth = (maxY - minY) * 304.8 + 100;
                        var width = (maxX - minX) * 304.8 + 100;
                        var element = _doc.GetElement(familyInstance.Id);
                        var param1 = element.LookupParameter("Высота");
                        var param2 = element.LookupParameter("Глубина");
                        var param3 = element.LookupParameter("Ширина");
                        var param4 = element.LookupParameter("Высота от 0 проекта");
                        param1.Set(RoundUpToMultipleOf50(height) / 304.8);
                        param2.Set(RoundUpToMultipleOf50(depth) / 304.8);
                        param3.Set(RoundUpToMultipleOf50(width) / 304.8);
                        param4.Set(level.Elevation + pointMiddle.Z - (RoundUpToMultipleOf50(height) / 304.8 / 2));
                    }
                }
                transaction.Commit();

                var wholes = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                    .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                    .Where(c => c.IsValidObject)
                    .Where(c => !string.IsNullOrEmpty(c.Name)
                        && c.DemolishedPhaseId.IntegerValue == -1
                        && !string.IsNullOrEmpty(c.Category?.Name)).Where(x => x.Name == "(отверстие)ОВиВК_интех").ToList();
                var listToDelete = new List<ElementId>();
                foreach (var whole1 in wholes)
                {
                    BoundingBoxXYZ bb1 = whole1.get_BoundingBox(null);
                    foreach (var whole2 in wholes)
                    {
                        BoundingBoxXYZ bb2 = whole2.get_BoundingBox(null);
                        if (whole1.Id.IntegerValue == whole2.Id.IntegerValue)
                            continue;

                        if (bb1 != null && bb2 != null && bb1.Max.X == bb2.Max.X && bb1.Max.Y == bb2.Max.Y && bb1.Max.Z == bb2.Max.Z &&
                            bb1.Min.X == bb2.Min.X && bb1.Min.Y == bb2.Min.Y && bb1.Min.Z == bb2.Min.Z)

                            if (createdFamilies.Any(x => x.IntegerValue == whole1.Id.IntegerValue))
                                listToDelete.Add(whole2.Id);
                            else
                                listToDelete.Add(whole1.Id);
                    }
                }

                Transaction transaction2 = new Transaction(_doc, "Удаление дубликатов отверстий");
                transaction2.Start();
                var distincted = listToDelete.GroupBy(x => x.IntegerValue).Select(x => x.FirstOrDefault()).ToList();
                _doc.Delete(distincted);
                transaction2.Commit();
                Close();
            }
            catch
            {
                Close();
            }
        }

        private Level GetLevelForPoint(double elevationZ)
        {
            var levels = new FilteredElementCollector(_doc).OfClass(typeof(Level)).Cast<Level>();
            var selected = levels.Where(x => x.Elevation < elevationZ).OrderByDescending(x => x.Elevation).FirstOrDefault();
            if (selected == null)
                selected = levels.Where(x => x.Elevation > elevationZ).OrderBy(x => x.Elevation).FirstOrDefault();

            return selected;
        }

        public static double RoundUpToMultipleOf50(double number)
        {
            double remainder = number % 50;
            if (remainder > 0)
                return number + (50 - remainder);
            return number;
        }

        private static void FacesFromSolids(Solid solid, List<Face> faces)
        {
            foreach (Face face in solid.Faces)
            {
                ElementId mid = face.MaterialElementId;
                if (mid == null)
                {
                    continue;
                }

                faces.Add(face);
            }
        }

        static private bool DoFacesIntersect(Face faceA, Face faceB)
        {
            foreach (EdgeArray edges in faceB.EdgeLoops)
            {
                foreach (Edge edge in edges)
                {
                    IList<XYZ> epts = edge.Tessellate();
                    foreach (XYZ ept in epts)
                    {
                        IntersectionResult intResult = faceA.Project(ept);
                        if (intResult != null)
                        {
                            if (Math.Abs(intResult.Distance) < Double.Epsilon)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }

    public class LinkGroupesDto
    {
        public List<Element> TouchingElems { get; set; }
    }

    public class IntersectDto
    {
        public Element PipeOrDuct { get; set; }
        public Element WallOrFloor { get; set; }
        public Transform Transform { get; set; }
        public List<Solid> IntersectingSolids { get; set; }
    }

    public class GroupedIntersectDto
    {
        public List<Element> PipesOrDucts { get; set; }
        public List<Element> WallsOrFloors { get; set; }
        public List<Solid> IntersectingSolids { get; set; }
    }

    public class CheckTouching
    {
        public Element Element { get; set; }
        public Transform Transform { get; set; }
    }

    public class LinkElemsWithTransform
    {
        public List<Element> Elements { get; set; }
        public RevitLinkInstance RevitLinkInstance { get; set; }
        public Document Document { get; set; }
        public Transform Transform { get; set; }
    }

    public class DocumentDtos
    {
        public RevitLinkInstance RevitLinkInstance { get; set; }
        public Document Document { get; set; }
        public Transform Transform { get; set; }
    }

    public class ExcelData
    {
        public string Type { get; set; }
        public string Diameter { get; set; }
        public string Thickness { get; set; }
        public string Size { get; set; }
        public string BiCode { get; set; }
    }
}
