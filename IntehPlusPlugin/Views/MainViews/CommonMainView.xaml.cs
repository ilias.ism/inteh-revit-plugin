﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using IntehPlus.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class CommonMainView : Window
    {
        ExternalCommandData _commandData;
        readonly Document _doc;

        public CommonMainView(Document doc, ExternalCommandData commandData)
        {
            InitializeComponent();
            _commandData = commandData;
            _doc = doc;
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Type_Rename_Click(object sender, RoutedEventArgs e)
        {
            var view = new TypeRenameView(_doc);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void Axis_Rename_Click(object sender, RoutedEventArgs e)
        {
            var view = new AxisRenameView(_doc);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void Parameter_ID_Click(object sender, RoutedEventArgs e)
        {
            var view = new ParameterIdView(_doc);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void Double_Element_Click(object sender, RoutedEventArgs e)
        {
            var allWalls = new FilteredElementCollector(_doc)
                        .OfCategory(BuiltInCategory.OST_Walls).WhereElementIsNotElementType().ToElements()
                        .Where(x => !x.Name.ToLower().Contains("витраж"));

            var allColumns = new FilteredElementCollector(_doc)
                        .OfCategory(BuiltInCategory.OST_StructuralColumns).WhereElementIsNotElementType().ToElements();

            List<Element> listToDeleteWalls = new List<Element>();
            List<ElementId> whiteListWalls = new List<ElementId>();

            foreach (var wall in allWalls)
            {
                foreach (var wall2 in allWalls)
                {
                    if (wall.Id != wall2.Id)
                    {
                        var locationCurve = wall.Location as LocationCurve;
                        var locationCurve2 = wall2.Location as LocationCurve;

                        var curve = locationCurve.Curve;
                        var curve2 = locationCurve2.Curve;

                        var len = Math.Round(curve.Length * 304.8);
                        var len2 = Math.Round(curve2.Length * 304.8);

                        var line = curve as Line;
                        var line2 = curve2 as Line;

                        var dir = line.Direction;
                        var dir2 = line2.Direction;

                        var X = Math.Round(dir.X);
                        var Y = Math.Round(dir.Y);
                        var Z = Math.Round(dir.Z);

                        var X2 = Math.Round(dir2.X);
                        var Y2 = Math.Round(dir2.Y);
                        var Z2 = Math.Round(dir2.Z);

                        var origin = line.Origin;
                        var origin2 = line2.Origin;

                        var xOrigin = Math.Round(origin.X, 5);
                        var yOrigin = Math.Round(origin.Y, 5);
                        var zOrigin = Math.Round(origin.Z, 5);

                        var xOrigin2 = Math.Round(origin2.X, 5);
                        var yOrigin2 = Math.Round(origin2.Y, 5);
                        var zOrigin2 = Math.Round(origin2.Z, 5);

                        if (len == len2 && wall.LevelId == wall2.LevelId && X == X2 && Y == Y2 && Z == Z2 && xOrigin == xOrigin2 && yOrigin == yOrigin2 && zOrigin == zOrigin2)
                        {
                            if(!(whiteListWalls.Contains(wall.Id) || whiteListWalls.Contains(wall2.Id)))
                            {
                                listToDeleteWalls.Add(wall2);
                                whiteListWalls.Add(wall.Id);
                            }
                        }
                    }
                }
            }

            List<ElementId> listToDeleteColumns = new List<ElementId>();
            List<ElementPointsDto> columnsPointList = new List<ElementPointsDto>();
            List<ElementId> whiteListColumns = new List<ElementId>();
            foreach (var column in allColumns)
            {
                ElementPointsDto elementInfo = new ElementPointsDto()
                {
                    ElementId = column.Id,
                    PointList = new List<XYZ>(),
                    LevelId = column.LevelId
                };
                List<GeometryInstance> tempList = column.get_Geometry(new Options()).Where(o => o is GeometryInstance).Cast<GeometryInstance>().ToList();
                foreach (GeometryInstance temp in tempList)
                {
                    foreach (GeometryObject geometryObject in temp.GetInstanceGeometry())
                    {
                        Solid solidAsSolid = geometryObject as Solid;
                        if (solidAsSolid != null && solidAsSolid.Edges != null)
                            foreach (Edge edge in solidAsSolid.Edges)
                            {
                                var startPoint = edge.AsCurve().GetEndPoint(0);
                                var endPoint = edge.AsCurve().GetEndPoint(1);

                                elementInfo.PointList.Add(endPoint);
                                elementInfo.PointList.Add(startPoint);
                            }
                    }
                }
                columnsPointList.Add(elementInfo);
            }

            foreach (var column in columnsPointList)
            {
                foreach (var column2 in columnsPointList)
                {
                    if (column.ElementId != column2.ElementId && column.LevelId != column2.LevelId)
                        if (column.PointList.Where(x => column2.PointList.Any(y => x.X == y.X && x.Y == y.Y && x.Z == y.Z)).Count() > 0)
                            if (!(whiteListColumns.Contains(column.ElementId) || whiteListColumns.Contains(column2.ElementId)))
                            {
                                listToDeleteColumns.Add(column2.ElementId);
                                whiteListColumns.Add(column.ElementId);
                            }
                }
            }

            int wallCounter = 0;
            using (Transaction t = new Transaction(_doc, "Удаление дублирований стен"))
            {
                t.Start();
                foreach (var elem in listToDeleteWalls)
                {
                    try
                    {
                        _doc.Delete(elem.Id);
                        wallCounter += 1;
                    }
                    catch
                    {

                    }
                }
                t.Commit();
            }

            int columnCounter = 0;
            using (Transaction t = new Transaction(_doc, "Удаление дублирований колонн"))
            {
                t.Start();
                foreach (var elem in listToDeleteColumns)
                {
                    try
                    {
                        _doc.Delete(elem);
                        columnCounter += 1;
                    }
                    catch
                    {

                    }
                }
                t.Commit();
            }

            MessageBox.Show("Удалено дублирований стен " + wallCounter.ToString() + "\n" + "Удалено дублирований колонн " + columnCounter.ToString());
            Close();
        }

        private void DeleteElemsByParam_Click(object sender, RoutedEventArgs e)
        {
            var view = new DeleteElemsByParamView(_doc);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void ClearWorkingSet_Click(object sender, RoutedEventArgs e)
        {
            var view = new ClearWorkingSetView(_doc);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void ViewSetCreate_Click(object sender, RoutedEventArgs e)
        {
            var view = new ViewSetCreateView(_doc, _commandData);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void FOPAdding_Click(object sender, RoutedEventArgs e)
        {
            var view = new FOPAddingView(_doc, _commandData);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void SpecParamsAdd_Click(object sender, RoutedEventArgs e)
        {
            var view = new SpecParamsAddView(_doc, _commandData);
            view.ShowDialog();
            view.Close();
            Close();
        }

        private void ParamCopy_Click(object sender, RoutedEventArgs e)
        {
            var view = new ParamCopyView(_doc, _commandData);
            view.ShowDialog();
            view.Close();
            Close();
        }
    }
}
