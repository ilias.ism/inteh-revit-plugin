﻿using Autodesk.Revit.DB;
using ClosedXML.Excel;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class CodesExcelModelView : Window
    {
        Document _doc;
        public CodesExcelModelView(Document doc)
        {
            InitializeComponent();
            KeyUp += MainWindow_KeyUp;
            _doc = doc;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            var list = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                            .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                            .Where(c => c.IsValidObject);
            List<Element> elemsList = list.Where(c => !string.IsNullOrEmpty(c.Name)
                                && c.DemolishedPhaseId.IntegerValue == -1
                                && !string.IsNullOrEmpty(c.Category?.Name)).ToList();
            var paramsList = CodesParams.Text.Split(',');
            var result = new List<ElemsDto>();
            foreach (var elem in elemsList)
            {
                foreach (var paramValue in paramsList)
                {
                    var parameter = elem.GetParameters(paramValue);
                    if (parameter.Any())
                    {
                        string valueTotal = "";
                        var value1 = parameter.FirstOrDefault()?.AsValueString();
                        var value2 = parameter.FirstOrDefault()?.AsString();
                        if (!string.IsNullOrEmpty(value1))
                            valueTotal = value1;
                        else if (!string.IsNullOrEmpty(value2))
                            valueTotal = value2;

                        var type = _doc.GetElement(elem.GetTypeId());
                        var family = type as ElementType;
                        result.Add(new ElemsDto()
                        {
                            CategoryName = elem.Category.Name,
                            FamilyName = family.FamilyName,
                            TypeName = _doc.GetElement(elem.GetTypeId()).Name,
                            CodeValue = valueTotal,
                        });
                    }
                    else
                    {
                        var type = _doc.GetElement(elem.GetTypeId());
                        if (type != null)
                        {
                            var parameterType = type.GetParameters(paramValue);
                            if (parameterType.Any())
                            {
                                string valueTotal = "";
                                var value1 = parameterType.FirstOrDefault()?.AsValueString();
                                var value2 = parameterType.FirstOrDefault()?.AsString();
                                if (!string.IsNullOrEmpty(value1))
                                    valueTotal = value1;
                                else if (!string.IsNullOrEmpty(value2))
                                    valueTotal = value2;

                                var family = type as ElementType;
                                result.Add(new ElemsDto()
                                {
                                    FamilyName = family.FamilyName,
                                    TypeName = _doc.GetElement(elem.GetTypeId()).Name,
                                    CodeValue = valueTotal,
                                    CategoryName = elem.Category.Name,
                                });
                            }
                        }
                    }
                }
            }

            IXLWorkbook workbook = new XLWorkbook();
            IXLWorksheet worksheet = workbook.Worksheets.Add("Лист 1");
            worksheet.Cell(1, 1).Value = "Категория";
            worksheet.Cell(1, 2).Value = "Семейство";
            worksheet.Cell(1, 3).Value = "Имя типа";
            worksheet.Cell(1, 4).Value = "Код для BI_код_оборудования";

            var counter = 2;
            var groupedByCategory = result.GroupBy(x => x.CategoryName).ToList();
            foreach (var groupByCat in groupedByCategory)
            {
                var groupedByFamily = groupByCat.GroupBy(x => x.FamilyName).ToList();
                foreach (var groupByFamily in groupedByFamily)
                {
                    var groupedByType = groupByFamily.GroupBy(x => x.TypeName).ToList();
                    foreach (var groupByType in groupedByType)
                    {
                        worksheet.Cell(counter, 1).Value = groupByCat.Key;
                        worksheet.Cell(counter, 2).Value = groupByFamily.Key;
                        worksheet.Cell(counter, 3).Value = groupByType.Key;
                        var value = groupByType.FirstOrDefault(x => !string.IsNullOrEmpty(x.CodeValue));
                        worksheet.Cell(counter, 4).Value = value == null ? "" : value.CodeValue;
                        counter += 1;
                    }
                }
            }

            var saveFileDialog = new System.Windows.Forms.SaveFileDialog
            {
                Filter = "Excel Files(.xlsx)| *.xlsx",
                FileName = _doc.Title.Replace(".rvt", "")
            };
            if (saveFileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) 
                return;

            workbook.SaveAs(saveFileDialog.FileName);
            Close();
        }

        public class ElemsDto
        {
            public string CategoryName { get; set; }
            public string FamilyName { get; set; }
            public string TypeName { get; set; }
            public string CodeValue { get; set; }
        }
    }
}
