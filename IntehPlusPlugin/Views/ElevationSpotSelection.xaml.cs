﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using IntehPlus.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class ElevationSpotSelection : Window
    {
        Document _doc;
        ExternalCommandData _commandData;
        OVVKMainView _ovvkView;
        public ElevationSpotSelection(Document doc, ExternalCommandData commandData, OVVKMainView ovvkView)
        {
            _doc = doc;
            _commandData = commandData;
            _ovvkView = ovvkView;
            var elevSpotsFirstSelection = new FilteredElementCollector(doc).OfClass(typeof(DimensionType)).Select(x => x as DimensionType).ToList();
            var elevSpots = elevSpotsFirstSelection.Where(x => x.StyleType == DimensionStyleType.SpotElevation).Select(x => x.Name).ToList();
            InitializeComponent();
            SpotName.ItemsSource = elevSpots;
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            var selectedSpot = SpotName.Text;

            try
            {
                Dictionary<Level, ModelLine> levelToElementPair = new Dictionary<Level, ModelLine>();
                var elevSpotss = new FilteredElementCollector(_doc).OfClass(typeof(SpotDimensionType)).ToList();
                var spotType = elevSpotss.Where(x => x.Name == selectedSpot).FirstOrDefault();
                var selectedElem = SelectElemFromRevit();
                var selectedElemLocation = selectedElem.Location as LocationCurve;
                var selectedElemCurve = selectedElemLocation.Curve as Line;
                var point = selectedElemCurve.Origin;

                var allLevels = RoomUtils.GetAllLevels(_doc);
                foreach (var level in allLevels)
                {
                    XYZ p1 = new XYZ((point.X * 304.8 + 2) / 304.8, point.Y, level.Elevation);
                    XYZ p2 = new XYZ((point.X * 304.8 - 2) / 304.8, point.Y, level.Elevation);
                    Line geomLine = Line.CreateBound(p1, p2);
                    XYZ dir = geomLine.Direction;
                    double x = dir.X, y = dir.Y, z = dir.Z;
                    XYZ normal = new XYZ(z - y, x - z, y - x);


                    Plane plane1 = Plane.CreateByNormalAndOrigin(p1, normal);
                    var planeRef = level.GetPlaneReference();
                    using (Transaction tra = new Transaction(_doc))
                    {
                        tra.Start("Выставление высотных отметок");
                        SketchPlane sketch = SketchPlane.Create(_doc, planeRef);
                        ModelLine line = _doc.Create.NewModelCurve(geomLine, sketch) as ModelLine;
                        levelToElementPair.Add(level, line);
                        tra.Commit();
                    }
                }

                using (Transaction tra = new Transaction(_doc))
                {
                    tra.Start("Выставление высотных отметок");

                    foreach (var pair in levelToElementPair)
                    {
                        Reference REF = new Reference(pair.Value);
                        SpotDimension spot = _doc.Create.NewSpotElevation(
                                    _doc.ActiveView,
                                    REF,
                                    new XYZ(point.X, point.Y, pair.Key.ProjectElevation),
                                    new XYZ(point.X, point.Y, pair.Key.ProjectElevation),
                                    new XYZ(point.X, point.Y, pair.Key.ProjectElevation),
                                    new XYZ(point.X, point.Y, pair.Key.ProjectElevation),
                                    true);
                        if (spotType != null)
                            spot.ChangeTypeId(spotType.Id);
                    }
                    tra.Commit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Решение высотной отметки для выбранной категории не найдено. Произошла ошибка: " + ex.Message);
            }
            Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private Element SelectElemFromRevit()
        {
            MessageBox.Show("Выберите элемент основу");
            UIDocument uidoc = _commandData.Application.ActiveUIDocument;
            Selection sel = uidoc.Selection;
            Hide();
            _ovvkView.Hide();
            Reference reference = sel.PickObject(ObjectType.Element, "Please select some elements");
            _ovvkView.Show();
            Show();
            var selectedElem = _doc.GetElement(reference.ElementId);
            return selectedElem;
        }
    }
}