﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace IntehPlus.Views
{
    /// <summary>
    /// Логика взаимодействия для AxisRenameView.xaml
    /// </summary>
    public partial class FOPAddingView : Window
    {
        Document _doc;
        ExternalCommandData _commandData;
        DefinitionGroups _myGroups;
        ExternalDefinition _selectedDefinition;
        List<string> _selectedCategories = new List<string>();
        bool _onType = false;
        public FOPAddingView(Document doc, ExternalCommandData commandData)
        {
            InitializeComponent();
            _doc = doc;
            _commandData = commandData;
            InitFOP();
            InitFamilies();
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void InitFamilies()
        {
            var categories = new List<string>();
            FilteredElementCollector collector = new FilteredElementCollector(_doc);
            ICollection<Element> elements = collector.OfClass(typeof(Family)).ToElements();
            foreach (Family element in elements)
            {
                var category = element.FamilyCategory;
                if (!category.IsTagCategory && element.IsEditable && element.IsUserCreated && category.CategoryType == CategoryType.Model)
                    categories.Add(category.Name);
            }

            var resultCats = categories.Distinct().ToList(); ;
            foreach (var category in resultCats)
            {
                var checkbox = new CheckBox
                {
                    Height = 20,
                    IsChecked = false,
                    Content = category
                };
                checkbox.Click += CheckBox_Click;
                _selectedCategories.Clear();
                CategoriesStackPanel.Children.Add(checkbox);
            }
        }

        private void InitFOP()
        {
            DefinitionFile defFile = _commandData.Application.Application.OpenSharedParameterFile();
            _myGroups = defFile.Groups;
            var listOfGroups = _myGroups.Select(x => x.Name).ToList();
            FOPParts.ItemsSource = listOfGroups;
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (_selectedDefinition == null)
            {
                MessageBox.Show("Параметр не выбран");
                return;
            }
            if (!_selectedCategories.Any())
            {
                MessageBox.Show("Категории не выбраны");
                return;
            }

            MessageBox.Show("Если параметра нет - будет добавлен, если параметр присутсвует по типу, а вы выбрали по экземпляру - будет удален и добавлен заново и наоборот, если параметр есть - семейство игнорируется");
            AddParams();
            Close();
        }

        private void AddParams()
        {
            FilteredElementCollector collector = new FilteredElementCollector(_doc);
            ICollection<Element> families = collector.OfClass(typeof(Family)).ToElements();
            var counter = 0;
            foreach (Family family in families)
            {
                var category = family.FamilyCategory;
                if (_selectedCategories.Contains(category.Name))
                {
                    var familyDoc = _doc.EditFamily(family);
                    try
                    {
                        using (Transaction transaction = new Transaction(familyDoc, "BI_код_оборудования добавление"))
                        {
                            transaction.Start();
                            var param = familyDoc.FamilyManager.get_Parameter(_selectedDefinition.Name);
                            if (param == null)
                            {
                                familyDoc.FamilyManager.AddParameter(_selectedDefinition, _selectedDefinition.ParameterGroup, !_onType);
                                counter += 1;
                            }
                            else
                            {
                                if (_onType && param.IsInstance || !_onType && !param.IsInstance)
                                {
                                    familyDoc.FamilyManager.RemoveParameter(param);
                                    familyDoc.FamilyManager.AddParameter(_selectedDefinition, _selectedDefinition.ParameterGroup, !_onType);
                                    counter += 1;
                                }
                            }
                            transaction.Commit();
                            familyDoc.LoadFamily(_doc, new FamilyLoadOptions());
                            familyDoc.Close(false);
                        }
                    }
                    catch (Exception ex)
                    {
                        familyDoc.Close(false);
                    }
                }
            }
            MessageBox.Show("Готово. Отредактировано семейств: " + counter.ToString());
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            if (checkBox.IsChecked == true)
                _selectedCategories.Add(checkBox.Content.ToString());
            if (checkBox.IsChecked == false)
                _selectedCategories.Remove(checkBox.Content.ToString());
            _selectedCategories.Distinct();
        }

        private void ValidationFOPParts(object sender, SelectionChangedEventArgs e)
        {
            DefinitionGroup myGroup = _myGroups.get_Item(FOPParts.SelectedValue.ToString());
            Definitions myDefinitions = myGroup.Definitions;
            var listOfDefs = myDefinitions.Select(x => x.Name).ToList();
            ParameterCombobox.ItemsSource = listOfDefs;
        }

        private void ValidationParameter(object sender, SelectionChangedEventArgs e)
        {
            DefinitionGroup myGroup = _myGroups.get_Item(FOPParts.Text);
            Definitions myDefinitions = myGroup.Definitions;
            if (ParameterCombobox.SelectedValue == null)
            {
                _selectedDefinition = null;
                SelectedData.Text = "Выбран параметр: ";
            }
            else
            {
                _selectedDefinition = myDefinitions.get_Item(ParameterCombobox.SelectedValue.ToString()) as ExternalDefinition;
                SelectedData.Text = "Выбран параметр: " + _selectedDefinition.Name;
            }
        }

        private void CheckedOnType(object sender, RoutedEventArgs e)
        {
            if (OnType.IsChecked == true)
                _onType = true;
            if (OnInstance != null)
                OnInstance.IsChecked = false;
        }

        private void CheckedOnInstance(object sender, RoutedEventArgs e)
        {
            if (OnInstance.IsChecked == true)
                _onType = false;
            if (OnType != null)
                OnType.IsChecked = false;
        }

        private void SetAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var child in CategoriesStackPanel.Children)
            {
                if (child is CheckBox checkBox)
                {
                    checkBox.IsChecked = true;
                    _selectedCategories.Add(checkBox.Content.ToString());
                }
            }
            _selectedCategories.Distinct();
        }

        private void DropAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var child in CategoriesStackPanel.Children)
            {
                if (child is CheckBox checkBox)
                {
                    checkBox.IsChecked = false;
                }
            }
            _selectedCategories.Clear();
        }
    }


    class FamilyLoadOptions : IFamilyLoadOptions
    {
        public bool OnFamilyFound(bool familyInUse, out bool overwriteParameterValues)
        {
            overwriteParameterValues = true;
            return true;
        }

        public bool OnSharedFamilyFound(Family sharedFamily, bool familyInUse, out FamilySource source, out bool overwriteParameterValues)
        {
            source = FamilySource.Family;
            overwriteParameterValues = true;
            return true;
        }
    }
}
