﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    /// <summary>
    /// Логика взаимодействия для AxisRenameView.xaml
    /// </summary>
    public partial class AxisRenameView : Window
    {
        Document _doc;
        public AxisRenameView(Document doc)
        {
            InitializeComponent();
            KeyUp += MainWindow_KeyUp;
            _doc = doc;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            var allAxis = new FilteredElementCollector(_doc)
                        .OfCategory(BuiltInCategory.OST_Grids)
                        .WhereElementIsNotElementType().OfType<Autodesk.Revit.DB.Grid>();

            var verticalAxes = allAxis.Where(x => Math.Round(x.Curve.GetEndPoint(1).Subtract(x.Curve.GetEndPoint(0)).Normalize().Y, 1) == 1 || Math.Round(x.Curve.GetEndPoint(1).Subtract(x.Curve.GetEndPoint(0)).Normalize().Y, 1) == -1);
            var horizontalAxes = allAxis.Where(x => Math.Round(x.Curve.GetEndPoint(1).Subtract(x.Curve.GetEndPoint(0)).Normalize().X, 1) == 1 || Math.Round(x.Curve.GetEndPoint(1).Subtract(x.Curve.GetEndPoint(0)).Normalize().X, 1) == -1);

            var verticalOrdered = verticalAxes.OrderBy(x => x.Curve.GetEndPoint(1).X);
            var horizontalOrdered = horizontalAxes.OrderBy(x => x.Curve.GetEndPoint(1).Y);

            var startValueVertical = VerticalName.Text;
            var startValueHorizontal = HorizontalName.Text.ToUpper();

            var calVertical = Convert.ToInt32(startValueVertical);

            using (Transaction tra = new Transaction(_doc))
            {
                tra.Start("Переименование осей часть 1");

                foreach (var grid in verticalOrdered)
                {
                    grid.Name = "..." + grid.Name;
                }
                foreach (var grid in horizontalOrdered)
                {
                    grid.Name = "..." + grid.Name;
                }

                tra.Commit();
            }

            using (Transaction tra = new Transaction(_doc))
            {
                tra.Start("Переименование осей часть 2");

                foreach (var grid in verticalOrdered)
                {
                    grid.Name = VerticalPrefix.Text + calVertical.ToString() + VerticaSuffix.Text;
                    calVertical += 1;
                }

                tra.Commit();
            }

            List<string> alhpabet = new List<string>() { "А", "Б", "В", "Г", "Д", "Е", "Ж", "И", "К", "Л", "М", "Н", "П", "Р", "С", "Т", "У", "Ф", "Э", "Ю", "Я" };
            var startValueIndex = alhpabet.IndexOf(startValueHorizontal);
            using (Transaction tra = new Transaction(_doc))
            {
                tra.Start("Переименование осей часть 3");

                foreach (var grid in horizontalOrdered)
                {
                    grid.Name = HorizontalPrefix.Text + alhpabet[startValueIndex] + HorizontalSuffix.Text;
                    startValueIndex += 1;
                }

                tra.Commit();
            }

            Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
