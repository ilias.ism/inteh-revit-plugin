﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class RoomFinishingView : Window
    {
        readonly Document doc;
        List<Level> levels;
        List<Room> rooms;
        List<Wall> walls;

        public RoomFinishingView(Document doc)
        {
            InitializeComponent();
            this.doc = doc;
            levels = GetAllLevels(doc);
            rooms = GetAllProjectRooms(doc);
            walls = GetAllProjectWallTypes(doc);
            var roomWallsType = GetAllRoomsSideElementsType(rooms);
            SetViewElements();
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void SetViewElements()
        {

        }

        private List<Level> GetAllLevels(Document document)
        {
            FilteredElementCollector collector = new FilteredElementCollector(document);
            List<Level> collection = collector.OfClass(typeof(Level)).OfType<Level>().OrderBy(lev => lev.Elevation).ToList();
            return collection;
        }

        private List<Element> GetAllRoomsSideElementsType(List<Room> rooms)
        {
            List<Element> resultList = new List<Element>();
            foreach (var room in rooms)
            {
                var roomSideELems = room.GetBoundarySegments(new SpatialElementBoundaryOptions());
                foreach (var elem in roomSideELems[0])
                {
                    var elemType = doc.GetElement(elem.ElementId);
                    resultList.Add(elemType);
                }
            }
            return resultList;
        }

        private List<Room> GetAllProjectRooms(Document doc)
        {
            return new FilteredElementCollector(doc)
                        .OfCategory(BuiltInCategory.OST_Rooms)
                        .WhereElementIsNotElementType()
                        .OfType<Room>().Where(x => x.Area != 0).ToList();
        }

        private List<Wall> GetAllProjectWallTypes(Document doc)
        {
            return new FilteredElementCollector(doc)
                        .OfCategory(BuiltInCategory.OST_Walls)
                        .WhereElementIsNotElementType()
                        .OfType<Wall>().ToList();
        }
    }
}
