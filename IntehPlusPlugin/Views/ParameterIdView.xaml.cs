﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    /// <summary>
    /// Логика взаимодействия для ParameterIdView.xaml
    /// </summary>
    public partial class ParameterIdView : Window
    {
        Document _doc;
        public ParameterIdView(Document doc)
        {
            _doc = doc;
            InitializeComponent();
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            var parameterName = ParameterName.Text;
            if (parameterName != null && parameterName != "")
            {
                List<ElementId> resultList = new List<ElementId>();
                FilteredElementCollector paramsList = new FilteredElementCollector(_doc).OfClass(typeof(SharedParameterElement));
                foreach (var param in paramsList)
                {
                    if (param.Name == parameterName)
                    {
                        resultList.Add(param.Id);
                    }
                }

                var message = "Найденные ID: \n" + string.Join(Environment.NewLine, resultList);
                MessageBox.Show(message);
                Close();
            }
            else
            {
                MessageBox.Show("Имя параметра введено некорректно");
                return;
            }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
