﻿using Autodesk.Revit.DB;
using IntehPlus.Dto;
using IntehPlusPlugin.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlusPlugin.Views
{
    /// <summary>
    /// Логика взаимодействия для FinishingAlignView.xaml
    /// </summary>
    public partial class FinishingAlignView : Window
    {
        private Document _doc;

        public FinishingAlignView(Document doc)
        {
            InitializeComponent();
            _doc = doc;
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void SavePoints_Click(object sender, RoutedEventArgs e)
        {
            var allWalls = new FilteredElementCollector(_doc, _doc.ActiveView.Id)
                        .OfCategory(BuiltInCategory.OST_Walls).WhereElementIsNotElementType().ToElements()
                        .Where(x => !x.Name.ToLower().Contains("витраж") && !x.Name.ToLower().Contains("отделк"));

            var allFinish = new FilteredElementCollector(_doc, _doc.ActiveView.Id)
                        .OfCategory(BuiltInCategory.OST_Walls).WhereElementIsNotElementType().ToElements()
                        .Where(x => x.Name.ToLower().Contains("отделк"));

            var allColumns = new FilteredElementCollector(_doc, _doc.ActiveView.Id)
                        .OfCategory(BuiltInCategory.OST_StructuralColumns).WhereElementIsNotElementType().ToElements();

            using (Transaction tra = new Transaction(_doc))
            {
                tra.Start("Сохранение положения стен");

                foreach (var finishWall in allFinish)
                {
                    finishWall.LookupParameter("Комментарии").Set("");
                    foreach (var wall in allWalls)
                    {
                        if ((CheckWallsTouching(finishWall, wall) || CheckWallsTouching(wall, finishWall)) && CheckDirection(finishWall, wall))
                        {
                            var wallBBMax = finishWall.get_BoundingBox(null).Max;
                            var wallBBMin = finishWall.get_BoundingBox(null).Min;
                            var wallBBMiddle = new XYZ((wallBBMax.X + wallBBMin.X) / 2, (wallBBMax.Y + wallBBMin.Y) / 2, (wallBBMax.Z + wallBBMin.Z) / 2);
                            var result = new WallDataDto()
                            {
                                X = wallBBMiddle.X,
                                Y = wallBBMiddle.Y,
                                Z = wallBBMiddle.Z,
                                HostElemId = wall.Id.ToString(),
                            };
                            string json = JsonConvert.SerializeObject(result);
                            finishWall.LookupParameter("Комментарии").Set(json);
                            break;
                        }
                    }

                    foreach (var column in allColumns)
                    {
                        if (CheckColumnTouching(finishWall, column))
                        {
                            var wallBBMax = finishWall.get_BoundingBox(null).Max;
                            var wallBBMin = finishWall.get_BoundingBox(null).Min;
                            var wallBBMiddle = new XYZ((wallBBMax.X + wallBBMin.X) / 2, (wallBBMax.Y + wallBBMin.Y) / 2, (wallBBMax.Z + wallBBMin.Z) / 2);
                            var result = new WallDataDto()
                            {
                                X = wallBBMiddle.X,
                                Y = wallBBMiddle.Y,
                                Z = wallBBMiddle.Z,
                                HostElemId = column.Id.ToString(),
                            };
                            string json = JsonConvert.SerializeObject(result);
                            finishWall.LookupParameter("Комментарии").Set(json);
                            break;
                        }
                    }
                }

                foreach (var wall in allWalls)
                {
                    var wallBBMax = wall.get_BoundingBox(null).Max;
                    var wallBBMin = wall.get_BoundingBox(null).Min;
                    var wallBBMiddle = new XYZ((wallBBMax.X + wallBBMin.X) / 2, (wallBBMax.Y + wallBBMin.Y) / 2, (wallBBMax.Z + wallBBMin.Z) / 2);
                    var result = new WallDataDto()
                    {
                        X = wallBBMiddle.X,
                        Y = wallBBMiddle.Y,
                        Z = wallBBMiddle.Z,
                        HostElemId = "",
                    };
                    string json = JsonConvert.SerializeObject(result);
                    wall.LookupParameter("Комментарии").Set(json);
                }

                foreach (var column in allColumns)
                {
                    var wallBBMax = column.get_BoundingBox(null).Max;
                    var wallBBMin = column.get_BoundingBox(null).Min;
                    var wallBBMiddle = new XYZ((wallBBMax.X + wallBBMin.X) / 2, (wallBBMax.Y + wallBBMin.Y) / 2, (wallBBMax.Z + wallBBMin.Z) / 2);
                    var result = new WallDataDto()
                    {
                        X = wallBBMiddle.X,
                        Y = wallBBMiddle.Y,
                        Z = wallBBMiddle.Z,
                        HostElemId = "",
                    };
                    string json = JsonConvert.SerializeObject(result);
                    column.LookupParameter("Комментарии").Set(json);
                }

                tra.Commit();
            }

            MessageBox.Show("Сохранение положения стен завершено, данные записаны внутрь элементов");
            Close();
        }

        private void Align_Click(object sender, RoutedEventArgs e)
        {
            var allWalls = new FilteredElementCollector(_doc, _doc.ActiveView.Id)
            .OfCategory(BuiltInCategory.OST_Walls).WhereElementIsNotElementType().ToElements()
            .Where(x => !x.Name.ToLower().Contains("витраж") && !x.Name.ToLower().Contains("отделк"));

            var allFinish = new FilteredElementCollector(_doc, _doc.ActiveView.Id)
                        .OfCategory(BuiltInCategory.OST_Walls).WhereElementIsNotElementType().ToElements()
                        .Where(x => x.Name.ToLower().Contains("отделк"));

            var allColumns = new FilteredElementCollector(_doc, _doc.ActiveView.Id)
                        .OfCategory(BuiltInCategory.OST_StructuralColumns).WhereElementIsNotElementType().ToElements();

            var counter = 0;
            using (Transaction tra = new Transaction(_doc))
            {
                tra.Start("Перемещение стен");

                foreach (var wall in allWalls)
                {
                    var wallBBMax = wall.get_BoundingBox(null).Max;
                    var wallBBMin = wall.get_BoundingBox(null).Min;
                    var wallBBMiddle = new XYZ((wallBBMax.X + wallBBMin.X) / 2, (wallBBMax.Y + wallBBMin.Y) / 2, (wallBBMax.Z + wallBBMin.Z) / 2);

                    var json = wall.LookupParameter("Комментарии").AsString();
                    var oldData = JsonConvert.DeserializeObject<WallDataDto>(json);
                    if (wallBBMiddle.X != oldData.X || wallBBMiddle.Y != oldData.Y)
                    {
                        var finishWalls = allFinish.Where(x => x.LookupParameter("Комментарии").AsString().Contains(wall.Id.ToString()));
                        foreach (var finishWall in finishWalls)
                        {
                            ElementTransformUtils.MoveElement(_doc, finishWall.Id, new XYZ(wallBBMiddle.X - oldData.X, wallBBMiddle.Y - oldData.Y, 0));
                            counter += 1;
                        }
                    }
                }

                tra.Commit();
            }



            MessageBox.Show("Перемещение завершено, элементов перемещено: " + counter.ToString());
            Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private bool CheckColumnTouching(Element wall, Element column)
        {
            if (wall.LevelId != column.LevelId) return false;

            List<XYZ> allWallPointsList = new List<XYZ>();
            List<double> allXPointsList = new List<double>();
            List<double> allYPointsList = new List<double>();
            List<double> allZPointsList = new List<double>();
            List<string> xList = new List<string>();
            List<string> yList = new List<string>();
            List<string> zList = new List<string>();

            GeometryElement wallElemGeom = wall.get_Geometry(new Options());
            foreach (var solid in wallElemGeom)
            {
                Solid solidAsSolid = solid as Solid;
                if (solidAsSolid != null && solidAsSolid.Edges != null)
                    foreach (Edge edge in solidAsSolid.Edges)
                    {
                        var startPoint = edge.AsCurve().GetEndPoint(0);
                        var endPoint = edge.AsCurve().GetEndPoint(1);

                        if (!(xList.Contains(startPoint.X.ToString()) && yList.Contains(startPoint.Y.ToString()) && zList.Contains(startPoint.Z.ToString())))
                        {
                            xList.Add(startPoint.X.ToString());
                            xList.Add(endPoint.X.ToString());
                            yList.Add(startPoint.Y.ToString());
                            yList.Add(endPoint.Y.ToString());
                            zList.Add(startPoint.Z.ToString());
                            xList.Add(endPoint.Z.ToString());
                            allWallPointsList.Add(startPoint);
                            allWallPointsList.Add(endPoint);
                        }
                    }
            }

            List<GeometryInstance> tempList = column.get_Geometry(new Options()).Where(o => o is GeometryInstance).Cast<GeometryInstance>().ToList();
            foreach (GeometryInstance temp in tempList)
            {
                foreach (GeometryObject geometryObject in temp.GetInstanceGeometry())
                {
                    Solid solidAsSolid = geometryObject as Solid;
                    if (solidAsSolid != null && solidAsSolid.Edges != null)
                        foreach (Edge edge in solidAsSolid.Edges)
                        {
                            var startPoint = edge.AsCurve().GetEndPoint(0);
                            var endPoint = edge.AsCurve().GetEndPoint(1);

                            allXPointsList.Add(startPoint.X);
                            allXPointsList.Add(endPoint.X);

                            allYPointsList.Add(startPoint.Y);
                            allYPointsList.Add(endPoint.Y);

                            allZPointsList.Add(startPoint.Z);
                            allZPointsList.Add(endPoint.Z);
                        }
                }
            }

            MaxMinPointsDto points = new MaxMinPointsDto
            {
                MaxX = Math.Round(allXPointsList.Max() * 304.8),
                MaxY = Math.Round(allYPointsList.Max() * 304.8),
                MaxZ = Math.Round(allZPointsList.Max() * 304.8),
                MinX = Math.Round(allXPointsList.Min() * 304.8),
                MinY = Math.Round(allYPointsList.Min() * 304.8),
                MinZ = Math.Round(allZPointsList.Min() * 304.8),
            };

            foreach (var wallPoint in allWallPointsList)
            {
                if ((points.MinX <= Math.Round(wallPoint.X * 304.8) && Math.Round(wallPoint.X * 304.8) <= points.MaxX) &&
                    (points.MinY <= Math.Round(wallPoint.Y * 304.8) && Math.Round(wallPoint.Y * 304.8) <= points.MaxY) &&
                    (points.MinZ <= Math.Round(wallPoint.Z * 304.8) && Math.Round(wallPoint.Z * 304.8) <= points.MaxZ))
                {
                    return true;
                }
            }

            return false;
        }

        private bool CheckWallsTouching(Element wall1, Element wall2)
        {
            if (wall1.LevelId != wall2.LevelId) return false;

            List<XYZ> allWallPointsList = new List<XYZ>();
            List<double> allXPointsList = new List<double>();
            List<double> allYPointsList = new List<double>();
            List<double> allZPointsList = new List<double>();
            List<string> xList = new List<string>();
            List<string> yList = new List<string>();
            List<string> zList = new List<string>();

            GeometryElement wallElemGeom = wall1.get_Geometry(new Options());
            foreach (var solid in wallElemGeom)
            {
                Solid solidAsSolid = solid as Solid;
                if (solidAsSolid != null && solidAsSolid.Edges != null)
                    foreach (Edge edge in solidAsSolid.Edges)
                    {
                        var startPoint = edge.AsCurve().GetEndPoint(0);
                        var endPoint = edge.AsCurve().GetEndPoint(1);

                        if (!(xList.Contains(startPoint.X.ToString()) && yList.Contains(startPoint.Y.ToString()) && zList.Contains(startPoint.Z.ToString())))
                        {
                            xList.Add(startPoint.X.ToString());
                            xList.Add(endPoint.X.ToString());
                            yList.Add(startPoint.Y.ToString());
                            yList.Add(endPoint.Y.ToString());
                            zList.Add(startPoint.Z.ToString());
                            xList.Add(endPoint.Z.ToString());
                            allWallPointsList.Add(startPoint);
                            allWallPointsList.Add(endPoint);
                        }
                    }
            }

            GeometryElement tempList = wall2.get_Geometry(new Options());
            foreach (var solid in tempList)
            {
                Solid solidAsSolid = solid as Solid;
                if (solidAsSolid != null && solidAsSolid.Edges != null)
                    foreach (Edge edge in solidAsSolid.Edges)
                    {
                        var startPoint = edge.AsCurve().GetEndPoint(0);
                        var endPoint = edge.AsCurve().GetEndPoint(1);

                        allXPointsList.Add(startPoint.X);
                        allXPointsList.Add(endPoint.X);

                        allYPointsList.Add(startPoint.Y);
                        allYPointsList.Add(endPoint.Y);

                        allZPointsList.Add(startPoint.Z);
                        allZPointsList.Add(endPoint.Z);
                    }
            }

            MaxMinPointsDto points = new MaxMinPointsDto
            {
                MaxX = Math.Round(allXPointsList.Max() * 304.8),
                MaxY = Math.Round(allYPointsList.Max() * 304.8),
                MaxZ = Math.Round(allZPointsList.Max() * 304.8),
                MinX = Math.Round(allXPointsList.Min() * 304.8),
                MinY = Math.Round(allYPointsList.Min() * 304.8),
                MinZ = Math.Round(allZPointsList.Min() * 304.8),
            };

            foreach (var wallPoint in allWallPointsList)
            {
                if ((points.MinX <= Math.Round(wallPoint.X * 304.8) && Math.Round(wallPoint.X * 304.8) <= points.MaxX) &&
                    (points.MinY <= Math.Round(wallPoint.Y * 304.8) && Math.Round(wallPoint.Y * 304.8) <= points.MaxY) &&
                    (points.MinZ <= Math.Round(wallPoint.Z * 304.8) && Math.Round(wallPoint.Z * 304.8) <= points.MaxZ))
                {
                    return true;
                }
            }

            return false;
        }

        private bool CheckDirection(Element wall1, Element wall2)
        {
            if (wall1.LevelId != wall2.LevelId) return false;
            LocationCurve locationCurve1 = wall1.Location as LocationCurve;
            LocationCurve locationCurve2 = wall2.Location as LocationCurve;

            var curve1 = locationCurve1.Curve as Line;
            var curve2 = locationCurve2.Curve as Line;

            var dir1 = curve1.Direction;
            var dir2 = curve2.Direction;

            if (Math.Abs(Math.Round(dir1.X)) == Math.Abs(Math.Round(dir2.X)) && Math.Abs(Math.Round(dir1.Y)) == Math.Abs(Math.Round(dir2.Y)))
                return true;


            return false;
        }
    }
}
