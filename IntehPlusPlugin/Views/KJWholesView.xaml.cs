﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class KJWholesView : Window
    {
        Document _doc;
        ExternalCommandData _commandData;
        public KJWholesView(Document doc, ExternalCommandData commandData)
        {
            _doc = doc;
            _commandData = commandData;
            InitializeComponent();
            KeyUp += MainWindow_KeyUp;
            SetWholeFamilies();
        }

        private void SetWholeFamilies()
        {
            var allWholes = new FilteredElementCollector(_doc).WhereElementIsNotElementType().ToElements().Where(x => x.Category != null && x.Category.Name == "Обобщенные модели").ToList();
            var familyNames = new List<string>();
            foreach (var element in allWholes)
            {
                FamilyInstance instance = element as FamilyInstance;
                var elementFamily = instance.Symbol.Family.Name;
                familyNames.Add(elementFamily);
            }
            var result = familyNames.Distinct().ToList();
            WholesSelection.ItemsSource = result;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(WholesSelection.Text.ToString()))
            {
                MessageBox.Show("Не выбрано семейство отверстий");
                return;
            }
            var floors = new FilteredElementCollector(_doc).WhereElementIsNotElementType().ToElements().Where(x => x.Category != null && x.Category.Name == "Перекрытия").ToList();
            var wholes = new FilteredElementCollector(_doc).WhereElementIsNotElementType().ToElements().Where(x => x.Category != null && x.Category.Name == "Обобщенные модели").ToList();
            var selectedWholes = new List<Element>();
            foreach (var element in wholes)
            {
                FamilyInstance instance = element as FamilyInstance;
                var elementFamily = instance.Symbol.Family.Name;
                if (elementFamily == WholesSelection.Text.ToString())
                    selectedWholes.Add(element);
            }

            using (Transaction transaction = new Transaction(_doc))
            {
                transaction.Start("Вырезание отверстий в перекрытиях");
                foreach (var whole in selectedWholes)
                {
                    foreach (var floor in floors)
                    {
                        try
                        {
                            JoinGeometryUtils.JoinGeometry(_doc, whole, floor);
                        }
                        catch
                        {

                        }
                    }
                }
                transaction.Commit();
            }
            Close();
        }
    }
}