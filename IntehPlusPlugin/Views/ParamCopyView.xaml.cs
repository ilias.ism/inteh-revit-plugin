﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace IntehPlus.Views
{
    /// <summary>
    /// Логика взаимодействия для AxisRenameView.xaml
    /// </summary>
    public partial class ParamCopyView : Window
    {
        Document _doc;
        ExternalCommandData _commandData;
        List<Element> _allSelectedElems = new List<Element>();
        List<Parameter> _ekzParamsListAsParam = new List<Parameter>();
        List<Parameter> _typeParamsListAsParam = new List<Parameter>();
        public ParamCopyView(Document doc, ExternalCommandData commandData)
        {
            InitializeComponent();
            _doc = doc;
            _commandData = commandData;
            ParamFrom.ItemsSource = new List<string>();
            ParamTo.ItemsSource = new List<string>();
            InitParamFrom();
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void InitParamFrom()
        {
            var uiDoc = _commandData.Application.ActiveUIDocument;
            var selectedIds = uiDoc.Selection.GetElementIds().ToList();
            foreach (var id in selectedIds)
            {
                var elem = _doc.GetElement(id);
                _allSelectedElems.Add(elem);
            }

            if (!_allSelectedElems.Any())
            {
                MessageBox.Show("Выберите элементы"); 
                Close();
            }

            var paramsList = new List<string>();
            foreach (var elem in _allSelectedElems)
            {
                var elemParams = elem.Parameters;
                foreach (Parameter elemParam in elemParams)
                {
                    paramsList.Add(elemParam.Definition.Name + " (экз.)");
                    _ekzParamsListAsParam.Add(elemParam);
                }

                var type = _doc.GetElement(elem.GetTypeId());
                var typeParams = type.Parameters;
                foreach (Parameter typeParam in typeParams)
                {
                    paramsList.Add(typeParam.Definition.Name + " (тип)");
                    _typeParamsListAsParam.Add(typeParam);
                }
            }

            ParamFrom.ItemsSource = paramsList.Distinct();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(ParamFrom.Text.ToString()))
            {
                MessageBox.Show("Не выбран параметр из которого копировать");
                return;
            }
            if (string.IsNullOrEmpty(ParamTo.Text.ToString()))
            {
                MessageBox.Show("Не выбран параметр в который копировать");
                return;
            }

            var paramFrom = ParamFrom.Text.Replace(" (тип)", "").Replace(" (экз.)", "");
            var paramTo = ParamTo.Text.Replace(" (тип)", "").Replace(" (экз.)", "");

            Transaction transaction = new Transaction(_doc, "Копирование параметров");
            transaction.Start();
            foreach (var elem in _allSelectedElems)
            {
                var selectedParamFrom = elem.GetParameters(paramFrom);
                var selectedParamTo = elem.GetParameters(paramTo);
                if (!selectedParamFrom.Any())
                {
                    var type = _doc.GetElement(elem.GetTypeId());
                    selectedParamFrom = type.GetParameters(paramFrom);
                }
                if (!selectedParamTo.Any())
                {
                    var type = _doc.GetElement(elem.GetTypeId());
                    selectedParamTo = type.GetParameters(paramTo);
                }

                if (selectedParamFrom.Any() && selectedParamTo.Any())
                {
                    try
                    {
                        var param1 = selectedParamFrom.First();
                        var param2 = selectedParamTo.First();
                        if (param1.StorageType == StorageType.String)
                        {
                            var value1 = param1.AsString();
                            param2.Set(value1);
                        }
                        else if (param1.StorageType == StorageType.Integer)
                        {
                            var value1 = param1.AsInteger();
                            param2.Set(value1);
                        }
                        else if (param1.StorageType == StorageType.Double)
                        {
                            var value1 = param1.AsDouble();
                            param2.Set(value1);
                        }
                        else if (param1.StorageType == StorageType.ElementId)
                        {
                            var value1 = param1.AsElementId();
                            param2.Set(value1);
                        }
                    }
                    catch
                    {

                    }
                }
            }
            transaction.Commit();
            MessageBox.Show("Завершено");
            Close();
        }

        private void ParamFromEvent(object sender, SelectionChangedEventArgs e)
        {
            ParamTo.SelectedItem = null;
            var selectedName = ParamFrom.SelectedValue.ToString();
            var fromEkzToType = false;
            if (selectedName.Contains(" (экз.)"))
                fromEkzToType = true;
            selectedName = selectedName.Replace(" (тип)", "").Replace(" (экз.)", "");

            var joined = new List<Parameter>();
            joined.AddRange(_ekzParamsListAsParam);
            joined.AddRange(_typeParamsListAsParam);
            var selectedParamStorageType = joined.Where(x => x.Definition.Name == selectedName).FirstOrDefault().StorageType;

            var paramsList = new List<string>();
            if (!fromEkzToType)
            {
                foreach (Parameter elemParam in _ekzParamsListAsParam)
                {
                    if (elemParam.StorageType == selectedParamStorageType && !elemParam.IsReadOnly)
                        paramsList.Add(elemParam.Definition.Name + " (экз.)");
                }
                foreach (Parameter elemParam in _typeParamsListAsParam)
                {
                    if (elemParam.StorageType == selectedParamStorageType && !elemParam.IsReadOnly)
                        paramsList.Add(elemParam.Definition.Name + " (тип)");
                }
            }
            else
            {
                foreach (Parameter elemParam in _ekzParamsListAsParam)
                {
                    if (elemParam.StorageType == selectedParamStorageType && !elemParam.IsReadOnly)
                        paramsList.Add(elemParam.Definition.Name + " (экз.)");
                }
            }

            ParamTo.ItemsSource = paramsList.Distinct();
        }
    }
}
