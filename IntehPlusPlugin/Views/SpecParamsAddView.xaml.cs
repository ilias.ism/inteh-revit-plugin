﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    /// <summary>
    /// Логика взаимодействия для AxisRenameView.xaml
    /// </summary>
    public partial class SpecParamsAddView : Window
    {
        Document _doc;
        ExternalCommandData _commandData;
        List<ViewSchedule> _selectedElems = new List<ViewSchedule>();
        public SpecParamsAddView(Document doc, ExternalCommandData commandData)
        {
            _doc = doc;
            _commandData = commandData;
            InitializeComponent();
            InitSchedules();
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void InitSchedules()
        {
            _selectedElems.Clear();
            var uiDoc = _commandData.Application.ActiveUIDocument;
            var selectedIds = uiDoc.Selection.GetElementIds().ToList();
            foreach (var id in selectedIds)
            {
                var elem = _doc.GetElement(id);
                if (elem.GetType().Name == "ViewSchedule")
                    _selectedElems.Add(elem as ViewSchedule);
            }

            if (!_selectedElems.Any())
            {
                MessageBox.Show("Среди выбранных элементов нет спецификаций");
                return;
            }    
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            using (Transaction transaction = new Transaction(_doc, "Adding fields to schedule"))
            {
                transaction.Start();
                foreach (var schedule in _selectedElems)
                {
                    schedule.Definition.AddField(ScheduleFieldType.Instance);

                }
                transaction.Commit();
            }

            Close();
        }
    }
}
