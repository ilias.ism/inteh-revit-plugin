﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class DeleteElemsByParamView : Window
    {
        Document _doc;
        List<Element> elemsList;
        List<string> valuesList = new List<string>();
        List<string> defaultParamsList = new List<string>();
        public DeleteElemsByParamView(Document doc)
        {
            _doc = doc;
            var list = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                            .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                            .Where(c => c.IsValidObject);
            elemsList = list.Where(c => !string.IsNullOrEmpty(c.Name)
                                && c.DemolishedPhaseId.IntegerValue == -1
                                && !string.IsNullOrEmpty(c.Category?.Name)).ToList();
            GetAllParams();
            InitializeComponent();
            ParamName.ItemsSource = defaultParamsList.OrderBy(x => x);
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void GetAllParams()
        {
            List<string> result = new List<string>();
            foreach (var elem in elemsList)
            {
                ParameterSet pSet = elem.Parameters;
                foreach (Parameter param in pSet) 
                {
                    var name = param.Definition.Name;
                    result.Add(name);
                }

                var typeId = elem.GetTypeId();
                if (typeId != null)
                {
                    var type = _doc.GetElement(typeId);
                    if (type != null)
                    {
                        ParameterSet pSetType = type.Parameters;
                        foreach (Parameter param in pSetType)
                        {
                            var name = param.Definition.Name;
                            result.Add(name);
                        }
                    }
                }
            }
            defaultParamsList = result.Distinct().ToList();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            var successList = 0;
            Transaction transaction = new Transaction(_doc, "Удаление элементов");
            transaction.Start();
            var paramName = ParamName.Text;
            foreach (var elem in elemsList)
            {
                try
                {
                    var param = elem.GetParameters(paramName).FirstOrDefault();
                    if (param != null)
                    {
                        var asStr = param.AsString();
                        var asValStr = param.AsValueString();
                        if (valuesList.Contains(asStr) || valuesList.Contains(asValStr))
                            continue;
                        else
                        {
                            _doc.Delete(elem.Id);
                            successList += 1;
                        }
                    }
                    else
                    {
                        var typeId = elem.GetTypeId();
                        var type = _doc.GetElement(typeId);
                        if (type != null)
                        {
                            Parameter typeParam = null;
                            try
                            {
                                typeParam = type.GetParameters(paramName).FirstOrDefault();
                            }
                            catch { }
                            if (typeParam != null)
                            {
                                var asStr = typeParam.AsString();
                                var asValStr = typeParam.AsValueString();
                                if (valuesList.Contains(asStr) || valuesList.Contains(asValStr))
                                    continue;
                                else
                                {
                                    _doc.Delete(elem.Id);
                                    successList += 1;
                                }
                            }
                        }
                    }
                }
                catch
                {

                }
            }
            transaction.Commit();

            MessageBox.Show("Успешно удалено: " + successList.ToString() + " объектов");
            Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ParamValuesChanged(object sender, SelectionChangedEventArgs e)
        {
            string text = (sender as ComboBox).SelectedItem as string;
            if (valuesList.Contains(text))
                valuesList.Remove(text);
            else
                if (!string.IsNullOrEmpty(text))
                valuesList.Add(text);
            var listAsString = string.Join(", ", valuesList.ToArray());
            SelectedValues.Text = "Выбранные значения: " + listAsString;
            if (valuesList.Any())
                Start_Button.IsEnabled = true;
            else
                Start_Button.IsEnabled = false;
            ParamValues.SelectedItem = null;
        }

        private void Calc_Click(object sender, RoutedEventArgs e)
        {
            var paramName = ParamName.Text;
            var paramsList = new List<string>();
            foreach (var elem in elemsList)
            {
                var param = elem.GetParameters(paramName).FirstOrDefault();
                if (param != null)
                {
                    var asStr = param.AsString();
                    var asValStr = param.AsValueString();
                    paramsList.Add(string.IsNullOrEmpty(asStr) ? asValStr : asStr);
                }
                else
                {
                    var typeId = elem.GetTypeId();
                    var type = _doc.GetElement(typeId);
                    if (type != null)
                    {
                        var typeParam = type.GetParameters(paramName).FirstOrDefault();
                        if (typeParam != null)
                        {
                            var asStr = typeParam.AsString();
                            var asValStr = typeParam.AsValueString();
                            paramsList.Add(string.IsNullOrEmpty(asStr) ? asValStr : asStr);
                        }
                    }
                }
            }

            var paramsCount = paramsList.Count.ToString();
            if (paramsList.Any())
            {
                ElemsCount.Text = "Найдено элементов: " + paramsCount;
                ParamValues.IsEnabled = true;
                ParamValues.ItemsSource = paramsList.Distinct();
            }
        }

        private void ValidationTextBox(object sender, SelectionChangedEventArgs e)
        {
            ParamValues.IsEnabled = false;
            valuesList.Clear();
        }
    }
}