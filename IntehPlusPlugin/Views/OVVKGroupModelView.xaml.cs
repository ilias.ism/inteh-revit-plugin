﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IntehPlus.Views
{
    public partial class OVVKGroupModelView : Window
    {
        Document _doc;
        public OVVKGroupModelView(Document doc)
        {
            InitializeComponent();
            _doc = doc;
            KeyUp += MainWindow_KeyUp;
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            var list = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                            .Where(c => c.CreatedPhaseId.IntegerValue != -1)
                            .Where(c => c.IsValidObject);
            List<Element> elemsList = list.Where(c => !string.IsNullOrEmpty(c.Name)
                                && c.DemolishedPhaseId.IntegerValue == -1
                                && !string.IsNullOrEmpty(c.Category?.Name)).ToList();

            using (Transaction tra = new Transaction(_doc))
            {
                tra.Start("Назначение групп модели");

                foreach (var elem in elemsList)
                {
                    if (elem.Category.Name == "Трубы")
                        SetElemGroupName(Трубы.Text, elem);
                    if (elem.Category.Name == "Гибкие трубы")
                        SetElemGroupName(Гибкие_трубы.Text, elem);
                    if (elem.Category.Name == "Материалы изоляции труб")
                        SetElemGroupName(Изоляция_труб.Text, elem);
                    if (elem.Category.Name == "Арматура трубопроводов")
                        SetElemGroupName(Арматура_труб.Text, elem);
                    if (elem.Category.Name == "Соединительные детали трубопроводов")
                        SetElemGroupName(Соединительные_детали_труб.Text, elem);
                    if (elem.Category.Name == "Оборудование")
                        SetElemGroupName(Оборудование.Text, elem);
                    if (elem.Category.Name == "Воздуховоды")
                        SetElemGroupName(Воздуховоды.Text, elem);
                    if (elem.Category.Name == "Гибкие воздуховоды")
                        SetElemGroupName(Гибкие_воздуховоды.Text, elem);
                    if (elem.Category.Name == "Материалы изоляции воздуховодов")
                        SetElemGroupName(Изоляция_воздуховодов.Text, elem);
                    if (elem.Category.Name == "Арматура воздуховодов")
                        SetElemGroupName(Арматура_воздуховодов.Text, elem);
                    if (elem.Category.Name == "Соединительные детали воздуховодов")
                        SetElemGroupName(Соединительные_детали_воздуховодов.Text, elem);
                    if (elem.Category.Name == "Воздухораспеделители")
                        SetElemGroupName(Воздухораспеделители.Text, elem);
                    if (elem.Category.Name == "Оборудование")
                        SetElemGroupNameByType(Радиаторы.Text, elem);
                }

                tra.Commit();
            }

            MessageBox.Show("Завершено");
            Close();
        }

        private void SetElemGroupName(string textBoxValue, Element elem)
        {
            try
            {
                var typeId = elem.GetTypeId();
                var typeElement = _doc.GetElement(typeId);
                var parameter = typeElement.GetParameters("Группа модели").FirstOrDefault();
                parameter?.Set(textBoxValue);
            }
            catch
            {

            }
        }

        private void SetElemGroupNameByType(string textBoxValue, Element elem)
        {
            try
            {
                var typeId = elem.GetTypeId();
                var typeElement = _doc.GetElement(typeId);
                if (typeElement.Name.Contains("(радиатор"))
                {
                    var parameter = typeElement.GetParameters("Группа модели").FirstOrDefault();
                    parameter?.Set(textBoxValue);
                }
            }
            catch
            {

            }
        }
    }
}
