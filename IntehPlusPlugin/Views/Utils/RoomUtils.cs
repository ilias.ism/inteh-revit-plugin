﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using IntehPlus.Dto;
using IntehPlus.Enums;
using IntehPlus.Views;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;

namespace IntehPlus.Utils
{
    public static class RoomUtils
    {

        public static List<Room> GetRoomFromLevel(this Document document)
        {
            var AllRooms = new FilteredElementCollector(document)
                                    .OfClass(typeof(SpatialElement))
                                    .WhereElementIsNotElementType()
                                    .Where(room => room.GetType() == typeof(Room))
                                    .Cast<Room>()
                                    .ToList();
            return AllRooms;
        }

        public static List<Level> GetAllLevels(Document document)
        {
            FilteredElementCollector collector = new FilteredElementCollector(document);
            List<Level> collection = collector.OfClass(typeof(Level)).OfType<Level>().OrderBy(lev => lev.Elevation).ToList();
            return collection;
        }

        public static List<Room> GetCertainRooms(List<Room> rooms, Dictionary<string, string> necessaryRoomTypes)
        {
            List<Room> result = new List<Room>();
            List<Room> temp = new List<Room>();
            foreach (var roomType in necessaryRoomTypes)
            {
                temp.AddRange(rooms.Where(x => x.LookupParameter("BI_индекс_помещения").AsString().StartsWith(roomType.Value)).ToList());
            }
            var groupedList = temp.GroupBy(x => x.LookupParameter("BI_квартира").AsValueString()).ToList();
            foreach (var group in groupedList)
            {
                result.Add(group.FirstOrDefault());
            }
            var mopAndTehRooms = rooms.Where(x => x.LookupParameter("BI_индекс_помещения").AsString().StartsWith("0")).ToList();
            result.AddRange(mopAndTehRooms);
            return result;
        }

        public static bool CheckParam(Room room, string[] paramNames)
        {
            try
            {
                foreach (var param in paramNames)
                {
                    var _ = room.LookupParameter(param);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static double GetDoubleAndConvert(Room room, string param)
        {
            return 0;
            //return UnitUtils.Convert(room.LookupParameter(param).AsDouble(), DisplayUnitType.DUT_SQUARE_FEET, DisplayUnitType.DUT_SQUARE_METERS);
        }

        public static ApartmentJsonDTO CreateApartmentJsonDTO(Room room)
        {
            ApartmentJsonDTO jsonRoomData = new ApartmentJsonDTO
            {
                ApartmentNumber = room.LookupParameter("BI_квартира").AsValueString(),
                RoomsCount = room.LookupParameter("BI_количество_комнат").AsValueString(),
                ApartmentArea = GetDoubleAndConvert(room, "BI_площадь_квартиры"),
                ApartmentLivingArea = GetDoubleAndConvert(room, "BI_площадь_квартиры_жилая"),
                ApartmentTotalArea = GetDoubleAndConvert(room, "BI_площадь_квартиры_общая"),
                ApartmentType = GetApartType(room.LookupParameter("BI_тип_помещения").AsString()),
                RoomAreaWithCoef = GetDoubleAndConvert(room, "BI_площадь_с_коэффициентом"),
                ApartmentTypeAsString = room.LookupParameter("BI_тип_помещения").AsString(),
            };
            return jsonRoomData;
        }

        public static ApartmentTypeEnum GetApartType(string typeName)
        {
            switch (typeName)
            {
                case "МОП":
                    return ApartmentTypeEnum.MOP;
                case "Техническое помещение":
                    return ApartmentTypeEnum.TechApp;
                case "Квартира":
                    return ApartmentTypeEnum.App;
                case "Пентхаус":
                    return ApartmentTypeEnum.Pent;
                case "Офис":
                    return ApartmentTypeEnum.Office;
                case "Паркинг":
                    return ApartmentTypeEnum.Parking;
                case "Кладовка":
                    return ApartmentTypeEnum.Pantry;
                case "Гараж":
                    return ApartmentTypeEnum.Garage;
                case "Цоколь":
                    return ApartmentTypeEnum.GroundFloor;
                case "Коттедж":
                    return ApartmentTypeEnum.Cottage;
                case "Таунхаус":
                    return ApartmentTypeEnum.Townhouse;
                case "Коттедж делюкс":
                    return ApartmentTypeEnum.CottageDeluxe;
                case "Коттедж комфорт":
                    return ApartmentTypeEnum.CottageComfort;
                case "Дуплекс":
                    return ApartmentTypeEnum.Duplex;
                case "Квадрохаус":
                    return ApartmentTypeEnum.QuadroHouse;
                case "Сплитхаус":
                    return ApartmentTypeEnum.SplitHouse;
                case "Участок":
                    return ApartmentTypeEnum.Plot;
                case "Квартира (тех.этаж)":
                    return ApartmentTypeEnum.TechStage;
                case "Мотопаркинг":
                    return ApartmentTypeEnum.Motoparking;
            }
            return ApartmentTypeEnum.App;
        }

        public static FileInfo GetFile(Document doc)
        {
            var path = doc.PathName;
            if (doc.IsDetached)
                path = path.Replace("_отсоединено.rvt", ".rvt");
            try
            {
                FileInfo file = new FileInfo(path);
                if (file.Exists)
                {
                    return file;
                }
                FileInfo fileWithoutDetach = new FileInfo(path);
            }
            catch
            {
                MessageBox.Show("Путь файла не найден");
            }
            return null;
        }


        public static double GetProjectMaxPoint(List<Element> allWalls)
        {
            double feetToMeter = 304.8;
            double maxPoint = -100000;
            var wallBoundingBoxMaxPoints = allWalls.Select(x => x.get_BoundingBox(null).Max.Z * feetToMeter);
            foreach (var bbPoint in wallBoundingBoxMaxPoints)
            {
                if (bbPoint > maxPoint)
                    maxPoint = bbPoint;
            }
            return maxPoint;
        }

        public static double GetProjectMinPoint(List<Element> allFloors)
        {
            double minPoint = 100000;
            double feetToMeter = 304.8;
            var floorBoundingBoxMaxPoints = allFloors.Select(x => x.get_BoundingBox(null).Min.Z * feetToMeter);
            foreach (var bbPoint in floorBoundingBoxMaxPoints)
            {
                if (bbPoint < minPoint)
                    minPoint = bbPoint;
            }
            if (minPoint == 100000) minPoint = 0;
            return minPoint;
        }

        public static ProjectPointsDTO GetBoxExtremeCoordinates(List<Element> allWalls)
        {
            double feetToMeter = 304.8;
            List<double> pointsXList = new List<double>();
            List<double> pointsYList = new List<double>();

            var wallsResult = allWalls.Where(x => !x.Name.ToLower().Contains("витраж"));
            foreach (var wall in wallsResult)
            {
                GeometryElement elemGeom = wall.get_Geometry(new Options());
                foreach (var solid in elemGeom)
                {
                    Solid solidAsSolid = solid as Solid;
                    foreach (Edge edge in solidAsSolid.Edges)
                    {
                        var startPoint = edge.AsCurve().GetEndPoint(0);
                        var endPoint = edge.AsCurve().GetEndPoint(1);
                        pointsXList.Add(startPoint.X * feetToMeter);
                        pointsYList.Add(startPoint.Y * feetToMeter);
                        pointsXList.Add(endPoint.X * feetToMeter);
                        pointsYList.Add(endPoint.Y * feetToMeter);
                    }
                }
            }

            return new ProjectPointsDTO
            {
                MaxX = pointsXList.Max(),
                MaxY = pointsYList.Max(),
                MinX = pointsXList.Min(),
                MinY = pointsYList.Min()
            };
        }

        public static ModelTypeEnum GetModelType(object selectedModelType)
        {
            switch (selectedModelType)
            {
                case "Первый этаж":
                    return ModelTypeEnum.FirstStage;
                case "Второй этаж":
                    return ModelTypeEnum.SecondStage;
                case "Типовой этаж":
                    return ModelTypeEnum.TypoStage;
                case "Тех подполье":
                    return ModelTypeEnum.TechStage;
                case "Крыша":
                    return ModelTypeEnum.Roof;
                case "Коттедж":
                    return ModelTypeEnum.Roof;
            }
            return ModelTypeEnum.FirstStage;
        }

        public static void SetProjectBasepoint(Document doc)
        {
            using (Transaction tra = new Transaction(doc))
            {
                tra.Start("New Transaction");
                FilteredElementCollector collector = new FilteredElementCollector(doc);
                IList<Element> siteElements = collector.WherePasses(new ElementCategoryFilter(BuiltInCategory.OST_ProjectBasePoint)).ToElements();
                foreach (Element basePoint in siteElements)
                {
                    basePoint.LookupParameter("Отм").Set(0);
                    basePoint.LookupParameter("С/Ю").Set(0);
                    basePoint.LookupParameter("В/З").Set(0);
                    basePoint.LookupParameter("Угол от истинного севера").Set(0);
                    tra.Commit();
                }
            }
        }
    }
}