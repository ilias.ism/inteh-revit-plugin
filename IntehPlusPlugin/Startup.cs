﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using IntehPlus.Interfaces;
using IntehPlus.Logic;
using System;
using System.Reflection;

namespace IntehPlus
{
    public class Startup : IExternalApplication
    {
        public static IBaseLogic _baseLogic;
        public static ICalculationLogic _calculationLogic;
        public static IUILogic _uiLogic;
        static readonly string thisAssemblyPath = Assembly.GetExecutingAssembly().Location;
        static readonly string resPath = "pack://application:,,,/IntehPlusPlugin;component/Resources/";
        public static DockablePaneId toolPaneId = new DockablePaneId(new Guid("24b9cbc4-474d-4d66-92ba-8252eeb5195a"));
        protected Document doc;
        protected UIDocument uiDoc;
        protected UIApplication uiApp;

        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }

        public virtual Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            doc = commandData.Application.ActiveUIDocument.Document;
            uiDoc = commandData.Application.ActiveUIDocument;
            uiApp = commandData.Application;
            return Result.Succeeded;
        }

        public Result OnStartup(UIControlledApplication application)
        {
            _baseLogic = new BaseLogic(application);
            _calculationLogic = new CalculationLogic(_baseLogic);
            _uiLogic = new UILogic(_baseLogic, application, thisAssemblyPath, resPath);
            _uiLogic.CreateRibbonTab();
            return Result.Succeeded;
        }

    }
}