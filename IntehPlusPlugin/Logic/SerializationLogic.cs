﻿using JWT;
using Newtonsoft.Json;

public class JwtNewtonSoft : IJsonSerializer
{
    public T Deserialize<T>(string json)
    {
        return JsonConvert.DeserializeObject<T>(json);
    }

    public string Serialize(object obj)
    {
        return JsonConvert.SerializeObject(obj);
    }
}