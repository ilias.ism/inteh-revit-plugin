﻿using Autodesk.Revit.UI;
using IntehPlus.Interfaces;
using System;
using System.Windows.Media.Imaging;

namespace IntehPlus.Logic
{
    public class UILogic: IUILogic
    {
        private readonly IBaseLogic _baseLogic;
        private readonly UIControlledApplication _application;
        private readonly string _thisAssemblyPath;
        private readonly string _resPath;
        static string resPath = "pack://application:,,,/IntehPlusPlugin;component/Resources/";

        public UILogic(IBaseLogic baseLogic, UIControlledApplication application, string thisAssemblyPath, string resPath)
        {
            _baseLogic = baseLogic;
            _application = application;
            _thisAssemblyPath = thisAssemblyPath;
            _resPath = resPath;
        }

        public void CreateRibbonTab()
        {
            var tabName = "IntehPlus";
            _application.CreateRibbonTab(tabName);
            RibbonPanel ribbonPanel1 = _application.CreateRibbonPanel(tabName, "IntehPlus");

            CreateArchitectureButton(ribbonPanel1);
            CreateCommonButton(ribbonPanel1);
            CreateElectricButton(ribbonPanel1);
            CreateOVVKButton(ribbonPanel1);
            CreateKJButton(ribbonPanel1);
            CreateCodesButton(ribbonPanel1);
            CreateSuperfilterButton(ribbonPanel1);
            CreateInspectorButton(ribbonPanel1);
        }

        void CreateArchitectureButton(RibbonPanel ribbonPanel)
        {
            PushButtonData b1Data = new PushButtonData("Архитектура", "Архитектура", _thisAssemblyPath, typeof(Commands.ArchitectureCommand).FullName);
            PushButton pb1 = ribbonPanel.AddItem(b1Data) as PushButton;
            pb1.ToolTip = "Архитектурные плагины";
            BitmapImage pbImage = new BitmapImage(new Uri(resPath + "arch.jpg"));
            pb1.Image = pb1.LargeImage = pbImage;
        }

        void CreateCommonButton(RibbonPanel ribbonPanel)
        {
            PushButtonData Data = new PushButtonData("Общие", "Общие", _thisAssemblyPath, typeof(Commands.CommonCommand).FullName);
            PushButton pb = ribbonPanel.AddItem(Data) as PushButton;
            pb.ToolTip = "Общие плагины";
            BitmapImage pbImage = new BitmapImage(new Uri(resPath + "tehIcon.jpg"));
            pb.Image = pb.LargeImage = pbImage;
        }

        void CreateElectricButton(RibbonPanel ribbonPanel)
        {
            PushButtonData Data = new PushButtonData("Электрика", "Электрика", _thisAssemblyPath, typeof(Commands.ElectricalCommand).FullName);
            PushButton pb = ribbonPanel.AddItem(Data) as PushButton;
            pb.ToolTip = "Плагины по электрике";
            BitmapImage pbImage = new BitmapImage(new Uri(resPath + "el.jpg"));
            pb.Image = pb.LargeImage = pbImage;
        }

        void CreateOVVKButton(RibbonPanel ribbonPanel)
        {
            PushButtonData Data = new PushButtonData("ОВиВК", "ОВиВК", _thisAssemblyPath, typeof(Commands.OVVKCommand).FullName);
            PushButton pb = ribbonPanel.AddItem(Data) as PushButton;
            pb.ToolTip = "Плагины по ОВиВК";
            BitmapImage pbImage = new BitmapImage(new Uri(resPath + "ovvk.jpg"));
            pb.Image = pb.LargeImage = pbImage;
        }

        void CreateKJButton(RibbonPanel ribbonPanel)
        {
            PushButtonData Data = new PushButtonData("КЖ", "КЖ", _thisAssemblyPath, typeof(Commands.KJCommand).FullName);
            PushButton pb = ribbonPanel.AddItem(Data) as PushButton;
            pb.ToolTip = "КЖ";
            BitmapImage pbImage = new BitmapImage(new Uri(resPath + "kj.jpg"));
            pb.Image = pb.LargeImage = pbImage;
        }

        void CreateCodesButton(RibbonPanel ribbonPanel)
        {
            PushButtonData Data = new PushButtonData("АГСК Коды", "АГСК Коды", _thisAssemblyPath, typeof(Commands.CodesCommand).FullName);
            PushButton pb = ribbonPanel.AddItem(Data) as PushButton;
            pb.ToolTip = "АГСК Коды";
            BitmapImage pbImage = new BitmapImage(new Uri(resPath + "agsk.jpg"));
            pb.Image = pb.LargeImage = pbImage;
        }

        void CreateSuperfilterButton(RibbonPanel ribbonPanel)
        {
            PushButtonData Data = new PushButtonData("Суперфильтр", "Суперфильтр", _thisAssemblyPath, typeof(Commands.SuperFilterCommand).FullName);
            PushButton pb = ribbonPanel.AddItem(Data) as PushButton;
            pb.ToolTip = "Суперфильтр";
            BitmapImage pbImage = new BitmapImage(new Uri(resPath + "filter.jpg"));
            pb.Image = pb.LargeImage = pbImage;
        }

        void CreateInspectorButton(RibbonPanel ribbonPanel)
        {
            PushButtonData Data = new PushButtonData("Инспектор", "Инспектор", _thisAssemblyPath, typeof(Commands.InspectorCommand).FullName);
            PushButton pb = ribbonPanel.AddItem(Data) as PushButton;
            pb.ToolTip = "Инспектор";
            BitmapImage pbImage = new BitmapImage(new Uri(resPath + "inspector.jpg"));
            pb.Image = pb.LargeImage = pbImage;
        }
    }
}
