﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using IntehPlus.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntehPlus.Logic
{
    public class BaseLogic: IBaseLogic, IExternalCommand
    {
        private readonly UIControlledApplication _uIControlledApplication;
        public BaseLogic(UIControlledApplication uIControlledApplication)
        {
            this._uIControlledApplication = uIControlledApplication;
        }

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            Document doc = uiApp.ActiveUIDocument.Document;
            return Result.Succeeded;
        }
    }
}
