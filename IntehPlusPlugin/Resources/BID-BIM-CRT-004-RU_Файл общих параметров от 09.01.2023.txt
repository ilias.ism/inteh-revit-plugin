# This is a Revit shared parameter file.
# Do not edit manually.
*META	VERSION	MINVERSION
META	2	1
*GROUP	ID	NAME
GROUP	1	01_Общие
GROUP	2	02_Архитектура
GROUP	3	03_Конструкции
GROUP	4	04_Инженерные системы
GROUP	5	05_Штамп
GROUP	6	06_Зависимости
GROUP	7	07_Вложенные семейства
GROUP	8	Other
*PARAM	GUID	NAME	DATATYPE	DATACATEGORY	GROUP	VISIBLE	DESCRIPTION	USERMODIFIABLE	HIDEWHENNOVALUE
PARAM	3487f300-aaa1-4c78-9be3-919d18cdf574	BI_тепловая_мощность_секции	HVAC_POWER		4	1		1	0
PARAM	cf2e8005-1d81-444a-9565-c54035bab8e6	BI_штамп_строка_6_фамилия	TEXT		5	1		1	0
PARAM	140f9f07-3745-493c-9e41-f317250117b7	BI_смещение_в_конце	LENGTH		6	1		1	0
PARAM	a6970c08-cee6-4703-b029-15660e6dac33	BI_комментарии_к_виду	TEXT		1	1		1	0
PARAM	15bffa08-3f27-49af-974e-fd8fa4ccf786	BI_количество_уровней	INTEGER		2	1	Для машиномест: Без подъемника - 1, Двухуровневый подъемник - 2, Трехуровневый подъемник - 3	1	0
PARAM	d48f0609-e17b-4882-ab5b-95029699d8aa	BI_штамп_боковой_строка_3_фамилия	TEXT		5	1		1	0
PARAM	ebab2c09-c77a-4d25-8b41-43867fc5111c	BI_класс_арматуры	INTEGER		3	1	Класс арматуры 	1	0
PARAM	a311e809-7de2-4628-9218-63a2105670c9	BI_примечание	TEXT		1	1		1	0
PARAM	463d790a-d63a-4b7b-8175-fb81ed946428	BI_примечание_расширенное	MULTILINETEXT		1	1	Расширенное примечание к элементу (многострочный текст)	1	0
PARAM	4b70cf0b-0295-41e7-b67b-dfc31c1c086e	BI_штамп_фамилия_руководителя	TEXT		5	1		1	0
PARAM	1052200e-fbc9-4eef-9aa2-2e6a29a7037a	BI_штамп_боковой_строка_4_должность	TEXT		5	1		1	0
PARAM	37deca0e-778e-47dd-93a8-3e7bebd8e7b0	BI_H2	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	effda80f-4b72-423a-a584-dd112f5598b5	BI_H1	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	9959cb0f-9d52-4ce2-a49e-5d3c3581bab8	BI_штамп_боковой_строка_2_должность	TEXT		5	1		1	0
PARAM	2ce73612-809b-46ea-ba06-36c32d2f3a89	BI_индекс_помещения	TEXT		2	1		1	0
PARAM	4ca4dc12-c41e-40ac-a56a-7c739a33fc60	BI_реализуемая_площадь_здания	AREA		1	1		1	0
PARAM	f46eff13-8595-43ef-8bf1-910ade2b8c1a	BI_размеры	TEXT		4	1		1	0
PARAM	df9bd215-726a-4754-a0eb-9c12dc961c93	BI_штамп_строка_2_фамилия	TEXT		5	1		1	0
PARAM	45e9ee17-3fce-4268-953b-56893f3ef395	BI_площадь_квартиры	AREA		2	1		1	0
PARAM	fb846918-7aaf-4549-aefc-20e5fe3aa749	BI_штамп_должность_руководителя	TEXT		5	1		1	0
PARAM	8abb0419-eeef-47dc-9795-8d5ef3a80a89	BI_штамп_боковой_строка_1_фамилия	TEXT		5	1		1	0
PARAM	4289cb19-9517-45de-9c02-5a74ebf5c85d	BI_единица_измерения	TEXT		1	1		1	0
PARAM	9c98831b-9450-412d-b072-7d69b39f4028	BI_обозначение	TEXT		1	1	Обозначение	1	0
PARAM	be29221e-5b74-4a61-a253-4eb5f3b532d4	BI_напряжение	ELECTRICAL_POTENTIAL		4	1	Электрическое напряжение	1	0
PARAM	39fd0523-339c-4dc2-bf8f-47ccf8957dd9	BI_наименование_составное_длина_1	LENGTH		3	1		1	0
PARAM	92ae0425-031b-40a9-8904-023f7389962b	BI_марка_изделия	TEXT		3	1	Марка изделия, закладной детали или арматурного каркаса (например, Каркас Кр-1, ЗД-1)	1	0
PARAM	5d7cb726-ac59-4f05-a902-8fdffa796d14	BI_шаг_стержней	LENGTH		3	1	Шаг стержней в массиве	1	0
PARAM	25dc5027-70e5-45fb-a1fb-2234f86a62d3	BI_площадь_квартиры_жилая	AREA		2	1		1	0
PARAM	92829d27-ed54-4976-a903-1ce6a6d3f2a4	BI_общая_масса_арматуры	NUMBER		3	1		1	0
PARAM	e0dfd327-ea9d-43ac-baaf-dc3c0ae12e56	BI_отделка_по_утеплителю	MULTILINETEXT		2	1		1	0
PARAM	5d14752d-f66d-454a-8546-3d9b9da01149	BI_отметка_от_нуля	LENGTH		1	1		1	0
PARAM	e6dfe630-97c1-4d6a-8de0-8f13dfd3ea51	BI_срубка_сваи	LENGTH		3	1		1	0
PARAM	497c6a31-92cc-4862-9c0d-9b917cb78d87	BI_состав	MULTILINETEXT		2	1	Состав элемента по слоям	1	0
PARAM	901e3233-8e07-42d3-b839-af8dd8d71301	BI_штамп_количество_листов	INTEGER		5	1		1	0
PARAM	eec6c033-b52e-4672-982b-b87e4708986c	BI_штамп_строка_3_фамилия	TEXT		5	1		1	0
PARAM	7a378b34-120f-41d3-9dc6-bbe580f4115f	BI_стадия	TEXT		1	1		1	0
PARAM	623d5b37-938c-4939-8f80-527e2a6a2ae2	BI_штамп_строка_1_должность	TEXT		5	1		1	0
PARAM	37739d37-ca0e-4646-8b58-d07df83b6fc7	BI_секция	INTEGER		1	1	Для связанных файлов	1	0
PARAM	dd9cc03a-a9d9-41ed-9083-9b6af83af075	BI_откос_наружный	YESNO		7	1		1	0
PARAM	19a0d63c-2cde-4d85-b647-316ea4a2de8b	BI_откос_внутренний_тип_окно	FAMILYTYPE	-2000014	7	1		1	0
PARAM	a809313d-c497-48d0-9615-a169bf280a49	BI_коэффициент_площади	NUMBER		2	1		1	0
PARAM	da075942-2789-4218-b9a2-c2a122e7a93e	BI_доп_марка_конструкции	TEXT		3	1		1	0
PARAM	d4a77742-b01d-48cc-b933-0693c5776d82	BI_материал_обозначение	TEXT		3	1	Текстовое обозначение материала элемента. Используется в спецификациях. 	1	0
PARAM	055ab943-2040-44e0-aa64-9acf8584a585	BI_штамп_строка_3_должность	TEXT		5	1		1	0
PARAM	00b51044-7202-4d28-a670-77f281d0e132	BI_общая_площадь_здания	AREA		1	1		1	0
PARAM	d8aa8c44-4816-4575-999a-22a7dc6cde47	BI_сортировка_группы	NUMBER		1	1		1	0
PARAM	d7eb5845-9c94-4a75-9b4e-1182693ea6e8	BI_толщина_внутренней_отделки	LENGTH		7	1		1	0
PARAM	80a26045-26c7-40e5-be95-4391ad939069	BI_свая_пробная	YESNO		3	1		1	0
PARAM	4673f045-9574-471f-9677-ac538a9e9a2d	BI_этаж	CURRENCY		1	1	Ниже первого этажа - начиная с -1 и далее по порядку&#xD&#xAНачиная с первого этажа - 1 и далее по порядку&#xD&#xAЧердак (техэтаж) - 99&#xD&#xAКровля (парапет, кровля паркинга) - 100&#xD&#xAБудка выхода на кровлю - 101	1	0
PARAM	ad5eca46-8750-4aa8-86c4-854e5ca703f8	BI_равномерное_распределение	YESNO		6	1	Остовляет шаг массива везде одинаковым и равномерно изменяет смещение по краям	1	0
PARAM	88006a47-86c7-4095-b13b-b51962ca3921	BI_наименование_составное_разделитель_1	TEXT		3	1		1	0
PARAM	b4226948-fb49-4de0-9c67-6ebebd4e714a	BI_перемычка_тип	FAMILYTYPE	-2001320	7	1		1	0
PARAM	8ce3784c-6d30-422c-b852-733e8e69c761	BI_отметка_от_этажа	LENGTH		1	1		1	0
PARAM	80efad4d-3dfe-41b7-b56f-5581b78d22d5	BI_подоконник_тип	FAMILYTYPE	-2000014	7	1		1	0
PARAM	1b1e174f-8df2-4c24-8b0e-24831b0cd0a9	BI_перемычка_четверть	LENGTH		7	1		1	0
PARAM	adbffe51-5f69-44e4-9b5c-4e254548dc46	BI_отделка_по_гипсокартону	MULTILINETEXT		2	1		1	0
PARAM	7e8a4453-1b99-4497-ba49-e3f549eb017a	BI_масса	MASS		1	1	Масса элемента	1	0
PARAM	1cc8d654-4cc8-4d12-ac0d-b23717115f65	BI_количество_расчетное	AREA		1	1	Для расчета в спецификациях, не разбивая группы моделей	1	0
PARAM	5350d954-198d-400c-beca-5613e48345b3	BI_rd	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	99880e57-3fa8-4125-8d39-df56ee4e7ded	BI_штамп_имя_листа_пользовательское	MULTILINETEXT		5	1		1	0
PARAM	93ca1c59-ff21-4e29-9e34-2d0af766df3d	BI_гнутая_по_месту	YESNO		3	1		1	0
PARAM	8a796d59-e513-4083-9665-27602b9f28de	BI_отверстие_назначение	TEXT		1	1		1	0
PARAM	a12b865c-74ca-426b-94f4-bd9344744cd3	BI_тип_изделия	INTEGER		1	1		1	0
PARAM	6d7e375d-8dc2-45bf-a9a9-ced54c167fda	BI_примечание_к_виду	TEXT		1	1		1	0
PARAM	856b675e-ea2b-4e60-8b5b-92321edb713e	BI_E	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	f194bf60-b880-4217-b793-1e0c30dda5e1	BI_наименование_краткое	TEXT		1	1	Наименование в краткой форме	1	0
PARAM	b2e65c63-c101-4baf-a5bc-4203165d5942	BI_откос_внутренний_тип_дверь	FAMILYTYPE	-2000023	7	1		1	0
PARAM	6c2c9e64-e5ca-4c80-bf32-3ec2ac1df3fc	BI_тип_помещения	TEXT		2	1		1	0
PARAM	09021265-8b1f-4af3-b3f7-0c1c350823f1	BI_смещение_в_начале_угол	ANGLE		6	1		1	0
PARAM	90f7d565-8c52-40e6-b723-13edaca93c21	BI_ширина_активной_створки	LENGTH		2	1	Ширина активной створки витражных дверей	1	0
PARAM	6b496b67-cda5-4b8f-b21d-005574cff9c6	BI_количество_секций	INTEGER		4	1		1	0
PARAM	23ae6269-466b-4c97-bcce-c82324521fc2	BI_штамп_боковой_строка_1_должность	TEXT		5	1		1	0
PARAM	03077d69-349a-4209-aa69-efd9e433980c	BI_ß	ANGLE		3	1		1	0
PARAM	e570566b-0f96-449b-a3d6-c435e5bf2e91	BI_штамп_боковой_строка_3_должность	TEXT		5	1		1	0
PARAM	9c9ad96c-e473-4682-afe3-cad8354a4950	BI_материал_тип_подсчета	INTEGER		1	1		1	0
PARAM	e945026d-5908-467e-bc95-f63ec2e3db63	BI_длина	LENGTH		1	1		1	0
PARAM	74cd446e-aa43-4cb8-9145-3ae289466a86	BI_штамп_строка_5_фамилия	TEXT		5	1		1	0
PARAM	e8c7986e-63dc-4e0a-93f7-09af35cc0d0c	BI_площадь_квартиры_общая	AREA		2	1		1	0
PARAM	44c2bd6f-7031-40a2-8932-9441eb8f4f65	BI_наименование_составное_длина_2	LENGTH		3	1		1	0
PARAM	64765b70-a27d-4081-a87a-c8b91a0cd4a5	BI_частота	ELECTRICAL_FREQUENCY		4	1		1	0
PARAM	26593872-c51f-4e67-bd0c-cf7e125f79c3	BI_штамп_боковой_строка_2_фамилия	TEXT		5	1		1	0
PARAM	03d3a172-bdaf-4942-adce-b302268080f4	BI_D	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	4b9a1e75-0b62-4413-aeb6-6db09ea76350	BI_штамп_номер_страницы	INTEGER		5	1		1	0
PARAM	1c854a75-f2ff-459a-8cfe-870fa133726a	BI_наименование_составное_текст_1	TEXT		3	1		1	0
PARAM	86779a75-8fc0-45d6-9052-eef57ebabcbe	BI_R	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	7733ee7a-2024-4c1a-b8cb-42aaad46b74e	BI_штамп_строка_4_фамилия	TEXT		5	1		1	0
PARAM	c59c0e7b-5539-45b5-98fb-e3f41444e232	BI_G	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	381b467b-3518-42bb-b183-35169c9bdfb1	BI_толщина_стенки	LENGTH		4	1	Толщина стенки элемента	1	0
PARAM	a8cdbf7b-d60a-485e-a520-447d2055f352	BI_завод_изготовитель	TEXT		4	1	Завод изготовитель оборудования	1	0
PARAM	afd9357c-d90e-42da-8823-919d62d7f635	BI_диаметр	LENGTH		1	1		1	0
PARAM	3630807e-1d10-4903-ba6f-fcfeae9345f3	BI_наименование_составное_разделитель_2	TEXT		3	1		1	0
PARAM	14536f7f-abdb-4083-a297-c1e3382eaebc	BI_наименование_изделия	TEXT		3	1		1	0
PARAM	e7db3f85-ac03-4322-9047-773319a55336	BI_версия_шаблона	NUMBER		1	1		1	0
PARAM	caacd185-dfc4-49b9-af3e-d2c7e1d8dfe9	BI_описание	TEXT		1	1		1	0
PARAM	02e00487-284a-4f98-8715-2b2575fca964	BI_отделка_по_газоблоку	MULTILINETEXT		2	1		1	0
PARAM	604acd87-4d11-4627-a9e0-9e06b8337499	BI_штамп_боковой_строка_5_должность	TEXT		5	1		1	0
PARAM	597e3688-e680-47fc-9168-760234a65fa0	BI_наименование_элемента_фасада	TEXT		2	1		1	0
PARAM	47a25b88-0109-431d-91f5-d24ea7b666ab	BI_проем_ширина	LENGTH		6	1		1	0
PARAM	905fca88-47cc-4016-ac9d-1403c39831c8	BI_смещение_сверху	LENGTH		6	1		1	0
PARAM	1969348a-ee8b-4063-8ff2-1cd564495c13	BI_штамп_боковой_строка_5_фамилия	TEXT		5	1		1	0
PARAM	2156d28a-1b86-4ae1-84e7-9a0ef0bb30dd	BI_квартира	CURRENCY		2	1		1	0
PARAM	2301588c-7eb4-412d-abea-f20c1b4507b7	BI_изображение_материала	IMAGE		2	1		1	0
PARAM	25cb1c8d-eac3-4b9c-97e0-03f4977e5835	BI_длина_нахлеста_множитель	INTEGER		3	1	Количество диаметров для определния длины нахлеста	1	0
PARAM	2caf388f-1950-4020-9ede-bffbef8b24c5	BI_верхний	YESNO		6	1	Для последних этажей. Убирает анкеровку арматур вверх	1	0
PARAM	66c2948f-4abe-48be-994a-2b78ea121beb	BI_ссылка_на_лист	TEXT		3	1		1	0
PARAM	2e6bef91-5dab-4964-8683-432522f35193	BI_штамп_боковой_строка_6_должность	TEXT		5	1		1	0
PARAM	861e0193-0b49-4d8d-9cfa-5f49da32fc7d	BI_увеличение_проема	YESNO		2	1		1	0
PARAM	e54fc393-d86d-4253-9e7d-02f5fa3843ac	BI_A	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	3fadbd95-e89f-45b4-821a-8f5edab693f5	BI_смещение_по_траектории	LENGTH		6	1		1	0
PARAM	ece17796-8fa7-444f-bc33-57a75cdcb84b	BI_смещение_в_конце_угол	ANGLE		6	1		1	0
PARAM	8292a196-5e2c-4482-b3f7-ba0d89ed605f	BI_наименование_составное_префикс	TEXT		3	1		1	0
PARAM	870e9197-27d5-401c-87a7-3133a235721f	BI_уклон	PIPING_SLOPE		4	1		1	0
PARAM	48eb8f9b-dc00-43f5-b943-431abf37997f	BI_количество	NUMBER		1	1		1	0
PARAM	2204049c-d557-4dfc-8d70-13f19715e45d	BI_марка_конструкции	TEXT		1	1	Марка конструктивного элемента где находится арматура	1	0
PARAM	e393fe9c-b916-49e1-95fd-7743c793491f	BI_высота_помещения	CURRENCY		2	1		1	0
PARAM	83ac059d-e41b-4d9a-bf64-7b336cd5563a	BI_сортировка	NUMBER		3	1		1	0
PARAM	b3c97c9e-0509-4901-a3c5-b157ca6fbffa	BI_штамп_боковой_строка_6_фамилия	TEXT		5	1		1	0
PARAM	9e789da3-cfd2-4f09-8cc7-702bc9c28f46	BI_количество_стержней	INTEGER		3	1	Количество стержней в массиве	1	0
PARAM	627547a4-f42e-42f3-a42e-c8713d635cd9	BI_площадь	AREA		1	1		1	0
PARAM	3de5f1a4-d560-4fa8-a74f-25d250fb3402	BI_группирование	TEXT		1	1		1	0
PARAM	40300ba6-9b36-422e-b825-2d285aee031d	BI_штамп_строка_5_должность	TEXT		5	1		1	0
PARAM	338033a7-4221-428e-9c76-4793d5155392	BI_диаметр_арматуры	LENGTH		3	1	Диаметр арматуры	1	0
PARAM	ffd5b2a7-3613-4013-ab33-ae18647b7e94	BI_эскиз	IMAGE		1	1		1	0
PARAM	4e1fb3aa-7268-42f0-9b07-d18cfdbf746e	BI_ширина	LENGTH		1	1		1	0
PARAM	b4d13aad-0763-4481-b015-63137342d074	BI_номинальная_мощность	ELECTRICAL_POWER		4	1	Номинальная электрическая мощность. P [Вт или кВт]	1	0
PARAM	4fb334ae-8579-4fc5-8582-bf2e37ac215e	BI_штамп_строка_2_должность	TEXT		5	1		1	0
PARAM	68df3fae-3e98-429a-a168-1b6f7a1486b0	BI_тип_элемента_КЖ	INTEGER		3	1	1 - Арматурный стержень; &#xD&#xA2 - Арматурный стержень в составе каркаса; &#xD&#xA4 - Элемент закладной детали; &#xD&#xA8 - Арматурный каркас; &#xD&#xA16 - Закладная деталь.	1	0
PARAM	ae146bb0-5e60-462d-926c-629cf6e05017	BI_C	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	017821b4-1f6e-49df-a144-2b51e326b59e	BI_проем_высота	LENGTH		6	1		1	0
PARAM	b3d72db4-e4d0-43ba-bf88-6093d2219dfb	BI_штамп_строка_4_должность	TEXT		5	1		1	0
PARAM	525d9ab4-936f-48d8-8006-4eb5235b93a3	BI_штамп_строка_6_должность	TEXT		5	1		1	0
PARAM	4c4727b6-e99c-4fbd-8656-d763fbd8acd7	BI_позиция	TEXT		1	1		1	0
PARAM	a4fe29b6-51e1-43f1-a299-5dfd0f52e95a	BI_фильтр	TEXT		1	1		1	0
PARAM	7edc43b6-9de5-4e42-8e97-dd8230c2d178	BI_арматура_по_оси	YESNO		6	1		1	0
PARAM	37a252b7-811b-48a7-909c-7252e8c14e05	BI_отлив_тип	FAMILYTYPE	-2000014	7	1		1	0
PARAM	fc0665b7-63dd-44f2-8805-558177eccfb2	BI_арматура_семейством	YESNO		3	1	Параметр включен для арматуры, выполненной загружаемыми семействами, и отключен для стандартной арматуры	1	0
PARAM	23d90ab8-0bcd-43f5-931a-998d34ff17a5	BI_ширина_стационарной_створки	LENGTH		2	1	Ширина стационарной створки витражных дверей	1	0
PARAM	646272b8-ca22-4d15-82ed-c3415a21e9a9	BI_арматура_длина_по_прямой	LENGTH		3	1		1	0
PARAM	2b2623ba-3d9e-43a8-8c22-907372e360be	BI_штамп_раздел_проекта	TEXT		5	1		1	0
PARAM	902f83ba-3663-42b2-bc47-7a4530527d1d	BI_тип_подсчета	INTEGER		1	1	-1 - не учитывать&#xD&#xA0 - шт&#xD&#xA1 - метры&#xD&#xA2 - кв. метры&#xD&#xA3 - куб. метры&#xD&#xA4 - килограммы	1	0
PARAM	b18d86bd-219c-4738-8faa-be002e13116e	BI_толщина_наружной_отделки	LENGTH		7	1		1	0
PARAM	7af46fbf-844e-40c1-b2ef-3d2aae237edc	BI_количество_фаз	INTEGER		4	1		1	0
PARAM	0e5595c0-dc4b-4aaa-9bcd-7f13713a80e0	BI_перемычка	YESNO		7	1		1	0
PARAM	1d2cf4c1-3b2c-49c4-81c0-f05b6ce12c2d	BI_номер_стояка	TEXT		4	1		1	0
PARAM	2f9abac5-9608-4bb6-b509-d75b738778cf	BI_арматура_гнутая	YESNO		3	1	Параметр включен для гнутых форм арматуры и выключен для прямых стержней	1	0
PARAM	dcd95bc7-6e47-4f7c-9f71-f78f2a029b77	BI_отлив	YESNO		7	1		1	0
PARAM	695be2c7-0bf6-4606-848c-c4b0405777f4	BI_смещение_с_траектории	LENGTH		6	1		1	0
PARAM	775ae4c7-cb52-4926-9574-626d708d2e9a	BI_наименование_составное_длина_3	LENGTH		3	1		1	0
PARAM	924756c8-9c97-41f1-8606-5864dae2bb44	BI_смещение_в_начале	LENGTH		6	1		1	0
PARAM	fa0b19c9-cb66-463c-96e5-1c40f1ac235f	BI_материал	MATERIAL		1	1		1	0
PARAM	c71a70c9-32d7-43ef-9f07-a8def2690147	BI_абсолютная_отметка_нуля	LENGTH		1	1		1	0
PARAM	194ae2ca-9b35-49c8-9706-0ee29f9915d5	BI_изменение_шага_снизу	YESNO		6	1		1	0
PARAM	2fd9e8cb-84f3-4297-b8b8-75f444e123ed	BI_код_оборудования	TEXT		4	1	Код оборудования, изделия, материала	1	0
PARAM	0430bfcd-5ab9-4c16-b2ab-952763b20fa6	BI_откос_наружный_тип_дверь	FAMILYTYPE	-2000023	7	1		1	0
PARAM	e6e0f5cd-3e26-485b-9342-23882b20eb44	BI_наименование	TEXT		1	1		1	0
PARAM	9ab66dce-a45d-49fb-907e-37af9504326e	BI_площадь_с_коэффициентом	AREA		2	1		1	0
PARAM	345c57cf-424f-49e2-b39e-368e289ca64c	BI_количество_комнат	CURRENCY		2	1		1	0
PARAM	5389cdcf-b207-490f-a9a4-462348af69b6	BI_фильтр_арматуры	TEXT		3	1	Параметр арматуры для использования в фильтрах	1	0
PARAM	8b8cd7cf-4981-4536-b0f7-762a86ae9a59	BI_изменение_шага_в_центре	YESNO		6	1		1	0
PARAM	e87304d0-5bcd-4d38-a29d-9f054e44e89a	BI_α	ANGLE		3	1		1	0
PARAM	4b9269d1-d66f-488c-87d0-29d2b05ff8c1	BI_F	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	7b9f33d3-70e3-467c-91ae-3a50e70695a1	BI_смещение_снизу	LENGTH		6	1		1	0
PARAM	2ec2c5d6-6c87-4284-8a25-5ac3d0f44ca2	BI_откос_наружный_тип_окно	FAMILYTYPE	-2000014	7	1		1	0
PARAM	393c62d7-8951-414d-9cb0-29584a035ebd	BI_B	REINFORCEMENT_LENGTH		3	1		1	0
PARAM	e851e7d7-2b9c-4066-9ced-d3236f3238a3	BI_порядковый_номер	INTEGER		2	1	Для указания порядкового номера машиномест	1	0
PARAM	616583d8-570c-46ec-b05d-68704b2a633a	BI_штамп_боковой_строка_4_фамилия	TEXT		5	1		1	0
PARAM	75f992d9-0e9e-4f03-b4cb-1e7122e76cbb	BI_толщина	LENGTH		1	1		1	0
PARAM	2aea0ddc-ffef-43a0-96be-cadaeefceb7c	BI_штамп_боковой_инвентарный_номер	TEXT		5	1		1	0
PARAM	a23436dc-1539-4d34-a320-3e9231955b24	BI_отделка_по_бетону	MULTILINETEXT		2	1		1	0
PARAM	141809de-48d2-4597-9293-6d337c4b306c	BI_отлив_выступ_от_откоса	LENGTH		7	1		1	0
PARAM	10fb72de-237e-4b9c-915b-8849b8908696	BI_сторона_здания	TEXT		2	1	Сторона здания	1	0
PARAM	33a98ae0-60f0-4ab0-b28f-bf875cc63435	BI_главный_элемент	YESNO		1	1		1	0
PARAM	4c6eb3e0-7a00-4e05-a6b5-81c89927b7d3	BI_штамп_строка_1_фамилия	TEXT		5	1		1	0
PARAM	844a01e2-19fc-4dc5-baa0-a4bda30ef1f9	BI_в_погонных_метрах	YESNO		3	1	В погонных метрах	1	0
PARAM	c74039e2-da2d-44f7-90af-a2efb9716492	BI_толщина_стены	LENGTH		6	1		1	0
PARAM	e97f0de3-1c29-4eef-88e1-40fe2722436b	BI_отделка_по_кирпичу	MULTILINETEXT		2	1		1	0
PARAM	fbd226e3-2500-4bf8-a12d-556d4597356a	BI_подоконник	YESNO		7	1		1	0
PARAM	952abee3-bed5-41b8-954d-037d4b240847	BI_отделка_потолка	MULTILINETEXT		2	1		1	0
PARAM	3bb3e5e4-1c53-4051-8eff-7786102c7330	BI_плотность	MASS_DENSITY		1	1		1	0
PARAM	282fa8e5-6c0e-4716-bdde-b7d245ee4682	BI_изменение_шага_в_начале	YESNO		6	1		1	0
PARAM	411b8fe8-754b-4e9b-8aa5-0280b1e65b0d	BI_объем	VOLUME		1	1		1	0
PARAM	ace912eb-094e-4fd8-a1c7-ccaada7682d3	BI_высота	LENGTH		1	1		1	0
PARAM	2f5698ee-e7a3-44c4-a38e-804c2daf04d0	BI_арматура_снизу	YESNO		6	1		1	0
PARAM	cfc2c0ee-26c6-450f-aceb-d9a4befa24a7	BI_длина_изделия	LENGTH		3	1		1	0
PARAM	27f3a4f0-d550-41f7-8fef-db5c0ec24602	BI_dоп	REINFORCEMENT_LENGTH		3	1	Диаметр оправки	1	0
PARAM	1eb36af2-9620-4adc-af97-8eebc615b95e	BI_откос_внутренний	YESNO		7	1		1	0
PARAM	4c2c0cf4-e1a7-40c0-8ef0-3791017cb7f4	BI_глубина	LENGTH		1	1		1	0
PARAM	001aa5f4-eb83-4eef-9a29-7525c3ad4798	BI_масса_1_пм	MASS_PER_UNIT_LENGTH		3	1	Масса 1 погонного метра элемента	1	0
PARAM	eb9264f6-a5f9-4195-95c3-0c4a82017bc5	BI_нижний	YESNO		6	1	Уберает разбежку снизу. Вся арматура будет начинаться с плиты	1	0
PARAM	6df445f8-f47d-47e8-93d2-b477ed1299f2	BI_площадь_внутренних_откосов	AREA		2	1	Площадь внутренних откосов	1	0
PARAM	f07965f9-3490-4c68-9404-3fa6721c1c6c	BI_холодильная_мощность	HVAC_COOLING_LOAD		4	1	Потребляемая/вырабатываемая холодильная мощность	1	0
PARAM	c9dbfffb-1d3e-4ed3-aae0-c374bdec39e5	BI_подъезд	CURRENCY		2	1		1	0
